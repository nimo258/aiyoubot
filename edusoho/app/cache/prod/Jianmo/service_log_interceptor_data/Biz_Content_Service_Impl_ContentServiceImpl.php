<?php 
 return array (
  'getContent' => 
  array (
  ),
  'getContentByAlias' => 
  array (
  ),
  'searchContents' => 
  array (
  ),
  'searchContentCount' => 
  array (
  ),
  'createContent' => 
  array (
    'module' => 'content',
    'action' => 'create',
    'param' => NULL,
    'postfix' => '',
    'funcName' => NULL,
    'funcParam' => 
    array (
      0 => 'content',
    ),
    'service' => 'Content:ContentService',
  ),
  'updateContent' => 
  array (
    'module' => 'content',
    'action' => 'update',
    'param' => 'id',
    'postfix' => '',
    'funcName' => NULL,
    'funcParam' => 
    array (
      0 => 'id',
      1 => 'content',
    ),
    'service' => 'Content:ContentService',
  ),
  'trashContent' => 
  array (
    'module' => 'content',
    'action' => 'trash',
    'param' => NULL,
    'postfix' => '',
    'funcName' => 'getContent',
    'funcParam' => 
    array (
      0 => 'id',
    ),
    'service' => 'Content:ContentService',
  ),
  'publishContent' => 
  array (
    'module' => 'content',
    'action' => 'publish',
    'param' => NULL,
    'postfix' => '',
    'funcName' => 'getContent',
    'funcParam' => 
    array (
      0 => 'id',
    ),
    'service' => 'Content:ContentService',
  ),
  'deleteContent' => 
  array (
    'module' => 'content',
    'action' => 'delete',
    'param' => NULL,
    'postfix' => '',
    'funcName' => NULL,
    'funcParam' => 
    array (
      0 => 'id',
    ),
    'service' => 'Content:ContentService',
  ),
  'isAliasAvaliable' => 
  array (
  ),
);