<?php 
 return array (
  'getThread' => 
  array (
  ),
  'createThread' => 
  array (
    'module' => 'thread',
    'action' => 'create',
    'param' => NULL,
    'postfix' => '',
    'funcName' => NULL,
    'funcParam' => 
    array (
      0 => 'thread',
    ),
    'service' => 'Thread:ThreadService',
  ),
  'updateThread' => 
  array (
    'module' => 'thread',
    'action' => 'update',
    'param' => 'id',
    'postfix' => '',
    'funcName' => NULL,
    'funcParam' => 
    array (
      0 => 'id',
      1 => 'fields',
    ),
    'service' => 'Thread:ThreadService',
  ),
  'deleteThread' => 
  array (
    'module' => 'thread',
    'action' => 'delete',
    'param' => NULL,
    'postfix' => '',
    'funcName' => NULL,
    'funcParam' => 
    array (
      0 => 'threadId',
    ),
    'service' => 'Thread:ThreadService',
  ),
  'setThreadSticky' => 
  array (
  ),
  'cancelThreadSticky' => 
  array (
  ),
  'setThreadNice' => 
  array (
  ),
  'cancelThreadNice' => 
  array (
  ),
  'setThreadSolved' => 
  array (
  ),
  'hitThread' => 
  array (
  ),
  'cancelThreadSolved' => 
  array (
  ),
  'searchThreads' => 
  array (
  ),
  'searchThreadCount' => 
  array (
  ),
  'waveThread' => 
  array (
  ),
  'getPost' => 
  array (
  ),
  'createPost' => 
  array (
  ),
  'deletePost' => 
  array (
  ),
  'searchPostsCount' => 
  array (
  ),
  'searchPosts' => 
  array (
  ),
  'setPostAdopted' => 
  array (
  ),
  'cancelPostAdopted' => 
  array (
  ),
  'wavePost' => 
  array (
  ),
  'getMemberByThreadIdAndUserId' => 
  array (
  ),
  'createMember' => 
  array (
  ),
  'deleteMember' => 
  array (
  ),
  'deleteMembersByThreadId' => 
  array (
  ),
  'searchMembers' => 
  array (
  ),
  'searchMemberCount' => 
  array (
  ),
  'countPartakeThreadsByUserIdAndTargetType' => 
  array (
  ),
  'findThreadIds' => 
  array (
  ),
  'findPostThreadIds' => 
  array (
  ),
  'voteUpPost' => 
  array (
  ),
);