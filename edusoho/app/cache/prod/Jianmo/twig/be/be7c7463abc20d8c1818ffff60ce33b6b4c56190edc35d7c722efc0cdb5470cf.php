<?php

/* admin/wechat-notification/index.html.twig */
class __TwigTemplate_98ba95bb0ca5a4de95b68911498af50f7856b80c375fbf93fd0efa66103ca84e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/wechat-notification/index.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["menu"] = "admin_operation_wechat_notification_record";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        // line 6
        echo "  <div class=\"well\" style=\"text-align:left;\">
    微信服务号通知不支持广告等营销类消息以及其它所有可能对用户造成骚扰的消息，如果通知发送失败，则可能是任务名称、课程名称等关键词中包含违规内容。
  </div>
<table class=\"table table-striped table-hover\" id=\"notification-table\" style=\"word-break:break-all;\">
  <thead>
    <tr>
      <th width=\"40%\">";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("wechat.notification.record_content"), "html", null, true);
        echo "</th>
      <th width=\"20%\">";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("wechat.notification.record_send_time"), "html", null, true);
        echo "</th>
      <th width=\"10%\">";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("wechat.notification.record_send_status"), "html", null, true);
        echo "</th>
      <th width=\"20%\">
        ";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("wechat.notification.record_number"), "html", null, true);
        echo "
        <a class=\"gray-primary\" href=\"javascript:;\" data-url=\"\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("wechat.notification.record_number_tips"), "html", null, true);
        echo "\"><i class=\"glyphicon glyphicon-question-sign text-muted\"></i></a>
      </th>
      <th width=\"10%\">";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("operation"), "html", null, true);
        echo "</th>
    </tr>
  </thead>
  <tbody>
  ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["notifications"]) ? $context["notifications"] : null));
        $context['_iterated'] = false;
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["notification"]) {
            // line 24
            echo "    ";
            $context["event"] = $this->getAttribute((isset($context["notificationEvents"]) ? $context["notificationEvents"] : null), $this->getAttribute($context["notification"], "eventId", array()), array(), "array");
            // line 25
            echo "    ";
            $this->loadTemplate("admin/wechat-notification/notification-tr.html.twig", "admin/wechat-notification/index.html.twig", 25)->display(array_merge($context, array("event" => (isset($context["event"]) ? $context["event"] : null))));
            // line 26
            echo "  ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 27
            echo "    <tr>
      <td colspan=\"20\">
        <div class=\"empty\">";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("wechat.notification.record.empty"), "html", null, true);
            echo "</div>
      </td>
    </tr>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['notification'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "  </tbody>
</table>
";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin_macro"]) ? $context["admin_macro"] : null), "paginator", array(0 => (isset($context["paginator"]) ? $context["paginator"] : null)), "method"), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "admin/wechat-notification/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 35,  122 => 33,  112 => 29,  108 => 27,  95 => 26,  92 => 25,  89 => 24,  71 => 23,  64 => 19,  59 => 17,  55 => 16,  50 => 14,  46 => 13,  42 => 12,  34 => 6,  31 => 5,  27 => 1,  25 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/wechat-notification/index.html.twig", "/var/www/edusoho/app/Resources/views/admin/wechat-notification/index.html.twig");
    }
}
