<?php

/* admin/system/report/status.html.twig */
class __TwigTemplate_752443617921fc20faf7ce7b7d3c8fc37f18fe3a82f34923a6b1f18cb390955a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/system/report/status.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["menu"] = "admin_report_status_list";
        // line 4
        $context["script_controller"] = "system/report-status";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_main($context, array $blocks = array())
    {
        // line 7
        echo "  <table class=\"table table-striped table-bordered\">
    <thead>
    <tr>
      <th width=\"40%\">";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.environmental_detection"), "html", null, true);
        echo "</th>
      <th width=\"20%\">";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.recommend_config"), "html", null, true);
        echo "</th>
      <th width=\"20%\">";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.current_status"), "html", null, true);
        echo "</th>
      <th width=\"20%\">";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.min_require"), "html", null, true);
        echo "</th>
    </tr>
    </thead>
    <tbody>

    <tr>
      <td>";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.operate_system"), "html", null, true);
        echo "</td>
      <td>Linux</td>
      <td>
        <span class=\"text-success\">√ ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "os", array()), "html", null, true);
        echo "</span>
      </td>
      <td>--</td>
    </tr>

    <tr>
      <td>";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.php_version"), "html", null, true);
        echo " （<a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_report_status_php");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.more_info"), "html", null, true);
        echo "</a>）</td>
      <td>5.5.x</td>
      <td>
        ";
        // line 31
        if ($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "phpVersionOk", array())) {
            // line 32
            echo "          <span class=\"text-success\">√ ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "phpVersion", array()), "html", null, true);
            echo "</span>
        ";
        } else {
            // line 34
            echo "          <span class=\"text-danger\">X ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "phpVersion", array()), "html", null, true);
            echo "</span>
        ";
        }
        // line 36
        echo "      </td>
      <td>5.3.3</td>
    </tr>

    <tr>
      <td>";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.php_run_user"), "html", null, true);
        echo "</td>
      <td>-</td>
      <td>
        <span class=\"text-success\">√ ";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "user", array()), "html", null, true);
        echo "</span>
      </td>
      <td>-</td>
    </tr>

    <tr>
      <td>";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.safe_mode"), "html", null, true);
        echo "</td>
      <td>";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.closed"), "html", null, true);
        echo "</td>
      <td>
        ";
        // line 53
        if (($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "safemode", array()) == "Off")) {
            // line 54
            echo "          <span class=\"text-danger\">X ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.open"), "html", null, true);
            echo "</span>
        ";
        } else {
            // line 56
            echo "          <span class=\"text-success\">√ ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.closed"), "html", null, true);
            echo "</span>
        ";
        }
        // line 58
        echo "      </td>
      <td>";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.closed"), "html", null, true);
        echo "</td>
    </tr>

    <tr>
      <td>PDO_MySQL</td>
      <td>";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.must"), "html", null, true);
        echo "</td>
      <td>
        ";
        // line 66
        if ($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "pdoMysqlOk", array())) {
            // line 67
            echo "          <span class=\"text-success\">√ ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.alerday_install"), "html", null, true);
            echo "</span>
        ";
        } else {
            // line 69
            echo "          <span class=\"text-danger\">X ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.no_install_mysql_pdo"), "html", null, true);
            echo "</span>
        ";
        }
        // line 71
        echo "      </td>
      <td>";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.must"), "html", null, true);
        echo "</td>
    </tr>

    <tr>
      <td>
        ";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.upload_file_size"), "html", null, true);
        echo "
        <div class=\"text-muted\">";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.upload_file_size_tips"), "html", null, true);
        echo "</div>
      </td>
      <td>";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.upload_file_size_greater_than_200m"), "html", null, true);
        echo "</td>
      <td>
        ";
        // line 82
        if ($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "uploadMaxFilesizeOk", array())) {
            // line 83
            echo "          <span class=\"text-success\">√ ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "uploadMaxFilesize", array()), "html", null, true);
            echo "</span>
        ";
        } else {
            // line 85
            echo "          <span class=\"text-danger\">X ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "uploadMaxFilesize", array()), "html", null, true);
            echo "</span>
        ";
        }
        // line 87
        echo "      </td>
      <td>2M</td>
    </tr>

    <tr>
      <td>
        ";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.form_data_size"), "html", null, true);
        echo "
        <div class=\"text-muted\">";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.form_data_size_tips"), "html", null, true);
        echo "</div>
      </td>
      <td>";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.upload_file_size_greater_than_200m"), "html", null, true);
        echo "</td>
      <td>
        ";
        // line 98
        if ($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "postMaxsizeOk", array())) {
            // line 99
            echo "          <span class=\"text-success\">√ ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "postMaxsize", array()), "html", null, true);
            echo "</span>
        ";
        } else {
            // line 101
            echo "          <span class=\"text-danger\">X ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "postMaxsize", array()), "html", null, true);
            echo "</span>
        ";
        }
        // line 103
        echo "      </td>
      <td>8M</td>
    </tr>

    <tr>
      <td>
        ";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.php_max_run_time"), "html", null, true);
        echo "
        <div class=\"text-muted\">";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.php_max_run_time_tips"), "html", null, true);
        echo "</div>
      </td>
      <td>";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.upload_file_size_greater_than_300m"), "html", null, true);
        echo "</td>
      <td>
        ";
        // line 114
        if ($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "maxExecutionTimeOk", array())) {
            // line 115
            echo "          <span class=\"text-success\">√ ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.php_max_execution_time", array("%maxExecutionTime%" => $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "maxExecutionTime", array()))), "html", null, true);
            echo "</span>
        ";
        } else {
            // line 117
            echo "          <span class=\"text-danger\">X ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.php_max_execution_time", array("%maxExecutionTime%" => $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "maxExecutionTime", array()))), "html", null, true);
            echo "</span>
        ";
        }
        // line 119
        echo "      </td>
      <td>";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.upload_file_execution_time_30s"), "html", null, true);
        echo "</td>
    </tr>

    <tr>
      <td>
        ";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.php_extend"), "html", null, true);
        echo "：mbstring
        <div class=\"text-muted\">";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.php__mbstring_extend_tips"), "html", null, true);
        echo "</div>
      </td>
      <td>";
        // line 128
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.must"), "html", null, true);
        echo "</td>
      <td>
        ";
        // line 130
        if ($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "mbstringOk", array())) {
            // line 131
            echo "          <span class=\"text-success\">√ ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.alerday_install"), "html", null, true);
            echo "</span>
        ";
        } else {
            // line 133
            echo "          <span class=\"text-danger\">X ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.no_install"), "html", null, true);
            echo "</span>
        ";
        }
        // line 135
        echo "      </td>
      <td>";
        // line 136
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.must"), "html", null, true);
        echo "</td>
    </tr>

    <tr>
      <td>
        ";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.php_extend"), "html", null, true);
        echo "：curl
        <div class=\"text-muted\">";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.php__curl_extend_tips"), "html", null, true);
        echo "</div>
      </td>
      <td>";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.default.must"), "html", null, true);
        echo "</td>
      <td>
        ";
        // line 146
        if ($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "curlOk", array())) {
            // line 147
            echo "          <span class=\"text-success\">√ ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.alerday_install"), "html", null, true);
            echo "</span>
        ";
        } else {
            // line 149
            echo "          <span class=\"text-danger\">X ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.no_install"), "html", null, true);
            echo "</span>
        ";
        }
        // line 151
        echo "      </td>
      <td>";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.must"), "html", null, true);
        echo "</td>
    </tr>

    <tr>
      <td>
        ";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.php_extend"), "html", null, true);
        echo "：GD
        <div class=\"text-muted\">";
        // line 158
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.php__gd_extend_tips"), "html", null, true);
        echo "</div>
      </td>
      <td>";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.default.must"), "html", null, true);
        echo "</td>
      <td>
        ";
        // line 162
        if ($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "gdOk", array())) {
            // line 163
            echo "          <span class=\"text-success\">√ ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.alerday_install"), "html", null, true);
            echo "</span>
        ";
        } else {
            // line 165
            echo "          <span class=\"text-danger\">X ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.no_install"), "html", null, true);
            echo "</span>
        ";
        }
        // line 167
        echo "      </td>
      <td>";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.must"), "html", null, true);
        echo "</td>
    </tr>
    </tbody>
  </table>

  <table class=\"table table-hover table-striped table-bordered\">
    <thead>
    <tr>
      <th width=\"60%\">";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.system_communication"), "html", null, true);
        echo "</th>
      <th width=\"40%\">";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.system_status"), "html", null, true);
        echo "</th>
    </tr>
    </thead>
    <tbody>
    <tr>
      <td>";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.system_communication_with_discuz"), "html", null, true);
        echo "</td>
      <td><span class=\"text-muted js-ucenter-check\" data-url=\"";
        // line 183
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_report_status_ucenter");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.system_checking"), "html", null, true);
        echo "</span></td>
    </tr>
    ";
        // line 185
        if ((_twig_default_filter($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("cloud_email.status"), "disable") == "disable")) {
            // line 186
            echo "      <tr>
        <td>";
            // line 187
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.system_email_send"), "html", null, true);
            echo " <i class=\"glyphicon glyphicon-info-sign text-info\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.system_email_send_tips"), "html", null, true);
            echo "\"></i></td>
        <td><span class=\"text-muted js-email-send-check\" data-url=\"";
            // line 188
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_report_status_email_send");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.system_checking"), "html", null, true);
            echo "</span>
        </td>
      </tr>
    ";
        }
        // line 192
        echo "    </tbody>
  </table>

  <div style=\"overflow:auto;max-height:400px;word-break:break-all;\">
    <table class=\"table table-hover table-striped table-bordered\" id=\"direcory-check-table\"
           data-url=\"";
        // line 197
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_report_status_directory");
        echo "\">
      <thead>
      <tr>
        <th width=\"60%\">";
        // line 200
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.system_file_and_catalog_authority"), "html", null, true);
        echo "</th>
        <th width=\"20%\">";
        // line 201
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.current_status"), "html", null, true);
        echo "</th>
        <th width=\"20%\">";
        // line 202
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.need_status"), "html", null, true);
        echo "</th>
      </tr>
      </thead>
      <tbody>
      <tr>
        <td colspan=\"6\" style=\"text-align: center;color: #c1c1c1;padding: 50px;\">";
        // line 207
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.system_scanning_file"), "html", null, true);
        echo "</td>
      </tr>
      </tbody>
    </table>
  </div>

  <div style=\"overflow:auto;max-height:400px;word-break:break-all;\">
    <table class=\"table table-hover table-striped table-bordered\" id=\"direcory-check-table\"
           data-url=\"";
        // line 215
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_report_status_directory");
        echo "\">
      <thead>
      <tr>
        <th width=\"30%\">";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.system_space_occupancy"), "html", null, true);
        echo "</th>
        <th width=\"20%\">";
        // line 219
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.system_available_space"), "html", null, true);
        echo "</th>
        <th width=\"25%\">";
        // line 220
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.system_total_space"), "html", null, true);
        echo "</th>
        <th width=\"25%\">";
        // line 221
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.reports_status.system_surplus_space"), "html", null, true);
        echo "</th>
      </tr>
      </thead>
      <tbody>
        ";
        // line 225
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["systemDiskUsage"]) ? $context["systemDiskUsage"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["diskUsage"]) {
            // line 226
            echo "          <tr>
            <td>
              ";
            // line 228
            echo twig_escape_filter($this->env, $this->getAttribute($context["diskUsage"], "name", array()), "html", null, true);
            echo "
              <a class=\"glyphicon glyphicon-question-sign text-muted pull-center\" data-toggle=\"popover\"
                 data-trigger=\"hover\" data-placement=\"top\" data-content=\"";
            // line 230
            echo twig_escape_filter($this->env, $this->getAttribute($context["diskUsage"], "title", array()), "html", null, true);
            echo "\">
              </a>
            </td>
            <td> ";
            // line 233
            echo twig_escape_filter($this->env, $this->getAttribute($context["diskUsage"], "free", array()), "html", null, true);
            echo " </td>
            <td> ";
            // line 234
            echo twig_escape_filter($this->env, $this->getAttribute($context["diskUsage"], "total", array()), "html", null, true);
            echo " </td>
            <td> ";
            // line 235
            echo twig_escape_filter($this->env, $this->getAttribute($context["diskUsage"], "rate", array()), "html", null, true);
            echo " </td>
          </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['diskUsage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 238
        echo "      </tbody>
    </table>
  </div>


";
    }

    public function getTemplateName()
    {
        return "admin/system/report/status.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  562 => 238,  553 => 235,  549 => 234,  545 => 233,  539 => 230,  534 => 228,  530 => 226,  526 => 225,  519 => 221,  515 => 220,  511 => 219,  507 => 218,  501 => 215,  490 => 207,  482 => 202,  478 => 201,  474 => 200,  468 => 197,  461 => 192,  452 => 188,  446 => 187,  443 => 186,  441 => 185,  434 => 183,  430 => 182,  422 => 177,  418 => 176,  407 => 168,  404 => 167,  398 => 165,  392 => 163,  390 => 162,  385 => 160,  380 => 158,  376 => 157,  368 => 152,  365 => 151,  359 => 149,  353 => 147,  351 => 146,  346 => 144,  341 => 142,  337 => 141,  329 => 136,  326 => 135,  320 => 133,  314 => 131,  312 => 130,  307 => 128,  302 => 126,  298 => 125,  290 => 120,  287 => 119,  281 => 117,  275 => 115,  273 => 114,  268 => 112,  263 => 110,  259 => 109,  251 => 103,  245 => 101,  239 => 99,  237 => 98,  232 => 96,  227 => 94,  223 => 93,  215 => 87,  209 => 85,  203 => 83,  201 => 82,  196 => 80,  191 => 78,  187 => 77,  179 => 72,  176 => 71,  170 => 69,  164 => 67,  162 => 66,  157 => 64,  149 => 59,  146 => 58,  140 => 56,  134 => 54,  132 => 53,  127 => 51,  123 => 50,  114 => 44,  108 => 41,  101 => 36,  95 => 34,  89 => 32,  87 => 31,  77 => 28,  68 => 22,  62 => 19,  53 => 13,  49 => 12,  45 => 11,  41 => 10,  36 => 7,  33 => 6,  29 => 1,  27 => 4,  25 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/system/report/status.html.twig", "/var/www/edusoho/app/Resources/views/admin/system/report/status.html.twig");
    }
}
