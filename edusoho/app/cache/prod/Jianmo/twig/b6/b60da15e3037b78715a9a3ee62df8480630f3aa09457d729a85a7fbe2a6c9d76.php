<?php

/* admin/navigation/navigation-modal.html.twig */
class __TwigTemplate_709081961d535233d60991a131676170af40bad004c0626bf1d611e5eeae85a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("old-bootstrap-modal-layout.html.twig", "admin/navigation/navigation-modal.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "old-bootstrap-modal-layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["block"] = ((array_key_exists("block", $context)) ? (_twig_default_filter((isset($context["block"]) ? $context["block"] : null), null)) : (null));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        // line 5
        echo "  ";
        if ((($this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "id", array()) == 0) && ($this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "type", array()) == "top"))) {
            // line 6
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.navigation.top.add"), "html", null, true);
            echo "
  ";
        } elseif ((($this->getAttribute(        // line 7
(isset($context["navigation"]) ? $context["navigation"] : null), "id", array()) > 0) && ($this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "type", array()) == "top"))) {
            // line 8
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.navigation.top.edit"), "html", null, true);
            echo "
  ";
        } elseif ((($this->getAttribute(        // line 9
(isset($context["navigation"]) ? $context["navigation"] : null), "id", array()) == 0) && ($this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "type", array()) == "foot"))) {
            // line 10
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.navigation.foot.add"), "html", null, true);
            echo "
  ";
        } elseif ((($this->getAttribute(        // line 11
(isset($context["navigation"]) ? $context["navigation"] : null), "id", array()) > 0) && ($this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "type", array()) == "foot"))) {
            // line 12
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.navigation.foot.edit"), "html", null, true);
            echo "
  ";
        } elseif ((($this->getAttribute(        // line 13
(isset($context["navigation"]) ? $context["navigation"] : null), "id", array()) == 0) && ($this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "type", array()) == "friendlyLink"))) {
            // line 14
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.navigation.friendlyLink.add"), "html", null, true);
            echo "
  ";
        } elseif ((($this->getAttribute(        // line 15
(isset($context["navigation"]) ? $context["navigation"] : null), "id", array()) > 0) && ($this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "type", array()) == "friendlyLink"))) {
            // line 16
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.navigation.friendlyLink.edit"), "html", null, true);
            echo "
  ";
        }
        // line 18
        echo "
";
    }

    // line 21
    public function block_body($context, array $blocks = array())
    {
        // line 22
        echo "
  <form class=\"form-horizontal\" id=\"navigation-form\"
      ";
        // line 24
        if (($this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "id", array()) == 0)) {
            // line 25
            echo "        action=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_navigation_create");
            echo "\"
      ";
        } else {
            // line 27
            echo "        action=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_navigation_update", array("id" => $this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "id", array()))), "html", null, true);
            echo "\"
      ";
        }
        // line 29
        echo "        method=\"post\">

    ";
        // line 31
        if ((isset($context["parentNavigation"]) ? $context["parentNavigation"] : null)) {
            // line 32
            echo "      <div class=\"row form-group\">
        <div class=\"col-md-3 control-label\"><label>";
            // line 33
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.navigation.parentNavigation"), "html", null, true);
            echo "</label></div>
        <div class=\"col-md-8 controls\">
          ";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["parentNavigation"]) ? $context["parentNavigation"] : null), "name", array()), "html", null, true);
            echo "
          <div class=\"help-block\">";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.navigation.parentNavigation.help_block"), "html", null, true);
            echo "</div>
        </div>
      </div>
    ";
        }
        // line 40
        echo "

    <div class=\"row form-group\">
      <div class=\"col-md-3 control-label\"><label for=\"name\">";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.navigation.name"), "html", null, true);
        echo "</label></div>
      <div class=\"col-md-8 controls\">
        <input class=\"form-control\" type=\"text\" id=\"name\" value=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "name", array()), "html", null, true);
        echo "\" name=\"name\"></div>
    </div>
    <p></p>

    <div class=\"row form-group\">
      <div class=\"col-md-3 control-label\"><label for=\"url\">";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.navigation.url"), "html", null, true);
        echo "</label></div>
      <div class=\"col-md-8 controls\">
        <input class=\"form-control width-input width-input-large\" type=\"text\" id=\"url\" ";
        // line 52
        if ($this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "url", array())) {
            echo " value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "url", array()), "html", null, true);
            echo "\" ";
        } else {
            echo " value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "getScheme", array(), "method"), "html", null, true);
            echo "://\" ";
        }
        echo " name=\"url\" >
      </div>
    </div>

    <p></p>

    <input type=\"hidden\" name=\"type\" value=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "type", array()), "html", null, true);
        echo "\">

    <div class=\"row form-group\">
      <div class=\"col-md-3 control-label\"><label>";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.navigation.new_windows"), "html", null, true);
        echo "</label></div>
      <div class=\"col-md-8 controls radios\">
        <div id=\"isNewWin\">
          <input type=\"radio\" name=\"isNewWin\" value=\"0\"
              ";
        // line 65
        if (($this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "isNewWin", array()) == 0)) {
            echo " checked=\"checked\" ";
        }
        echo ">
          <label>";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.navigation.isNewWin.no"), "html", null, true);
        echo "</label>
          <input type=\"radio\" name=\"isNewWin\" value=\"1\"
              ";
        // line 68
        if (($this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "isNewWin", array()) == 1)) {
            echo " checked=\"checked\" ";
        }
        echo ">
          <label>";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.navigation.isNewWin.yes"), "html", null, true);
        echo "</label>
        </div>
      </div>
    </div>

    <div class=\"row form-group\">
      <div class=\"col-md-3 control-label\">
        <label>";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.navigation.status"), "html", null, true);
        echo "</label>
      </div>
      <div class=\"col-md-8 controls radios\">
        <div id=\"isOpen\">
          <input type=\"radio\" name=\"isOpen\" value=\"1\"
              ";
        // line 81
        if (($this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "isOpen", array()) == 1)) {
            echo " checked=\"checked\" ";
        }
        echo ">
          <label>";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.navigation.status.open"), "html", null, true);
        echo "</label>
          <input type=\"radio\" name=\"isOpen\" value=\"0\"
              ";
        // line 84
        if (($this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "isOpen", array()) == 0)) {
            echo " checked=\"checked\" ";
        }
        echo ">
          <label>";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.navigation.status.closed"), "html", null, true);
        echo "</label>
        </div>
      </div>
    </div>
    <input type=\"hidden\" name=\"parentId\" value=\"";
        // line 89
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["parentNavigation"]) ? $context["parentNavigation"] : null), "id", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["parentNavigation"]) ? $context["parentNavigation"] : null), "id", array()), 0)) : (0)), "html", null, true);
        echo "\">
    <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderCsrfToken("site"), "html", null, true);
        echo "\">

  </form>

  <script type=\"text/javascript\">
    ";
        // line 95
        if (($this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "id", array()) == 0)) {
            // line 96
            echo "      app.load('navigation/create-modal')
    ";
        } else {
            // line 98
            echo "      app.load('navigation/edit-modal')
    ";
        }
        // line 100
        echo "  </script>

";
    }

    // line 104
    public function block_footer($context, array $blocks = array())
    {
        // line 105
        echo "  <button type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.cancel"), "html", null, true);
        echo "</button>
  <button id=\"navigation-save-btn\" data-submiting-text=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.submit.submiting"), "html", null, true);
        echo "\" type=\"submit\" class=\"btn btn-primary\"
          data-toggle=\"form-submit\" data-target=\"#navigation-form\">";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.save"), "html", null, true);
        echo "</button>
";
    }

    public function getTemplateName()
    {
        return "admin/navigation/navigation-modal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  283 => 107,  279 => 106,  274 => 105,  271 => 104,  265 => 100,  261 => 98,  257 => 96,  255 => 95,  247 => 90,  243 => 89,  236 => 85,  230 => 84,  225 => 82,  219 => 81,  211 => 76,  201 => 69,  195 => 68,  190 => 66,  184 => 65,  177 => 61,  171 => 58,  154 => 52,  149 => 50,  141 => 45,  136 => 43,  131 => 40,  124 => 36,  120 => 35,  115 => 33,  112 => 32,  110 => 31,  106 => 29,  100 => 27,  94 => 25,  92 => 24,  88 => 22,  85 => 21,  80 => 18,  74 => 16,  72 => 15,  67 => 14,  65 => 13,  60 => 12,  58 => 11,  53 => 10,  51 => 9,  46 => 8,  44 => 7,  39 => 6,  36 => 5,  33 => 4,  29 => 1,  27 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/navigation/navigation-modal.html.twig", "/var/www/edusoho/app/Resources/views/admin/navigation/navigation-modal.html.twig");
    }
}
