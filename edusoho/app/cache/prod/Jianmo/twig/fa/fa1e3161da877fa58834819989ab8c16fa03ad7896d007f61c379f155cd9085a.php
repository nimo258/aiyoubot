<?php

/* v2/sidebar.html.twig */
class __TwigTemplate_94932e151b64926fe13ceb947b726ee0223831310ecf58d00c7913a0bfa9953c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"cd-sidebar\">
  <ul class=\"cd-sidebar__list\">
    <li class=\"cd-sidebar__heading\">
      ";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.account_center"), "html", null, true);
        echo "
    </li>
    <li class=\"cd-sidebar__item ";
        // line 6
        if (((isset($context["side_nav"]) ? $context["side_nav"] : null) == "my-orders")) {
            echo " active ";
        }
        echo "\">
      <a href=\"";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("my_orders");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.order_menu"), "html", null, true);
        echo "</a>
    </li>
    ";
        // line 9
        if ($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("coin.coin_enabled", 0)) {
            // line 10
            echo "      <li class=\"cd-sidebar__item  ";
            if (((isset($context["side_nav"]) ? $context["side_nav"] : null) == "my-coin")) {
                echo " active ";
            }
            echo "\">
        <a href=\"";
            // line 11
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("my_coin");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.account.my_coin", array("%coin_name%" => $this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("coin.coin_name"))), "html", null, true);
            echo "</a>
      </li>
    ";
        }
        // line 14
        echo "    ";
        if ($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("invite.invite_code_setting", 0)) {
            // line 15
            echo "      <li class=\"cd-sidebar__item ";
            if (((isset($context["side_nav"]) ? $context["side_nav"] : null) == "my-invite-code")) {
                echo " active ";
            }
            echo "\">
        <a href=\"";
            // line 16
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("my_invite_code");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.account.invite_code"), "html", null, true);
            echo "</a>
      </li>
    ";
        }
        // line 19
        echo "    <li class=\"cd-sidebar__item ";
        if (((isset($context["side_nav"]) ? $context["side_nav"] : null) == "my-coupon")) {
            echo " active ";
        }
        echo "\">
      <a href=\"";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("my_cards");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("plugin.coupon"), "html", null, true);
        echo "</a>
    </li>
    ";
        // line 22
        echo $this->env->getExtension('Codeages\PluginBundle\Twig\SlotExtension')->slot("my.account.menu.extension", array("sideNav" => (isset($context["side_nav"]) ? $context["side_nav"] : null)));
        echo "
    ";
        // line 23
        echo $this->env->getExtension('Codeages\PluginBundle\Twig\SlotExtension')->slot("my.account.menu.extension.invoice", array("sideNav" => (isset($context["side_nav"]) ? $context["side_nav"] : null)));
        echo "

    ";
        // line 25
        if ($this->env->getExtension('AppBundle\Twig\WebExtension')->isPluginInstalled("BusinessDrainage")) {
            // line 26
            echo "      <li class=\"cd-sidebar__item ";
            if (((isset($context["side_nav"]) ? $context["side_nav"] : null) == "my-exchange")) {
                echo " active ";
            }
            echo "\">
        <a href=\"";
            // line 27
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("business_drainage_ecard_my_exchange");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("兑换卡券"), "html", null, true);
            echo "</a>
      </li>
    ";
        }
        // line 30
        echo "
    <li class=\"cd-sidebar__heading\">
      ";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.settings.menu_heading"), "html", null, true);
        echo "
    </li>
    <li class=\"cd-sidebar__item ";
        // line 34
        if (((isset($context["side_nav"]) ? $context["side_nav"] : null) == "profile")) {
            echo "active";
        }
        echo "\">
      <a href=\"";
        // line 35
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("settings");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.settings.personal_info_menu"), "html", null, true);
        echo "</a>
    </li>
    <li class=\"cd-sidebar__item ";
        // line 37
        if (((isset($context["side_nav"]) ? $context["side_nav"] : null) == "approval")) {
            echo "active";
        }
        echo "\">
      <a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("setting_approval_submit");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.settings.verification_menu"), "html", null, true);
        echo "</a>
    </li>
    <li class=\"cd-sidebar__item ";
        // line 40
        if (((isset($context["side_nav"]) ? $context["side_nav"] : null) == "security")) {
            echo "active";
        }
        echo "\">
      <a href=\"";
        // line 41
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("settings_security");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.settings.security_menu"), "html", null, true);
        echo "</a>
    </li>
    ";
        // line 43
        if ($this->env->getExtension('AppBundle\Twig\WebExtension')->isPluginInstalled("TeacherAudit")) {
            echo "   
    <li class=\"cd-sidebar__item ";
            // line 44
            if (((isset($context["side_nav"]) ? $context["side_nav"] : null) == "teacheraudit")) {
                echo "active";
            }
            echo "\">
      <a href=\"";
            // line 45
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("teacher_audit_submit");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.settings.teachers_qualification_menu"), "html", null, true);
            echo "</a>
    </li>
    ";
        }
        // line 48
        echo "    ";
        if ((($this->getAttribute($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("login_bind", array()), "enabled", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("login_bind", array()), "enabled", array()), false)) : (false))) {
            // line 49
            echo "      <li class=\"cd-sidebar__item ";
            if (((isset($context["side_nav"]) ? $context["side_nav"] : null) == "binds")) {
                echo "active";
            }
            echo "\" >
        <a href=\"";
            // line 50
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("settings_binds");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.settings.oauth_menu"), "html", null, true);
            echo "</a>
      </li>
    ";
        }
        // line 53
        echo "  </ul>
</div>";
    }

    public function getTemplateName()
    {
        return "v2/sidebar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 53,  193 => 50,  186 => 49,  183 => 48,  175 => 45,  169 => 44,  165 => 43,  158 => 41,  152 => 40,  145 => 38,  139 => 37,  132 => 35,  126 => 34,  121 => 32,  117 => 30,  109 => 27,  102 => 26,  100 => 25,  95 => 23,  91 => 22,  84 => 20,  77 => 19,  69 => 16,  62 => 15,  59 => 14,  51 => 11,  44 => 10,  42 => 9,  35 => 7,  29 => 6,  24 => 4,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "v2/sidebar.html.twig", "/var/www/edusoho/app/Resources/views/v2/sidebar.html.twig");
    }
}
