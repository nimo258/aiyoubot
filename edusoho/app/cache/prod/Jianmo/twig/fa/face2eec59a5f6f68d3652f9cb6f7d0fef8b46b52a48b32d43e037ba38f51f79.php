<?php

/* admin/theme/theme-edit-config-li.html.twig */
class __TwigTemplate_088547652b888fb1bd68ca7f3198510657aa35e292f03f02fef99f84baa42de3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<li class=\"list-group-item clearfix  ";
        if (($this->getAttribute((isset($context["pendant"]) ? $context["pendant"] : null), "code", array()) != "footer-link")) {
            echo "theme-edit-item";
        }
        echo "\" id=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pendant"]) ? $context["pendant"] : null), "id", array()), "html", null, true);
        echo "\">
  <div class=\"col-md-4 \">
    ";
        // line 3
        if (($this->getAttribute((isset($context["pendant"]) ? $context["pendant"] : null), "code", array()) != "footer-link")) {
            // line 4
            echo "      <span class=\"glyphicon glyphicon-move edit-sort\"></span>
    ";
        } else {
            // line 6
            echo "      <span class=\"glyphicon glyphicon-lock  edit-sort\"></span>
    ";
        }
        // line 8
        echo "    <label>
      <input type=\"checkbox\" data-component-id=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pendant"]) ? $context["pendant"] : null), "id", array()), "html", null, true);
        echo "\" class=\"check-block\" ";
        if (((array_key_exists("isChoiced", $context)) ? (_twig_default_filter((isset($context["isChoiced"]) ? $context["isChoiced"] : null), null)) : (null))) {
            echo "checked";
        }
        echo ">
      <span class=\"default-title\">";
        // line 10
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["orginAllConfig"]) ? $context["orginAllConfig"] : null), (isset($context["key"]) ? $context["key"] : null), array(), "array", false, true), "defaultTitle", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["orginAllConfig"]) ? $context["orginAllConfig"] : null), (isset($context["key"]) ? $context["key"] : null), array(), "array", false, true), "defaultTitle", array()), "")) : ("")), "html", null, true);
        echo "</span>
    </label>
  </div>
  <div class=\"col-md-5\">
    ";
        // line 14
        if ((($this->getAttribute((isset($context["pendant"]) ? $context["pendant"] : null), "title", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["pendant"]) ? $context["pendant"] : null), "title", array()), null)) : (null))) {
            // line 15
            echo "      ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pendant"]) ? $context["pendant"] : null), "title", array()), "html", null, true);
            echo "
    ";
        } else {
            // line 17
            echo "      ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pendant"]) ? $context["pendant"] : null), "defaultTitle", array()), "html", null, true);
            echo "
    ";
        }
        // line 19
        echo "  </div>
  <div class=\"col-md-3 text-right\">
    ";
        // line 21
        $context["blockPendant"] = array(0 => "middle-banner", 1 => "footer-link", 2 => "advertisement-banner");
        // line 22
        echo "    ";
        if (twig_in_filter($this->getAttribute((isset($context["pendant"]) ? $context["pendant"] : null), "code", array()), (isset($context["blockPendant"]) ? $context["blockPendant"] : null))) {
            // line 23
            echo "      <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_block", array("category" => _twig_default_filter("theme", "all"))), "html", null, true);
            echo "\" target=\"_blank\" class=\"btn btn-primary btn-sm item-set-btn\" ";
            if ( !((array_key_exists("isChoiced", $context)) ? (_twig_default_filter((isset($context["isChoiced"]) ? $context["isChoiced"] : null))) : (""))) {
                echo "style=\"display:none;\"";
            }
            echo ">
        <span class=\"glyphicon glyphicon-wrench\"></span> 
        ";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.edit.setting_btn"), "html", null, true);
            echo "
      </a>
    ";
        } elseif (($this->getAttribute(        // line 27
(isset($context["pendant"]) ? $context["pendant"] : null), "code", array()) == "friend-link")) {
            // line 28
            echo "      <a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_navigation", array("type" => "friendlyLink"));
            echo "\" target=\"_blank\" class=\"btn btn-primary btn-sm item-set-btn\" ";
            if ( !((array_key_exists("isChoiced", $context)) ? (_twig_default_filter((isset($context["isChoiced"]) ? $context["isChoiced"] : null))) : (""))) {
                echo "style=\"display:none;\"";
            }
            echo ">
        <span class=\"glyphicon glyphicon-wrench\"></span> 
        ";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.edit.setting_btn"), "html", null, true);
            echo "
      </a>
    ";
        } else {
            // line 33
            echo "      <button class=\"btn btn-primary btn-sm item-edit-btn\" ";
            if ( !((array_key_exists("isChoiced", $context)) ? (_twig_default_filter((isset($context["isChoiced"]) ? $context["isChoiced"] : null), null)) : (null))) {
                echo "style=\"display:none;\"";
            }
            echo " data-title=\"Edit\" data-url=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_themes_config_edit");
            echo "\"><span class=\"glyphicon glyphicon-pencil\"></span> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.edit_btn"), "html", null, true);
            echo "</button>
    ";
        }
        // line 35
        echo "  </div>
</li>";
    }

    public function getTemplateName()
    {
        return "admin/theme/theme-edit-config-li.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 35,  113 => 33,  107 => 30,  97 => 28,  95 => 27,  90 => 25,  80 => 23,  77 => 22,  75 => 21,  71 => 19,  65 => 17,  59 => 15,  57 => 14,  50 => 10,  42 => 9,  39 => 8,  35 => 6,  31 => 4,  29 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/theme/theme-edit-config-li.html.twig", "/var/www/edusoho/app/Resources/views/admin/theme/theme-edit-config-li.html.twig");
    }
}
