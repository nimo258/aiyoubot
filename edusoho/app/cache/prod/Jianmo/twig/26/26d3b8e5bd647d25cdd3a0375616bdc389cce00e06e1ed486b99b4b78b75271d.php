<?php

/* admin/block/block-visual-edit.html.twig */
class __TwigTemplate_fd7e7e20879a6f7e883dd8fd59cb9a116e04c1921a6f317dda55c1189031a827 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/block/block-visual-layout.html.twig", "admin/block/block-visual-edit.html.twig", 1);
        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'blockVisual' => array($this, 'block_blockVisual'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/block/block-visual-layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["menu"] = ((array_key_exists("menu", $context)) ? (_twig_default_filter((isset($context["menu"]) ? $context["menu"] : null), "admin_block")) : ("admin_block"));
        // line 4
        $context["currentPage"] = "admin_block_visual_edit";
        // line 8
        $context["script_controller"] = "block/visual-edit";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_page_title($context, array $blocks = array())
    {
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.title"), "html", null, true);
        echo " ";
    }

    // line 10
    public function block_blockVisual($context, array $blocks = array())
    {
        // line 11
        echo "  <style type=\"text/css\">
    .sortable-list .placeholder {
      margin: 10px 0;
      width: 100%;
      height: 40px;
      background: #FCFFC0;
      border: 1px dashed #ccc;
      list-style: none;
    }
    .nav.sortable-list .placeholder {
      width: 80px;
      padding: 10px 15px;
      background: #eee;
      margin: 0;
    }
    .nav.nav-pills .dragged{
      position: absolute!important;
    }
  </style>
  ";
        // line 30
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "debug", array())) {
            // line 31
            echo "    <a class=\"btn btn-default btn-sm pull-right\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_block_visual_view_data", array("blockTemplateId" => $this->getAttribute((isset($context["block"]) ? $context["block"] : null), "blockTemplateId", array()))), "html", null, true);
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.watch_json_config"), "html", null, true);
            echo "</a>
  ";
        }
        // line 33
        echo "
    <!-- 以下是简墨新版的样式 -->
    <h4>";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["block"]) ? $context["block"] : null), "title", array()), "html", null, true);
        echo "</h4>
    <form id=\"block-edit-form\" class=\"visual-edit-form form-horizontal\" method=\"POST\" action=\"\">
      ";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["web_macro"]) ? $context["web_macro"] : null), "flash_messages", array(), "method"), "html", null, true);
        echo "
      ";
        // line 38
        $this->loadTemplate("admin/block/block-visual-items.html.twig", "admin/block/block-visual-edit.html.twig", 38)->display($context);
        // line 39
        echo "    </form>
    <!-- 以上是新版的样式 -->

  <!-- <div class=\"col-md-4\">
    ";
        // line 43
        $this->loadTemplate("admin/block/affix-nav.html.twig", "admin/block/block-visual-edit.html.twig", 43)->display($context);
        // line 44
        echo "  </div> -->

";
    }

    public function getTemplateName()
    {
        return "admin/block/block-visual-edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 44,  99 => 43,  93 => 39,  91 => 38,  87 => 37,  82 => 35,  78 => 33,  70 => 31,  68 => 30,  47 => 11,  44 => 10,  36 => 6,  32 => 1,  30 => 8,  28 => 4,  26 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/block/block-visual-edit.html.twig", "/var/www/edusoho/app/Resources/views/admin/block/block-visual-edit.html.twig");
    }
}
