<?php

/* admin/app/center.html.twig */
class __TwigTemplate_6a340ff9a4cc9679e8c3f473907d1b1068fd674a621b551f1f0faccd430d28dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/app/center.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["menu"] = "admin_app_center";
        // line 5
        $context["script_controller"] = "app/center";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        // line 8
        echo "  <div class=\"mbl btn-group\">
    <a href=\"";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("admin_app_center", array("postStatus" => "all"));
        echo "\" type=\"button\"
       class=\"btn btn-default btn-sm ";
        // line 10
        if (((isset($context["type"]) ? $context["type"] : null) == "all")) {
            echo " btn-primary";
        }
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.type.all"), "html", null, true);
        echo "</a>
    <a href=\" ";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("admin_app_center", array("postStatus" => "theme"));
        echo "\" type=\"button\"
       class=\"btn btn-default btn-sm ";
        // line 12
        if (((isset($context["type"]) ? $context["type"] : null) == "theme")) {
            echo "btn-primary";
        }
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.type.theme"), "html", null, true);
        echo "</a>
    <a href=\" ";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("admin_app_center", array("postStatus" => "app"));
        echo "\" type=\"button\"
       class=\"btn btn-default btn-sm ";
        // line 14
        if (((isset($context["type"]) ? $context["type"] : null) == "app")) {
            echo "btn-primary";
        }
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.type.app"), "html", null, true);
        echo "</a>
  </div>

  <div class=\"checkbox-group appTypeChoices\" RepeatDirection=\"Horizontal\" id=\"appTypeChoices\" name=\"appTypeChoices\"
       style=\"float:right;margin-top:-45px;\">
    <div class=\"sortedCourses\">
      <input type=\"checkbox\" name=\"appTypeChoices\" id=\"installedApps\"
             value=\"installedApps\" ";
        // line 21
        if ((((array_key_exists("appTypeChoices", $context)) ? (_twig_default_filter((isset($context["appTypeChoices"]) ? $context["appTypeChoices"] : null), null)) : (null)) == "installedApps")) {
            echo " data-url=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_app_center", array("postStatus" => (isset($context["type"]) ? $context["type"] : null), "showType" => "unhidden")), "html", null, true);
            echo "\"  checked ";
        } else {
            echo " data-url=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_app_center", array("postStatus" => (isset($context["type"]) ? $context["type"] : null), "showType" => "hidden")), "html", null, true);
            echo "\" ";
        }
        echo ">
      ";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.buyed_app_hidden"), "html", null, true);
        echo "
      <input type=\"hidden\" name=\"type\" value='";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : null), "html", null, true);
        echo "' id=\"type\">
    </div>
  </div>

  ";
        // line 27
        if ((((array_key_exists("status", $context)) ? (_twig_default_filter((isset($context["status"]) ? $context["status"] : null), null)) : (null)) == "error")) {
            // line 28
            echo "    <div class=\"alert alert-danger\">";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.error_tips", array("%cloudSettingUrl%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_setting_cloud")));
            echo "</div>
  ";
        } elseif ((((        // line 29
array_key_exists("status", $context)) ? (_twig_default_filter((isset($context["status"]) ? $context["status"] : null), null)) : (null)) == "unlink")) {
            // line 30
            echo "    <p class=\"alert alert-danger\">
      ";
            // line 31
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.edu_cloud.connect_failed_tips", array("%EduSoho%" => $this->env->getExtension('AppBundle\Twig\WebExtension')->removeCopyright("EduSoho"))), "html", null, true);
            echo "
    </p>
  ";
        } else {
            // line 34
            echo "    <table class=\"table table-striped table-hover\" id=\"app-table-container\">
      <thead>

      </thead>
      <tbody>

      ";
            // line 40
            if (((isset($context["type"]) ? $context["type"] : null) == "all")) {
                // line 41
                echo "        ";
                $context["apps"] = (isset($context["apps"]) ? $context["apps"] : null);
                // line 42
                echo "      ";
            } elseif (((isset($context["type"]) ? $context["type"] : null) == "theme")) {
                // line 43
                echo "        ";
                $context["apps"] = (isset($context["theme"]) ? $context["theme"] : null);
                // line 44
                echo "      ";
            } elseif (((isset($context["type"]) ? $context["type"] : null) == "app")) {
                // line 45
                echo "        ";
                $context["apps"] = (isset($context["allApp"]) ? $context["allApp"] : null);
                // line 46
                echo "      ";
            }
            // line 47
            echo "
      ";
            // line 48
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["apps"]) ? $context["apps"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["app"]) {
                // line 49
                echo "        ";
                $context["installedApp"] = (($this->getAttribute((isset($context["installedApps"]) ? $context["installedApps"] : null), $this->getAttribute($context["app"], "code", array()), array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["installedApps"]) ? $context["installedApps"] : null), $this->getAttribute($context["app"], "code", array()), array(), "array"), null)) : (null));
                // line 50
                echo "
        ";
                // line 51
                if ((((array_key_exists("appTypeChoices", $context)) ? (_twig_default_filter((isset($context["appTypeChoices"]) ? $context["appTypeChoices"] : null), null)) : (null)) == "installedApps")) {
                    // line 52
                    echo "
          ";
                    // line 53
                    if ((((($this->getAttribute($context["app"], "purchased", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["app"], "purchased", array()), false)) : (false)) == true) || (isset($context["installedApp"]) ? $context["installedApp"] : null))) {
                        // line 54
                        echo "
            ";
                        // line 55
                        if ((isset($context["installedApp"]) ? $context["installedApp"] : null)) {
                            // line 56
                            echo "            ";
                        } else {
                            // line 57
                            echo "              <tr>
                <td>
                  ";
                            // line 59
                            $this->loadTemplate("admin/app/app-detail.html.twig", "admin/app/center.html.twig", 59)->display($context);
                            // line 60
                            echo "                  <div class=\"right-info pull-right\">
                    <a href=\"#\" data-toggle=\"modal\" data-target=\"#modal\"
                       data-url=\"";
                            // line 62
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_app_package_update_modal", array("id" => $this->getAttribute($context["app"], "latestPackageId", array()), "type" => "install")), "html", null, true);
                            echo "\"
                       class=\"btn btn-primary app-btn\" data-keyboard=\"false\" data-backdrop=\"static\">";
                            // line 63
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.package_update_btn"), "html", null, true);
                            echo "</a>
                  </div>
                </td>
              </tr>
            ";
                        }
                        // line 68
                        echo "          ";
                    } else {
                        // line 69
                        echo "            <tr>
              <td>
                ";
                        // line 71
                        $this->loadTemplate("admin/app/app-detail.html.twig", "admin/app/center.html.twig", 71)->display($context);
                        // line 72
                        echo "
                <div class=\"right-info pull-right\">
                  ";
                        // line 74
                        if (((($this->getAttribute($context["app"], "userAccess", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["app"], "userAccess", array()), null)) : (null)) == "impossible")) {
                            // line 75
                            echo "                    <a href=\"http://open.edusoho.com\" class=\"user-access\" target=_blank>";
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.user_access"), "html", null, true);
                            echo "</a>
                  ";
                        } else {
                            // line 77
                            echo "                    ";
                            if (($this->getAttribute($context["app"], "price", array()) == 0)) {
                                // line 78
                                echo "                      <a href=\"#\" data-toggle=\"modal\" data-target=\"#modal\"
                         data-url=\"";
                                // line 79
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_app_package_update_modal", array("id" => $this->getAttribute($context["app"], "latestPackageId", array()), "type" => "install")), "html", null, true);
                                echo "\"
                         class=\"btn btn-primary app-btn\" data-keyboard=\"false\"
                         data-backdrop=\"static\">";
                                // line 81
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.package_update_btn"), "html", null, true);
                                echo "</a>
                    ";
                            } else {
                                // line 83
                                echo "                      <a href=\"http://open.edusoho.com/app/";
                                echo twig_escape_filter($this->env, $this->getAttribute($context["app"], "code", array()), "html", null, true);
                                echo "\" class=\"btn btn-warning app-btn\"
                         target=\"_blank\">";
                                // line 84
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.buy_btn"), "html", null, true);
                                echo "</a>
                    ";
                            }
                            // line 86
                            echo "                    <p class=\"mtm\"><span class=\"text-muted\">";
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.price"), "html", null, true);
                            echo "</span><span
                          class=\"price\"> ¥ ";
                            // line 87
                            if (($this->getAttribute($context["app"], "price", array()) > 0)) {
                                echo twig_escape_filter($this->env, $this->getAttribute($context["app"], "price", array()), "html", null, true);
                            } else {
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.free"), "html", null, true);
                            }
                            echo "</span>
                    </p>
                  ";
                        }
                        // line 90
                        echo "                </div>
              </td>
            </tr>
          ";
                    }
                    // line 94
                    echo "
        ";
                } else {
                    // line 96
                    echo "          <tr>
            <td>
              ";
                    // line 98
                    $this->loadTemplate("admin/app/app-detail.html.twig", "admin/app/center.html.twig", 98)->display($context);
                    // line 99
                    echo "              <div class=\"right-info pull-right\">

                ";
                    // line 101
                    if ((((($this->getAttribute($context["app"], "purchased", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["app"], "purchased", array()), false)) : (false)) == true) || (isset($context["installedApp"]) ? $context["installedApp"] : null))) {
                        // line 102
                        echo "
                  ";
                        // line 103
                        if ((isset($context["installedApp"]) ? $context["installedApp"] : null)) {
                            // line 104
                            echo "                    ";
                            if (((($this->getAttribute($context["app"], "ontrial", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["app"], "ontrial", array()), false)) : (false)) && ((($this->getAttribute($context["app"], "userAccess", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["app"], "userAccess", array()), null)) : (null)) != "ok"))) {
                                // line 105
                                echo "                      <p><a href=\"javascript:;\" class=\"btn btn-default disabled app-btn\">";
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.expired_btn"), "html", null, true);
                                echo "</a></p>
                      <p><a href=\"http://open.edusoho.com/app/";
                                // line 106
                                echo twig_escape_filter($this->env, $this->getAttribute($context["app"], "code", array()), "html", null, true);
                                echo "\" class=\"btn btn-warning app-btn\"
                            target=\"_blank\">";
                                // line 107
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.buy_btn"), "html", null, true);
                                echo "</a></p>
                    ";
                            } elseif (((($this->getAttribute(                            // line 108
$context["app"], "ontrial", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["app"], "ontrial", array()), false)) : (false)) && ((($this->getAttribute($context["app"], "purchased", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["app"], "purchased", array()), false)) : (false)) == false))) {
                                // line 109
                                echo "                      <p><a href=\"javascript:;\" class=\"btn btn-default disabled app-btn\">";
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.ontrial_btn"), "html", null, true);
                                echo "</a></p>
                      <p><a href=\"http://open.edusoho.com/app/";
                                // line 110
                                echo twig_escape_filter($this->env, $this->getAttribute($context["app"], "code", array()), "html", null, true);
                                echo "\" class=\"btn btn-warning app-btn\"
                            target=\"_blank\">";
                                // line 111
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.buy_btn"), "html", null, true);
                                echo "</a></p>
                    ";
                            } else {
                                // line 113
                                echo "                      <a href=\"javascript:;\" class=\"btn btn-default disabled app-btn\">";
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.buyed_btn"), "html", null, true);
                                echo "</a>
                    ";
                            }
                            // line 115
                            echo "                  ";
                        } else {
                            // line 116
                            echo "                    <a href=\"#\" data-toggle=\"modal\" data-target=\"#modal\"
                       data-url=\"";
                            // line 117
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_app_package_update_modal", array("id" => $this->getAttribute($context["app"], "latestPackageId", array()), "type" => "install")), "html", null, true);
                            echo "\"
                       class=\"btn btn-primary app-btn\" data-keyboard=\"false\" data-backdrop=\"static\">";
                            // line 118
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.package_update_btn"), "html", null, true);
                            echo "</a>
                  ";
                        }
                        // line 120
                        echo "
                  <p class=\"mtm\"><span class=\"text-muted\">";
                        // line 121
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.price"), "html", null, true);
                        echo "</span><span
                        class=\"price\"> ¥ ";
                        // line 122
                        if (($this->getAttribute($context["app"], "price", array()) > 0)) {
                            echo twig_escape_filter($this->env, $this->getAttribute($context["app"], "price", array()), "html", null, true);
                        } else {
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.free"), "html", null, true);
                        }
                        echo "</span>
                  </p>
                ";
                    } else {
                        // line 125
                        echo "
                  ";
                        // line 126
                        if ($this->getAttribute($context["app"], "buyable", array(), "any", true, true)) {
                            // line 127
                            echo "                    ";
                            $context["triable"] = (($this->getAttribute($context["app"], "triable", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["app"], "triable", array()), false)) : (false));
                            // line 128
                            echo "                    ";
                            if ((isset($context["triable"]) ? $context["triable"] : null)) {
                                // line 129
                                echo "                      <p><a href=\"#\" data-toggle=\"modal\" data-target=\"#modal\"
                            data-url=\"";
                                // line 130
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_app_package_update_modal", array("id" => $this->getAttribute($context["app"], "latestPackageId", array()), "type" => "install")), "html", null, true);
                                echo "\"
                            class=\"btn btn-primary app-btn\" data-keyboard=\"false\"
                            data-backdrop=\"static\">";
                                // line 132
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.tria_btn"), "html", null, true);
                                echo "</a></p>
                    ";
                            }
                            // line 134
                            echo "                    ";
                            if ($this->getAttribute($context["app"], "buyable", array())) {
                                // line 135
                                echo "                      <p><a href=\"http://open.edusoho.com/app/";
                                echo twig_escape_filter($this->env, $this->getAttribute($context["app"], "code", array()), "html", null, true);
                                echo "\" class=\"btn btn-warning app-btn\"
                            target=\"_blank\">";
                                // line 136
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.buy_btn"), "html", null, true);
                                echo "</a></p>
                    ";
                            } else {
                                // line 138
                                echo "                      <a href=\"http://open.edusoho.com/app/";
                                echo twig_escape_filter($this->env, $this->getAttribute($context["app"], "code", array()), "html", null, true);
                                echo "\" class=\"btn btn-warning app-btn\"
                         target=\"_blank\">";
                                // line 139
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.show_detail_btn"), "html", null, true);
                                echo "</a>
                    ";
                            }
                            // line 141
                            echo "                    <p class=\"mtm\"><span class=\"text-muted\">";
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.price"), "html", null, true);
                            echo "</span><span
                          class=\"price\"> ¥ ";
                            // line 142
                            if (($this->getAttribute($context["app"], "price", array()) > 0)) {
                                echo twig_escape_filter($this->env, $this->getAttribute($context["app"], "price", array()), "html", null, true);
                            } else {
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.free"), "html", null, true);
                            }
                            echo "</span>
                    </p>
                  ";
                        } else {
                            // line 145
                            echo "                    ";
                            if (((($this->getAttribute($context["app"], "userAccess", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["app"], "userAccess", array()), null)) : (null)) == "impossible")) {
                                // line 146
                                echo "                      <a href=\"http://www.edusoho.com/product/system\" class=\"user-access\"
                         target=_blank>";
                                // line 147
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.user_access"), "html", null, true);
                                echo "</a>
                    ";
                            } else {
                                // line 149
                                echo "                      ";
                                if (($this->getAttribute($context["app"], "price", array()) == 0)) {
                                    // line 150
                                    echo "                        <a href=\"#\" data-toggle=\"modal\" data-target=\"#modal\"
                           data-url=\"";
                                    // line 151
                                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_app_package_update_modal", array("id" => $this->getAttribute($context["app"], "latestPackageId", array()), "type" => "install")), "html", null, true);
                                    echo "\"
                           class=\"btn btn-primary app-btn\" data-keyboard=\"false\"
                           data-backdrop=\"static\">";
                                    // line 153
                                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.package_update_btn"), "html", null, true);
                                    echo "</a>
                      ";
                                } else {
                                    // line 155
                                    echo "                        <a href=\"http://open.edusoho.com/app/";
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["app"], "code", array()), "html", null, true);
                                    echo "\" class=\"btn btn-warning app-btn\"
                           target=\"_blank\">";
                                    // line 156
                                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.buy_btn"), "html", null, true);
                                    echo "</a>
                      ";
                                }
                                // line 158
                                echo "                      <p class=\"mtm\"><span class=\"text-muted\">";
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.price"), "html", null, true);
                                echo "</span><span
                            class=\"price\"> ¥ ";
                                // line 159
                                if (($this->getAttribute($context["app"], "price", array()) > 0)) {
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["app"], "price", array()), "html", null, true);
                                } else {
                                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.app_center.free"), "html", null, true);
                                }
                                echo "</span>
                      </p>
                    ";
                            }
                            // line 162
                            echo "                  ";
                        }
                        // line 163
                        echo "                ";
                    }
                    // line 164
                    echo "
              </div>
            </td>
          </tr>
        ";
                }
                // line 169
                echo "      ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['app'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 170
            echo "      </tbody>
    </table>
  ";
        }
    }

    public function getTemplateName()
    {
        return "admin/app/center.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  513 => 170,  499 => 169,  492 => 164,  489 => 163,  486 => 162,  476 => 159,  471 => 158,  466 => 156,  461 => 155,  456 => 153,  451 => 151,  448 => 150,  445 => 149,  440 => 147,  437 => 146,  434 => 145,  424 => 142,  419 => 141,  414 => 139,  409 => 138,  404 => 136,  399 => 135,  396 => 134,  391 => 132,  386 => 130,  383 => 129,  380 => 128,  377 => 127,  375 => 126,  372 => 125,  362 => 122,  358 => 121,  355 => 120,  350 => 118,  346 => 117,  343 => 116,  340 => 115,  334 => 113,  329 => 111,  325 => 110,  320 => 109,  318 => 108,  314 => 107,  310 => 106,  305 => 105,  302 => 104,  300 => 103,  297 => 102,  295 => 101,  291 => 99,  289 => 98,  285 => 96,  281 => 94,  275 => 90,  265 => 87,  260 => 86,  255 => 84,  250 => 83,  245 => 81,  240 => 79,  237 => 78,  234 => 77,  228 => 75,  226 => 74,  222 => 72,  220 => 71,  216 => 69,  213 => 68,  205 => 63,  201 => 62,  197 => 60,  195 => 59,  191 => 57,  188 => 56,  186 => 55,  183 => 54,  181 => 53,  178 => 52,  176 => 51,  173 => 50,  170 => 49,  153 => 48,  150 => 47,  147 => 46,  144 => 45,  141 => 44,  138 => 43,  135 => 42,  132 => 41,  130 => 40,  122 => 34,  116 => 31,  113 => 30,  111 => 29,  106 => 28,  104 => 27,  97 => 23,  93 => 22,  81 => 21,  67 => 14,  63 => 13,  55 => 12,  51 => 11,  43 => 10,  39 => 9,  36 => 8,  33 => 7,  29 => 1,  27 => 5,  25 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/app/center.html.twig", "/var/www/edusoho/app/Resources/views/admin/app/center.html.twig");
    }
}
