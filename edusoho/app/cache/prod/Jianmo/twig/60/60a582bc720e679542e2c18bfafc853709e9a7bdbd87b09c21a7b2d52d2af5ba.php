<?php

/* admin/system/mobile.html.twig */
class __TwigTemplate_1fd456f136620a463ecb60c4b96f3ce76aa510ecf2bd9e4b8486592f9c3644bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/system/mobile.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["menu"] = "admin_operation_mobile_banner_manage";
        // line 5
        $context["script_controller"] = "operation/mobile";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_main($context, array $blocks = array())
    {
        // line 9
        echo "<style>

#mobile-banner1-container img ,
#mobile-banner2-container img ,
#mobile-banner3-container img ,
#mobile-banner4-container img ,
#mobile-banner5-container img
{max-width: 80%; margin-bottom: 10px;}

.course-grids {
  margin:0 -15px 0 0;
  padding:0;
  list-style: none;
}

.course-grid {
  display: inline-block;
  vertical-align: top;
  margin: 15px 15px 15px 0;
  border: 1px solid #e1e1e1;
  border-radius: 4px;
}

.banner-course .course-grid {
  margin: 0 0 0 0;
  margin-left: 10px;
}

.course-grid .series-mode-label {
  position: absolute;
  top: 10px;
  right: 10px;
  font-size: 12px;
}

.course-grid .grid-body {
  position: relative;
  width: 170px;
  display: block;
  overflow: hidden;
  border-radius: 4px;
  color: #353535;
}

.grid-body a{
  text-decoration: none;
}

.course-grid .title {
  display: block;
  padding: 10px;
  min-height: 52px;
  color: #555;
  font-weight: bold;
}

.course-grid .add-course {
  font-size: 80px;
  height: 148px;
  text-align: center;
  padding-top: 30px;
}


</style>
";
        // line 74
        if (($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("mobile.enabled") == 1)) {
            // line 75
            echo "
";
            // line 76
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["web_macro"]) ? $context["web_macro"] : null), "flash_messages", array(), "method"), "html", null, true);
            echo "

<form class=\"form-horizontal\" id=\"mobile-form\" method=\"post\">

  <fieldset>
    <div class=\"help-block\">";
            // line 81
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.mobile_manage.head_tips");
            echo "</div>
    ";
            // line 82
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 5));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 83
                echo "      ";
                $context["banner"] = ("banner" . $context["i"]);
                // line 84
                echo "      <div class=\"form-group\">
        <div class=\"col-md-2 control-label\">
          <label for=\"";
                // line 86
                echo twig_escape_filter($this->env, (isset($context["banner"]) ? $context["banner"] : null), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.mobile_manage.banner_label") . $context["i"]), "html", null, true);
                echo "</label>
        </div>
        <div class=\"col-md-8 controls\">
          ";
                // line 89
                $context["moobileId"] = ("mobile-banner" . $context["i"]);
                // line 90
                echo "          <div id=\"";
                echo twig_escape_filter($this->env, (isset($context["moobileId"]) ? $context["moobileId"] : null), "html", null, true);
                echo "-container\">
              ";
                // line 91
                if ($this->getAttribute((isset($context["mobile"]) ? $context["mobile"] : null), (isset($context["banner"]) ? $context["banner"] : null), array(), "array")) {
                    // line 92
                    echo "                <img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute((isset($context["mobile"]) ? $context["mobile"] : null), (isset($context["banner"]) ? $context["banner"] : null), array(), "array")), "html", null, true);
                    echo "\">
              ";
                }
                // line 94
                echo "          </div>

          <a class=\"btn btn-default btn-sm\" id=\"";
                // line 96
                echo twig_escape_filter($this->env, (isset($context["moobileId"]) ? $context["moobileId"] : null), "html", null, true);
                echo "-upload\" data-url=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_operation_mobile_picture_upload", array("type" => (isset($context["banner"]) ? $context["banner"] : null))), "html", null, true);
                echo "\" data-upload-token=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('AppBundle\Twig\WebExtension')->makeUpoadToken("system", "image"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.mobile_manage.upload_btn"), "html", null, true);
                echo "</a>
          <button class=\"btn btn-default btn-sm\" id=\"";
                // line 97
                echo twig_escape_filter($this->env, (isset($context["moobileId"]) ? $context["moobileId"] : null), "html", null, true);
                echo "-remove\" type=\"button\" data-url=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_operation_mobile_picture_remove", array("type" => (isset($context["banner"]) ? $context["banner"] : null))), "html", null, true);
                echo "\" ";
                if ( !$this->getAttribute((isset($context["mobile"]) ? $context["mobile"] : null), (isset($context["banner"]) ? $context["banner"] : null), array(), "array")) {
                    echo "style=\"display:none;\"";
                }
                echo ">";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.mobile_manage.delete_btn"), "html", null, true);
                echo "</button>

          <p class=\"help-block\">";
                // line 99
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.mobile_manage.img_tips"), "html", null, true);
                echo "</p>
            ";
                // line 100
                $context["bannerClick"] = ("bannerClick" . $context["i"]);
                // line 101
                echo "          <div class=\"banner-setting\" role=\"";
                echo twig_escape_filter($this->env, (("banner" . $context["i"]) . "-setting"), "html", null, true);
                echo "\" ";
                if ( !$this->getAttribute((isset($context["mobile"]) ? $context["mobile"] : null), (isset($context["banner"]) ? $context["banner"] : null), array(), "array")) {
                    echo "style=\"display:none;\"";
                }
                echo ">
            <input type=\"radio\" role=\"";
                // line 102
                echo twig_escape_filter($this->env, (isset($context["bannerClick"]) ? $context["bannerClick"] : null), "html", null, true);
                echo "\" name=\"";
                echo twig_escape_filter($this->env, (isset($context["bannerClick"]) ? $context["bannerClick"] : null), "html", null, true);
                echo "\" ";
                if ( !$this->getAttribute((isset($context["mobile"]) ? $context["mobile"] : null), (isset($context["bannerClick"]) ? $context["bannerClick"] : null), array(), "array")) {
                    echo "checked=\"checked\"";
                }
                echo " value=\"0\"/>";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.mobile_manage.default_click_radio"), "html", null, true);
                echo "
            <input type=\"radio\" role=\"";
                // line 103
                echo twig_escape_filter($this->env, (isset($context["bannerClick"]) ? $context["bannerClick"] : null), "html", null, true);
                echo "\" name=\"";
                echo twig_escape_filter($this->env, (isset($context["bannerClick"]) ? $context["bannerClick"] : null), "html", null, true);
                echo "\" value=\"1\" ";
                if (($this->getAttribute((isset($context["mobile"]) ? $context["mobile"] : null), (isset($context["bannerClick"]) ? $context["bannerClick"] : null), array(), "array") == "1")) {
                    echo "checked=\"checked\"";
                }
                echo "/>";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.mobile_manage.goto_link_radio"), "html", null, true);
                echo "
            <input type=\"radio\" role=\"";
                // line 104
                echo twig_escape_filter($this->env, (isset($context["bannerClick"]) ? $context["bannerClick"] : null), "html", null, true);
                echo "\" name=\"";
                echo twig_escape_filter($this->env, (isset($context["bannerClick"]) ? $context["bannerClick"] : null), "html", null, true);
                echo "\" value=\"2\" ";
                if (($this->getAttribute((isset($context["mobile"]) ? $context["mobile"] : null), (isset($context["bannerClick"]) ? $context["bannerClick"] : null), array(), "array") == "2")) {
                    echo "checked=\"checked\"";
                }
                echo "/>";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.mobile_manage.goto_inside_course_radio"), "html", null, true);
                echo "

            <div class=\"row\">
              <div class=\"col-xs-11\">
                  ";
                // line 108
                $context["bannerUrl"] = ("bannerUrl" . $context["i"]);
                // line 109
                echo "                <input type=\"text\" id=\"";
                echo twig_escape_filter($this->env, (isset($context["bannerUrl"]) ? $context["bannerUrl"] : null), "html", null, true);
                echo "\" name=\"";
                echo twig_escape_filter($this->env, (isset($context["bannerUrl"]) ? $context["bannerUrl"] : null), "html", null, true);
                echo "\" class=\"form-control\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mobile"]) ? $context["mobile"] : null), (isset($context["bannerUrl"]) ? $context["bannerUrl"] : null), array(), "array"), "html", null, true);
                echo "\" placeholder=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.mobile_manage.link_url_placeholder"), "html", null, true);
                echo "\" ";
                if (($this->getAttribute((isset($context["mobile"]) ? $context["mobile"] : null), (isset($context["bannerClick"]) ? $context["bannerClick"] : null), array(), "array") != "1")) {
                    echo "style=\"display:none\"";
                }
                echo "/>
              </div>
            </div>

            <div class=\"row\" id=\"";
                // line 113
                echo twig_escape_filter($this->env, ("selectBannerCourse" . $context["i"]), "html", null, true);
                echo "\" data-role=\"selectBannerCourse\" ";
                if (($this->getAttribute((isset($context["mobile"]) ? $context["mobile"] : null), (isset($context["bannerClick"]) ? $context["bannerClick"] : null), array(), "array") != "2")) {
                    echo "style=\"display:none\"";
                }
                echo ">
              <a data-role=\"selectCourse\" class=\"btn btn-sm btn-primary pull-left\" data-toggle=\"modal\" data-target=\"#modal\" data-url=\"";
                // line 114
                echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_course_search_to_fill_banner");
                echo "\" >
                  ";
                // line 115
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.mobile_manage.selete_course_btn"), "html", null, true);
                echo "
              </a>
              <div name=\"";
                // line 117
                echo twig_escape_filter($this->env, ("bannerCourseShow" . $context["i"]), "html", null, true);
                echo "\">
                <ul class=\"banner-course\" role=\"bannerCourse\">
                    ";
                // line 119
                $this->loadTemplate("admin/course/course-item.html.twig", "admin/system/mobile.html.twig", 119)->display(array_merge($context, array("course" => $this->getAttribute((isset($context["bannerCourses"]) ? $context["bannerCourses"] : null), $context["i"], array(), "array"))));
                // line 120
                echo "                </ul>
              </div>
              ";
                // line 122
                $context["bannerJumpToCourseId"] = ("bannerJumpToCourseId" . $context["i"]);
                // line 123
                echo "              <input type=\"text\" name=\"";
                echo twig_escape_filter($this->env, (isset($context["bannerJumpToCourseId"]) ? $context["bannerJumpToCourseId"] : null), "html", null, true);
                echo "\" class=\"form-control\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mobile"]) ? $context["mobile"] : null), (isset($context["bannerJumpToCourseId"]) ? $context["bannerJumpToCourseId"] : null), array(), "array"), "html", null, true);
                echo "\" placeholder=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.mobile_manage.inside_course_id_placeholder"), "html", null, true);
                echo "\" style=\"display:none;\"/>
            </div>
          </div>


          <input type=\"hidden\" name=\"";
                // line 128
                echo twig_escape_filter($this->env, (isset($context["banner"]) ? $context["banner"] : null), "html", null, true);
                echo "\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["mobile"]) ? $context["mobile"] : null), (isset($context["banner"]) ? $context["banner"] : null), array(), "array"), "html", null, true);
                echo "\">
        </div>
      </div>
    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 132
            echo "
  </fieldset>

  <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            // line 135
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderCsrfToken("site"), "html", null, true);
            echo "\">

  <div class=\"row form-group\">
    <div class=\"controls col-md-offset-2 col-md-8\">
      <button type=\"submit\" class=\"btn btn-primary\">";
            // line 139
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.submit"), "html", null, true);
            echo "</button>
    </div>
  </div>

</form>

";
        } else {
            // line 146
            echo "<div class=\"well\" style=\"text-align:center;\">

";
            // line 148
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.mobile_manage.open_tips", array("%openUrl%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_setting_mobile")));
            echo "
</div>
";
        }
        // line 151
        echo "
";
    }

    public function getTemplateName()
    {
        return "admin/system/mobile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  363 => 151,  357 => 148,  353 => 146,  343 => 139,  336 => 135,  331 => 132,  311 => 128,  298 => 123,  296 => 122,  292 => 120,  290 => 119,  285 => 117,  280 => 115,  276 => 114,  268 => 113,  250 => 109,  248 => 108,  233 => 104,  221 => 103,  209 => 102,  200 => 101,  198 => 100,  194 => 99,  181 => 97,  171 => 96,  167 => 94,  161 => 92,  159 => 91,  154 => 90,  152 => 89,  144 => 86,  140 => 84,  137 => 83,  120 => 82,  116 => 81,  108 => 76,  105 => 75,  103 => 74,  36 => 9,  33 => 8,  29 => 1,  27 => 5,  25 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/system/mobile.html.twig", "/var/www/edusoho/app/Resources/views/admin/system/mobile.html.twig");
    }
}
