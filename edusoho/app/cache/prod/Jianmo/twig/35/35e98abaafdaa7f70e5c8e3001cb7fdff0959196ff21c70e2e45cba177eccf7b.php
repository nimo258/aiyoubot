<?php

/* admin/theme/edit.html.twig */
class __TwigTemplate_dfd738dac0dc50e7f6211e7e0ce6051bbdcaade0689e6c1dfc28bb8a6bc901d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("admin/theme/base-edit.html.twig", "admin/theme/edit.html.twig", 2);
        $this->blocks = array(
            'theme_nav' => array($this, 'block_theme_nav'),
            'theme_panes' => array($this, 'block_theme_panes'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/theme/base-edit.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_theme_nav($context, array $blocks = array())
    {
        // line 4
        echo "  <li class=\"active\"><a href=\"#left\" role=\"tab\" data-toggle=\"tab\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.components"), "html", null, true);
        echo "</a></li>
  <li><a href=\"#top\" role=\"tab\" data-toggle=\"tab\">";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.top"), "html", null, true);
        echo "</a></li>
  <li><a href=\"#maincolor\" role=\"tab\" data-toggle=\"tab\">";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.coloring_scheme_btn"), "html", null, true);
        echo "</a></li>
";
    }

    // line 9
    public function block_theme_panes($context, array $blocks = array())
    {
        // line 10
        echo "  ";
        $this->loadTemplate("admin/theme/tab-panel/components.html.twig", "admin/theme/edit.html.twig", 10)->display(array_merge($context, array("allBlockConfig" => (($this->getAttribute($this->getAttribute(        // line 11
(isset($context["allConfig"]) ? $context["allConfig"] : null), "blocks", array(), "any", false, true), "left", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["allConfig"]) ? $context["allConfig"] : null), "blocks", array(), "any", false, true), "left", array()))) : ("")), "blockConfig" => (($this->getAttribute($this->getAttribute(        // line 12
(isset($context["themeConfig"]) ? $context["themeConfig"] : null), "blocks", array(), "any", false, true), "left", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["themeConfig"]) ? $context["themeConfig"] : null), "blocks", array(), "any", false, true), "left", array()))) : ("")), "configKey" => "left", "isActive" => true)));
        // line 16
        echo "  ";
        $this->loadTemplate("admin/theme/tab-panel/nav.html.twig", "admin/theme/edit.html.twig", 16)->display(array_merge($context, array("navigation" => (($this->getAttribute((isset($context["themeConfig"]) ? $context["themeConfig"] : null), "navigation", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["themeConfig"]) ? $context["themeConfig"] : null), "navigation", array()))) : ("")))));
        // line 17
        echo "  ";
        $this->loadTemplate("admin/theme/tab-panel/maincolor.html.twig", "admin/theme/edit.html.twig", 17)->display(array_merge($context, array("maincolor" => $this->getAttribute((isset($context["themeConfig"]) ? $context["themeConfig"] : null), "maincolor", array()), "navigationcolor" => $this->getAttribute((isset($context["themeConfig"]) ? $context["themeConfig"] : null), "navigationcolor", array()))));
    }

    public function getTemplateName()
    {
        return "admin/theme/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 17,  55 => 16,  53 => 12,  52 => 11,  50 => 10,  47 => 9,  41 => 6,  37 => 5,  32 => 4,  29 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/theme/edit.html.twig", "/var/www/edusoho/app/Resources/views/admin/theme/edit.html.twig");
    }
}
