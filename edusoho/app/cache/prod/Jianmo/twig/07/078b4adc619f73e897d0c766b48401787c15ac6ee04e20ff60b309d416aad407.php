<?php

/* admin/system/log/template.html.twig */
class __TwigTemplate_70ee927071bd221c5bf8f2200869dbc138b432aaacd52e896e6a9ba6b2751d11 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

";
        // line 3
        if (($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "shouldShowTemplate", array()) == true)) {
            // line 4
            echo "
  ";
            // line 5
            $context["template"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(((("log.action.template." . $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "module", array())) . ".") . $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "action", array())), $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "urlParamsJson", array()));
            // line 6
            echo "
  ";
            // line 7
            if (((isset($context["template"]) ? $context["template"] : null) == ((("log.action.template." . $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "module", array())) . ".") . $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "action", array())))) {
                // line 8
                echo "
    ";
                // line 9
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "message", array()), "html", null, true);
                echo "

  ";
            } else {
                // line 12
                echo "
    ";
                // line 13
                echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(((("log.action.template." . $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "module", array())) . ".") . $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "action", array())), $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "urlParamsJson", array()));
                echo "

  ";
            }
            // line 16
            echo "
  ";
            // line 17
            if (($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "shouldShowModal", array()) == true)) {
                // line 18
                echo "    <a href=\"#modal\" data-toggle=\"modal\" class=\"btn btn-default btn-sm\"
       data-url=\"";
                // line 19
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_logs_field_change", array("log" => (isset($context["log"]) ? $context["log"] : null))), "html", null, true);
                echo "\">
      ";
                // line 20
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("log.field.detail"), "html", null, true);
                echo "
    </a>
  ";
            }
            // line 23
            echo "
";
        } else {
            // line 25
            echo "
  ";
            // line 26
            echo twig_escape_filter($this->env, $this->env->getExtension('AppBundle\Twig\AppExtension')->logTrans($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "message", array()), $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "data", array())), "html", null, true);
            echo "

";
        }
    }

    public function getTemplateName()
    {
        return "admin/system/log/template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 26,  75 => 25,  71 => 23,  65 => 20,  61 => 19,  58 => 18,  56 => 17,  53 => 16,  47 => 13,  44 => 12,  38 => 9,  35 => 8,  33 => 7,  30 => 6,  28 => 5,  25 => 4,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/system/log/template.html.twig", "/var/www/edusoho/app/Resources/views/admin/system/log/template.html.twig");
    }
}
