<?php

/* admin/system/pay/wxpay.html.twig */
class __TwigTemplate_f8b426a46e6ba1f68e2b4b6b4e19c61e386eda0f285e9301c34862e1d6971055 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<fieldset>
  <legend>";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.payment_setting.wxpay"), "html", null, true);
        echo "</legend>
  <div class=\"form-group\">
    <div class=\"col-md-3 control-label\">
      <label>";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.payment_setting.api_status"), "html", null, true);
        echo "</label>
    </div>
    <div class=\"controls col-md-1\">
      <p class=\"form-control-static\">
        ";
        // line 9
        if ($this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "wxpay_enabled", array())) {
            // line 10
            echo "          ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.payment_setting.api_open"), "html", null, true);
            echo "
        ";
        } else {
            // line 12
            echo "          ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.payment_setting.api_close"), "html", null, true);
            echo "
        ";
        }
        // line 14
        echo "      </p>
    </div>
    <div class=\"controls col-md-2 form-control-static\">
      <a target=\"_blank\" href=\"";
        // line 17
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_setting_wechat_auth");
        echo "\">
      ";
        // line 18
        if ($this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "wxpay_enabled", array())) {
            // line 19
            echo "        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.payment_setting.api_close.direct"), "html", null, true);
            echo "
      ";
        } else {
            // line 21
            echo "        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.payment_setting.api_open.direct"), "html", null, true);
            echo "
      ";
        }
        // line 23
        echo "      </a>
    </div>
  </div>

</fieldset>
";
    }

    public function getTemplateName()
    {
        return "admin/system/pay/wxpay.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 23,  66 => 21,  60 => 19,  58 => 18,  54 => 17,  49 => 14,  43 => 12,  37 => 10,  35 => 9,  28 => 5,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/system/pay/wxpay.html.twig", "/var/www/edusoho/app/Resources/views/admin/system/pay/wxpay.html.twig");
    }
}
