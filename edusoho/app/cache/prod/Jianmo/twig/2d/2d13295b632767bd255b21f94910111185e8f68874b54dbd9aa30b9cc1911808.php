<?php

/* admin/content/content-modal.html.twig */
class __TwigTemplate_9471dd9052cd8e1d97a137431882f9d472791fcd2ae44200c13412e55cd551fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("old-bootstrap-modal-layout.html.twig", "admin/content/content-modal.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "old-bootstrap-modal-layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["modal_class"] = "modal-lg";
        // line 148
        $context["hideFooter"] = true;
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.custom_page_manage.create_custom_page"), "html", null, true);
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "
";
        // line 8
        $context["content"] = ((array_key_exists("content", $context)) ? (_twig_default_filter((isset($context["content"]) ? $context["content"] : null), null)) : (null));
        // line 9
        echo "
<form class=\"two-col-form\" id=\"content-form\" method=\"post\" enctype=\"multipart/form-data\"
  ";
        // line 11
        if ((isset($context["content"]) ? $context["content"] : null)) {
            // line 12
            echo "    action=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_content_edit", array("id" => $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "id", array()))), "html", null, true);
            echo "\"
  ";
        } else {
            // line 14
            echo "    action=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_content_create", array("type" => $this->getAttribute((isset($context["type"]) ? $context["type"] : null), "alias", array()))), "html", null, true);
            echo "\"
  ";
        }
        // line 16
        echo "  >
  <div class=\"row\">
    <div class=\"col-md-8 two-col-main\">
      ";
        // line 19
        if (twig_in_filter("title", $this->getAttribute((isset($context["type"]) ? $context["type"] : null), "basicFields", array()))) {
            // line 20
            echo "        <div class=\"form-group\">
          <label for=\"content-title-field\" class=\"control-label\">";
            // line 21
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.custom_page_manage.custom_page_title"), "html", null, true);
            echo "</label>
          <div class=\"controls\">
            <input class=\"form-control\" id=\"content-title-field\" type=\"text\" name=\"title\" value=\"";
            // line 23
            echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->fieldValue((isset($context["content"]) ? $context["content"] : null), "title");
            echo "\">
          </div>
        </div>
      ";
        }
        // line 27
        echo "
      <div class=\"form-group\">
        <label for=\"editor-field\" class=\"control-label\">";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.custom_page_manage.custom_page_editor_type"), "html", null, true);
        echo "</label>
        <div class=\"controls radios\">
          <label><input type=\"radio\" name=\"editor\" value=\"richeditor\" 
            ";
        // line 32
        if (twig_test_empty((isset($context["content"]) ? $context["content"] : null))) {
            // line 33
            echo "             checked=\"checked\" 
            ";
        } elseif (($this->getAttribute(        // line 34
(isset($context["content"]) ? $context["content"] : null), "editor", array()) == "richeditor")) {
            // line 35
            echo "             checked=\"checked\"
            ";
        }
        // line 36
        echo ">
            ";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.custom_page_manage.custom_page_richeditor_radio"), "html", null, true);
        echo "</label>
        <label><input type=\"radio\" name=\"editor\" value=\"none\"
          ";
        // line 39
        if (((isset($context["content"]) ? $context["content"] : null) && ($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "editor", array()) == "none"))) {
            echo " checked=\"checked\" ";
        }
        // line 40
        echo "          >";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.custom_page_manage.custom_page_htmleditor_radio"), "html", null, true);
        echo "</label>
        </div>
      </div>

      ";
        // line 44
        if (twig_in_filter("body", $this->getAttribute((isset($context["type"]) ? $context["type"] : null), "basicFields", array()))) {
            // line 45
            echo "        <label for=\"content-body-field\" class=\"control-label\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.custom_page_manage.custom_page_content"), "html", null, true);
            echo "</label>

        <div class=\"form-group\" 
        ";
            // line 48
            if (((isset($context["content"]) ? $context["content"] : null) && ($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "editor", array()) == "none"))) {
                // line 49
                echo "          style=\"display:none\"
        ";
            }
            // line 51
            echo "        >
          <div class=\"controls\">
            <textarea class=\"form-control\" id=\"richeditor-body-field\" rows=\"16\" name=\"richeditor-body\"
              data-image-upload-url=\"";
            // line 54
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("editor_upload", array("token" => $this->env->getExtension('AppBundle\Twig\WebExtension')->makeUpoadToken("default"))), "html", null, true);
            echo "\"
              data-flash-upload-url=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("editor_upload", array("token" => $this->env->getExtension('AppBundle\Twig\WebExtension')->makeUpoadToken("default", "flash"))), "html", null, true);
            echo "\"
            >";
            // line 56
            echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->fieldValue((isset($context["content"]) ? $context["content"] : null), "body");
            echo "</textarea>
          </div>
        </div>

        <div class=\"form-group\" 
        ";
            // line 61
            if (twig_test_empty((isset($context["content"]) ? $context["content"] : null))) {
                // line 62
                echo "               style=\"display:none\" 
              ";
            } elseif (($this->getAttribute(            // line 63
(isset($context["content"]) ? $context["content"] : null), "editor", array()) == "richeditor")) {
                // line 64
                echo "               style=\"display:none\"
              ";
            }
            // line 65
            echo ">
          <div class=\"controls\">
            <textarea class=\"form-control\" id=\"noneeditor-body-field\" rows=\"16\" name=\"noneeditor-body\">";
            // line 67
            echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->fieldValue((isset($context["content"]) ? $context["content"] : null), "body");
            echo "</textarea>
          </div>
        </div> 
      ";
        }
        // line 71
        echo "
      ";
        // line 72
        if ($this->getAttribute((isset($context["type"]) ? $context["type"] : null), "extendedFields", array())) {
            // line 73
            echo "        ";
            $this->loadTemplate((("admin/content/" . $this->getAttribute((isset($context["type"]) ? $context["type"] : null), "alias", array())) . "-extended-fields.html.twig"), "admin/content/content-modal.html.twig", 73)->display($context);
            // line 74
            echo "      ";
        }
        // line 75
        echo "    </div>
    <div class=\"col-md-4 two-col-side\">
      ";
        // line 77
        if (twig_in_filter("alias", $this->getAttribute((isset($context["type"]) ? $context["type"] : null), "basicFields", array()))) {
            // line 78
            echo "        <div class=\"panel panel-default\">
          <div class=\"panel-heading\">";
            // line 79
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.custom_page_manage.custom_page_url"), "html", null, true);
            echo "</div>
          <div class=\"panel-body\">
            <div class=\"form-group\">
              <div class=\"controls\">
                <input class=\"form-control width-input width-input-large\" type=\"text\" name=\"alias\" value=\"";
            // line 83
            echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->fieldValue((isset($context["content"]) ? $context["content"] : null), "alias");
            echo "\" data-url=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_content_alias_check", array("that" => (($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "alias", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "alias", array()), "")) : ("")))), "html", null, true);
            echo "\">

                <i class=\"glyphicon glyphicon-question-sign text-muted\" data-toggle=\"tooltip\" data-placement=\"top\" data-trigger=\"hover\" data-html=\"false\" data-content=\"";
            // line 85
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.custom_page_manage.custom_page_url_tips"), "html", null, true);
            echo "\"></i>
              </div>
            </div>
          </div>
        </div>
      ";
        }
        // line 91
        echo "
      ";
        // line 92
        if (twig_in_filter("categoryId", $this->getAttribute((isset($context["type"]) ? $context["type"] : null), "basicFields", array()))) {
            // line 93
            echo "        <div class=\"panel\">
          <div class=\"panel-heading\">";
            // line 94
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.custom_page_manage.title_category"), "html", null, true);
            echo "</div>
          <select name=\"categoryId\">
            ";
            // line 96
            echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->selectOptions($this->env->getExtension('AppBundle\Twig\WebExtension')->getCategoryChoices("default"), $this->env->getExtension('AppBundle\Twig\HtmlExtension')->fieldValue((isset($context["content"]) ? $context["content"] : null), "categoryId"), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.custom_page_manage.custom_page_category_option"));
            echo "
          </select>
        </div>
      ";
        }
        // line 100
        echo "
      ";
        // line 101
        if (twig_in_filter("tagIds", $this->getAttribute((isset($context["type"]) ? $context["type"] : null), "basicFields", array()))) {
            // line 102
            echo "        <div class=\"panel\">
          <div class=\"panel-heading\">";
            // line 103
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.custom_page_manage.custom_page_tag"), "html", null, true);
            echo "</div>
          <div class=\"form-group\">
            <div class=\"controls\" style=\"width:100%;\">
              <input class=\"width-full\" id=\"content-tags-field\" type=\"text\" name=\"tags\" value=\"";
            // line 106
            echo twig_escape_filter($this->env, $this->env->getExtension('AppBundle\Twig\WebExtension')->tagsJoinFilter($this->env->getExtension('AppBundle\Twig\HtmlExtension')->fieldValue((isset($context["content"]) ? $context["content"] : null), "tagIds")), "html", null, true);
            echo "\">
            </div>
          </div>
        </div>
      ";
        }
        // line 111
        echo "
      ";
        // line 112
        if (twig_in_filter("template", $this->getAttribute((isset($context["type"]) ? $context["type"] : null), "basicFields", array()))) {
            // line 113
            echo "        <div class=\"panel panel-default\" >
          <div class=\"panel-heading\">";
            // line 114
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.custom_page_manage.custom_page_template"), "html", null, true);
            echo "</div>
          <div class=\"panel-body\">
            ";
            // line 116
            $context["templates"] = array("default" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.custom_page_manage.custom_page_default_template"), "blank" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.custom_page_manage.custom_page_no_nav_template"), "fullBlank" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.custom_page_manage.custom_page_blank_template"));
            // line 121
            echo "
            <select class=\"form-control\" name=\"template\">
              ";
            // line 123
            echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->selectOptions((isset($context["templates"]) ? $context["templates"] : null), $this->env->getExtension('AppBundle\Twig\HtmlExtension')->fieldValue((isset($context["content"]) ? $context["content"] : null), "template"));
            echo "
            </select>
          </div>
        </div>
      ";
        }
        // line 127
        echo " 

      <input type=\"hidden\" name=\"publishedTime\" value=\"";
        // line 129
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->env->getExtension('AppBundle\Twig\HtmlExtension')->fieldValue((isset($context["content"]) ? $context["content"] : null), "publishedTime", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "server", array()), "get", array(0 => "REQUEST_TIME"), "method")), "Y-m-d H:i:s"), "html", null, true);
        echo "\">


    </div>
  </div>
  <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderCsrfToken("site"), "html", null, true);
        echo "\">
</form>


  <script> app.load('content/content-modal'); </script>

";
    }

    // line 142
    public function block_footer($context, array $blocks = array())
    {
        // line 143
        echo "
    <button type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\">";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.cancel"), "html", null, true);
        echo "</button>
    <button id=\"content-save-btn\" type=\"submit\" class=\"btn btn-primary\" data-toggle=\"form-submit\" data-target=\"#content-form\" data-loading-text=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.save.submiting"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.save"), "html", null, true);
        echo "</button>
";
    }

    public function getTemplateName()
    {
        return "admin/content/content-modal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  333 => 145,  329 => 144,  326 => 143,  323 => 142,  312 => 134,  304 => 129,  300 => 127,  292 => 123,  288 => 121,  286 => 116,  281 => 114,  278 => 113,  276 => 112,  273 => 111,  265 => 106,  259 => 103,  256 => 102,  254 => 101,  251 => 100,  244 => 96,  239 => 94,  236 => 93,  234 => 92,  231 => 91,  222 => 85,  215 => 83,  208 => 79,  205 => 78,  203 => 77,  199 => 75,  196 => 74,  193 => 73,  191 => 72,  188 => 71,  181 => 67,  177 => 65,  173 => 64,  171 => 63,  168 => 62,  166 => 61,  158 => 56,  154 => 55,  150 => 54,  145 => 51,  141 => 49,  139 => 48,  132 => 45,  130 => 44,  122 => 40,  118 => 39,  113 => 37,  110 => 36,  106 => 35,  104 => 34,  101 => 33,  99 => 32,  93 => 29,  89 => 27,  82 => 23,  77 => 21,  74 => 20,  72 => 19,  67 => 16,  61 => 14,  55 => 12,  53 => 11,  49 => 9,  47 => 8,  44 => 7,  41 => 6,  35 => 5,  31 => 1,  29 => 148,  27 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/content/content-modal.html.twig", "/var/www/edusoho/app/Resources/views/admin/content/content-modal.html.twig");
    }
}
