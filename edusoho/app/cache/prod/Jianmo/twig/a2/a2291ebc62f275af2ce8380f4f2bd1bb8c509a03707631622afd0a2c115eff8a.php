<?php

/* default/header/part/user-inform.html.twig */
class __TwigTemplate_f92eee9edcbc7983c0ee78b676097480fd45fc47821ff29aca79db0d40200c0b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["messageSetting"] = $this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("message");
        // line 2
        echo "<li class=\"user-inform-li js-user-inform hidden-xs hidden-sm cd-ml24 cd-dropdown\" data-toggle=\"cd-dropdown\">
  <a class=\"inform-dropdown-toggle\"><i class=\"cd-icon cd-icon-bell\"></i>";
        // line 3
        if ((($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "newNotificationNum", array()) > 0) || ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "newMessageNum", array()) > 0))) {
            echo "<span class=\"inform-dropdown-toggle__num cd-badge cd-badge-danger ";
            if ((($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "newNotificationNum", array()) + $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "newMessageNum", array())) > 9)) {
                echo " inform-dropdown-toggle__num--big ";
            }
            echo "\">";
            echo twig_escape_filter($this->env, ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "newNotificationNum", array()) + $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "newMessageNum", array())), "html", null, true);
            echo "</span>";
        }
        echo "</a>
  <div class=\"dropdown-menu inform-dropdown js-user-nav-dropdown\" role=\"menu\">
    <div class=\"clearfix inform-dropdown-head\" role=\"tablist\">
      <a class=\"";
        // line 6
        if (((($this->getAttribute((isset($context["messageSetting"]) ? $context["messageSetting"] : null), "showable", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["messageSetting"]) ? $context["messageSetting"] : null), "showable", array()), "1")) : ("1")) != 0)) {
            echo "inform-dropdown-head__tab active";
        } else {
            echo "inform-dropdown-head__one_tab";
        }
        echo " js-inform-tab\" href=\"#notification\" role=\"tab\" data-toggle=\"tab\" data-type=\"newNotification\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("notification.user_notification"), "html", null, true);
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "newNotificationNum", array()) > 0)) {
            echo "<span class=\"inform-dropdown-dot\"></span>";
        }
        echo "</a>
      ";
        // line 7
        if (((($this->getAttribute((isset($context["messageSetting"]) ? $context["messageSetting"] : null), "showable", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["messageSetting"]) ? $context["messageSetting"] : null), "showable", array()), "1")) : ("1")) != 0)) {
            // line 8
            echo "        <a class=\"inform-dropdown-head__tab js-inform-tab\" href=\"#message\" role=\"tab\" data-toggle=\"tab\" data-type=\"conversation\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.btn.send_message"), "html", null, true);
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "newMessageNum", array()) > 0)) {
                echo "<span class=\"inform-dropdown-dot\"></span>";
            }
            echo "</a>
      ";
        }
        // line 10
        echo "    </div>
    <div class=\"tab-content\">
      <div class=\"notification-content tab-pane active\" role=\"tabpanel\" id=\"notification\">
        <div class=\"inform-dropdown-body js-inform-dropdown-body\">
          ";
        // line 14
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "newNotificationNum", array()) > 0)) {
            // line 15
            echo "            <div class=\"inform-loading js-inform-loading\"></div>
            <div class=\"js-inform-newNotification\"></div>
          ";
        } else {
            // line 18
            echo "            <div class=\"inform-empty-status js-inform-empty\">
              <img class=\"cd-mb24\" src=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/notice/null-notice.png"), "html", null, true);
            echo "\"
            srcset=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/notice/null-notice.png"), "html", null, true);
            echo " 1x, ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/notice/null-notice@2x.png"), "html", null, true);
            echo " 2x\"/>
              <div class=\"inform-empty-status__tip\">";
            // line 21
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("homepage.header.no_new_notifications"), "html", null, true);
            echo "</div>
            </div>
          ";
        }
        // line 24
        echo "        </div>
        <div class=\"inform-dropdown-foot\">
          <a class=\"cd-link-minor\" href=\"";
        // line 26
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("notification");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("homepage.header.check_all_notifications"), "html", null, true);
        echo "</a>
        </div>
      </div>
      <div class=\"message-content tab-pane\" role=\"tabpanel\" id=\"message\">
        <div class=\"inform-dropdown-body js-inform-dropdown-body\">
          ";
        // line 31
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "newMessageNum", array()) > 0)) {
            // line 32
            echo "            <div class=\"inform-loading js-inform-loading\"></div>
            <div class=\"js-inform-conversation\"></div>
          ";
        } else {
            // line 35
            echo "            <div class=\"inform-empty-status js-inform-empty\">
              <img class=\"cd-mb24\" src=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/notice/null-message.png"), "html", null, true);
            echo "\"
          srcset=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/notice/null-message.png"), "html", null, true);
            echo " 1x, ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/notice/null-message@2x.png"), "html", null, true);
            echo " 2x\"/>
              <div class=\"inform-empty-status__tip\">";
            // line 38
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("homepage.header.no_new_messages"), "html", null, true);
            echo "</div>
            </div>
          ";
        }
        // line 41
        echo "        </div>
        <div class=\"inform-dropdown-foot\">
          <a class=\"cd-link-minor\" href=\"";
        // line 43
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("message");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("homepage.header.check_all_messages"), "html", null, true);
        echo "</a>
        </div>
      </div>
    </div>
  </div>
</li>";
    }

    public function getTemplateName()
    {
        return "default/header/part/user-inform.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 43,  134 => 41,  128 => 38,  122 => 37,  118 => 36,  115 => 35,  110 => 32,  108 => 31,  98 => 26,  94 => 24,  88 => 21,  82 => 20,  78 => 19,  75 => 18,  70 => 15,  68 => 14,  62 => 10,  53 => 8,  51 => 7,  38 => 6,  24 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/header/part/user-inform.html.twig", "/var/www/edusoho/app/Resources/views/default/header/part/user-inform.html.twig");
    }
}
