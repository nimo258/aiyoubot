<?php

/* admin/operation-analysis/register.html.twig */
class __TwigTemplate_fbc8adfbf103481948e2539239c29f1968587c1b11328a05068575b965e9d969 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/operation-analysis/analysis-base-layout.html.twig", "admin/operation-analysis/register.html.twig", 1);
        $this->blocks = array(
            'analysisBody' => array($this, 'block_analysisBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/operation-analysis/analysis-base-layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["script_controller"] = "analysis/register";
        // line 3
        $context["href"] = "admin_operation_analysis_register";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_analysisBody($context, array $blocks = array())
    {
        // line 6
        echo "<div class=\"col-md-12\">  
  ";
        // line 7
        if (((isset($context["tab"]) ? $context["tab"] : null) == "trend")) {
            // line 8
            echo "\t <div id=\"line-data\">
   </div>
   <input id=\"data\"  type=\"hidden\" value='";
            // line 10
            if ((isset($context["data"]) ? $context["data"] : null)) {
                echo twig_escape_filter($this->env, (isset($context["data"]) ? $context["data"] : null), "html", null, true);
            }
            echo "'>
   <input id=\"registerStartDate\"  type=\"hidden\" value='";
            // line 11
            if ((isset($context["registerStartDate"]) ? $context["registerStartDate"] : null)) {
                echo twig_escape_filter($this->env, (isset($context["registerStartDate"]) ? $context["registerStartDate"] : null), "html", null, true);
            }
            echo "'>  
  ";
        } elseif ((        // line 12
(isset($context["tab"]) ? $context["tab"] : null) == "detail")) {
            // line 13
            echo "    ";
            $this->loadTemplate("admin/operation-analysis/register.table.html.twig", "admin/operation-analysis/register.html.twig", 13)->display($context);
            // line 14
            echo "  ";
        }
        // line 15
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "admin/operation-analysis/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 15,  62 => 14,  59 => 13,  57 => 12,  51 => 11,  45 => 10,  41 => 8,  39 => 7,  36 => 6,  33 => 5,  29 => 1,  27 => 3,  25 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/operation-analysis/register.html.twig", "/var/www/edusoho/app/Resources/views/admin/operation-analysis/register.html.twig");
    }
}
