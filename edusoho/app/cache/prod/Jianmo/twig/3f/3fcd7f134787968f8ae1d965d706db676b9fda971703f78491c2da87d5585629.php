<?php

/* admin/course-set/data.html.twig */
class __TwigTemplate_3c302b84442885218f5dd2e719478db0c72a51a5bd8b1d79ddd5aa5ed0d73c35 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/course-set/data.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["menu"] = "admin_course_set_data";
        // line 5
        $context["script_controller"] = "course/data";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        // line 8
        echo "  ";
        $this->loadTemplate("admin/course/tab-data.html.twig", "admin/course-set/data.html.twig", 8)->display($context);
        // line 9
        echo "
    <form id=\"message-search-form\" class=\"form-inline well well-sm mtl\" action=\"\" method=\"get\" novalidate>
      ";
        // line 11
        $this->loadTemplate("org/org-tree-select.html.twig", "admin/course-set/data.html.twig", 11)->display(array_merge($context, array("orgCode" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "orgCode"), "method"), "modal" => "list")));
        // line 12
        echo "      
      <div class=\"form-group\">
        <select style=\"width:150px;\" class=\"form-control\" name=\"categoryId\">
          ";
        // line 15
        echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->selectOptions($this->env->getExtension('AppBundle\Twig\WebExtension')->getCategoryChoices("course"), $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "query", array()), "get", array(0 => "categoryId"), "method"), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_manage.category_placeholder"));
        echo "
        </select>
      </div>
      <div class=\"form-group\">
        <input class=\"form-control\" type=\"text\" placeholder=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_manage.title_placeholder"), "html", null, true);
        echo "\" name=\"title\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "title"), "method"), "html", null, true);
        echo "\">
      </div>
      <div class=\"form-group\">
        <input class=\"form-control\" type=\"text\" placeholder=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_manage.creator_placeholer"), "html", null, true);
        echo "\" name=\"creatorName\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "creatorName"), "method"), "html", null, true);
        echo "\">
      </div>
      <button class=\"btn btn-primary\">";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.search"), "html", null, true);
        echo "</button>
    </form>
    <div class=\"table-responsive\">
        <table class=\"table table-bordered\" style=\"word-break:break-all;text-align:center;\">
          <tr class=\"active\">
            <td width=\"30%\">";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_manage.statistics.name"), "html", null, true);
        echo "</td>
             ";
        // line 30
        if (((isset($context["filter"]) ? $context["filter"] : null) == "classroom")) {
            // line 31
            echo "            <td>
              ";
            // line 32
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_manage.statistics.classroom", array("%classroom%" => _twig_default_filter($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("classroom.name"), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("classroom")))), "html", null, true);
            echo "
            </td>
             ";
        }
        // line 35
        echo "            <td>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_manage.statistics.teach_plan_count"), "html", null, true);
        echo "</td>
            <td>";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_manage.statistics.task_count", array("%taskName%" => _twig_default_filter($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("course.task_name"), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_course.task")))), "html", null, true);
        echo "</td>
            <td>";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_manage.statistics.student_number"), "html", null, true);
        echo "</td>
            <td>";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_manage.statistics.finished_number"), "html", null, true);
        echo "</td>
            <td>";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_manage.statistics.study_time_minute"), "html", null, true);
        echo "</td>
            <td>";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_manage.statistics.income_RMB"), "html", null, true);
        echo "</td>
            <td>";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_manage.operater"), "html", null, true);
        echo "</td>
          </tr>
          
          ";
        // line 44
        if ((isset($context["courseSets"]) ? $context["courseSets"] : null)) {
            // line 45
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["courseSets"]) ? $context["courseSets"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["courseSet"]) {
                // line 46
                echo "            <tr>
              <td>
                <a data-toggle=\"modal\" data-target=\"#modal\" data-url=\"";
                // line 48
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_set_detail_data", array("id" => $this->getAttribute($context["courseSet"], "id", array()))), "html", null, true);
                echo "\" href=\"javascript:;\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["courseSet"], "title", array()), "html", null, true);
                echo "</a>
              </td>
              ";
                // line 50
                if (((isset($context["filter"]) ? $context["filter"] : null) == "classroom")) {
                    // line 51
                    echo "              <td>
                ";
                    // line 52
                    $context["classroom"] = (($this->getAttribute((isset($context["classrooms"]) ? $context["classrooms"] : null), $this->getAttribute($context["courseSet"], "id", array()), array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["classrooms"]) ? $context["classrooms"] : null), $this->getAttribute($context["courseSet"], "id", array()), array(), "array"), null)) : (null));
                    // line 53
                    echo "                ";
                    if ((isset($context["classroom"]) ? $context["classroom"] : null)) {
                        // line 54
                        echo "                  <a href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("classroom_show", array("id" => $this->getAttribute((isset($context["classroom"]) ? $context["classroom"] : null), "classroomId", array()))), "html", null, true);
                        echo "\" target=\"_blank\">";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["classroom"]) ? $context["classroom"] : null), "classroomTitle", array()), "html", null, true);
                        echo "</a>
                ";
                    } elseif ( !                    // line 55
(isset($context["classroom"]) ? $context["classroom"] : null)) {
                        // line 56
                        echo "                  <span class=\"label label-danger live-label mls\">";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_manage.statistics.no_introduce"), "html", null, true);
                        echo "</span> 
                ";
                    }
                    // line 58
                    echo "              </td>
              ";
                }
                // line 60
                echo "              <td>";
                echo twig_escape_filter($this->env, (($this->getAttribute($context["courseSet"], "courseCount", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["courseSet"], "courseCount", array()))) : ("")), "html", null, true);
                echo "</td>
              <td>";
                // line 61
                echo twig_escape_filter($this->env, (($this->getAttribute($context["courseSet"], "taskCount", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["courseSet"], "taskCount", array()))) : ("")), "html", null, true);
                echo "</td>
              <td>";
                // line 62
                echo twig_escape_filter($this->env, (($this->getAttribute($context["courseSet"], "studentNum", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["courseSet"], "studentNum", array()))) : ("")), "html", null, true);
                echo "</td>
              <td>";
                // line 63
                echo twig_escape_filter($this->env, (($this->getAttribute($context["courseSet"], "isLearnedNum", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["courseSet"], "isLearnedNum", array()), 0)) : (0)), "html", null, true);
                echo "</td>
              <td>";
                // line 64
                echo twig_escape_filter($this->env, $this->getAttribute($context["courseSet"], "learnedTime", array()), "html", null, true);
                echo "</td>
              <td>";
                // line 65
                echo twig_escape_filter($this->env, (($this->getAttribute($context["courseSet"], "income", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["courseSet"], "income", array()))) : ("")), "html", null, true);
                echo "</td>
              <td>
                <a data-toggle=\"modal\" data-target=\"#modal\" data-url=\"";
                // line 67
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_courses_data", array("courseSetId" => $this->getAttribute($context["courseSet"], "id", array()))), "html", null, true);
                echo "\" href=\"javascript:;\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_manage.statistics.watch_task", array("%taskName%" => _twig_default_filter($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("course.task_name"), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_course.task")))), "html", null, true);
                echo "</a>
              </td>
            </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['courseSet'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 71
            echo "          ";
        }
        echo "   
        </table>
     ";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin_macro"]) ? $context["admin_macro"] : null), "paginator", array(0 => (isset($context["paginator"]) ? $context["paginator"] : null)), "method"), "html", null, true);
        echo "
      </div>
";
    }

    public function getTemplateName()
    {
        return "admin/course-set/data.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 73,  211 => 71,  199 => 67,  194 => 65,  190 => 64,  186 => 63,  182 => 62,  178 => 61,  173 => 60,  169 => 58,  163 => 56,  161 => 55,  154 => 54,  151 => 53,  149 => 52,  146 => 51,  144 => 50,  137 => 48,  133 => 46,  128 => 45,  126 => 44,  120 => 41,  116 => 40,  112 => 39,  108 => 38,  104 => 37,  100 => 36,  95 => 35,  89 => 32,  86 => 31,  84 => 30,  80 => 29,  72 => 24,  65 => 22,  57 => 19,  50 => 15,  45 => 12,  43 => 11,  39 => 9,  36 => 8,  33 => 7,  29 => 1,  27 => 5,  25 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/course-set/data.html.twig", "/var/www/edusoho/app/Resources/views/admin/course-set/data.html.twig");
    }
}
