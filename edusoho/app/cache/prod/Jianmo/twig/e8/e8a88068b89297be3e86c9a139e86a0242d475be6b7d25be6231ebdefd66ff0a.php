<?php

/* admin/invite/records.html.twig */
class __TwigTemplate_5b7d36b8a6796dea44b3f2330258e36b88d831d1dd5b5c6b2c7c4461995615c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/invite/records.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["menu"] = "admin_operation_invite_record";
        // line 4
        $context["script_controller"] = "invite/record";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_main($context, array $blocks = array())
    {
        // line 7
        echo "    <form id=\"invite-record\" class=\"form-inline well well-sm\" action=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_invite_record");
        echo "\" method=\"get\" novalidate>
        <input id=\"nickname\" class=\"form-control\" type=\"text\" name=\"nickname\" placeholder=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_invite.nickname.placeholer"), "html", null, true);
        echo "\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "query", array()), "get", array(0 => "nickname"), "method"), "html", null, true);
        echo "\">
        <div class=\"form-group \">
            <input class=\"form-control\" type=\"text\" id=\"startDate\" name=\"startDate\"
                   value=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "query", array()), "get", array(0 => "startDate"), "method"), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.user.form.input_placeholder.startDate"), "html", null, true);
        echo "\">
            -
            <input class=\"form-control\" type=\"text\" id=\"endDate\" name=\"endDate\"
                   value=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "query", array()), "get", array(0 => "endDate"), "method"), "html", null, true);
        echo "\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.user.form.input_placeholder.endDate"), "html", null, true);
        echo "\">
        </div>
        <button class=\"btn btn-primary\" type=\"submit\">";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.search"), "html", null, true);
        echo "</button>
        ";
        // line 17
        $this->loadTemplate("export/export-btn.html.twig", "admin/invite/records.html.twig", 17)->display(array_merge($context, array("exportFileName" => "invite-records", "targetFormId" => "invite-record")));
        // line 22
        echo "    </form>

    <table class=\"table table-hover table-striped\" >
        <thead>
        <tr>
            <th width=\"10%\">";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_invite.invite_code_owner"), "html", null, true);
        echo "</th>
            <th width=\"10%\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_invite.register_user"), "html", null, true);
        echo "</th>
            <th width=\"15%\">";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_invite.payingUserTotalPrice_th"), "html", null, true);
        echo "</th>
            <th width=\"15%\">";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_invite.coinAmountPrice_th"), "html", null, true);
        echo "</th>
            <th width=\"15%\">";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_invite.amountPrice_th"), "html", null, true);
        echo "</th>
            <th width=\"10%\">";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.register.invite_code_label"), "html", null, true);
        echo "</th>
            <th width=\"15%\">";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.account.my_invite_code.invite_time"), "html", null, true);
        echo "</th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 37
        if (((array_key_exists("records", $context)) ? (_twig_default_filter((isset($context["records"]) ? $context["records"] : null))) : (""))) {
            // line 38
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(((array_key_exists("records", $context)) ? (_twig_default_filter((isset($context["records"]) ? $context["records"] : null))) : ("")));
            foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
                // line 39
                echo "                ";
                $context["inviteUser"] = $this->getAttribute((isset($context["users"]) ? $context["users"] : null), $this->getAttribute($context["record"], "inviteUserId", array()), array(), "array");
                // line 40
                echo "                ";
                $context["invitedUser"] = $this->getAttribute((isset($context["users"]) ? $context["users"] : null), $this->getAttribute($context["record"], "invitedUserId", array()), array(), "array");
                // line 41
                echo "               <tr>
                   <td>";
                // line 42
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["inviteUser"]) ? $context["inviteUser"] : null), "nickname", array()), "html", null, true);
                echo "</td>
                   <td>";
                // line 43
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["invitedUser"]) ? $context["invitedUser"] : null), "nickname", array()), "html", null, true);
                echo "</td>
                   <td>";
                // line 44
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "amount", array()), "html", null, true);
                echo "</td>
                   <td>";
                // line 45
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "coinAmount", array()), "html", null, true);
                echo "</td>
                   <td>";
                // line 46
                echo twig_escape_filter($this->env, $this->getAttribute($context["record"], "cashAmount", array()), "html", null, true);
                echo "</td>
                   <td>";
                // line 47
                echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["inviteUser"]) ? $context["inviteUser"] : null), "inviteCode", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["inviteUser"]) ? $context["inviteUser"] : null), "inviteCode", array()), "-")) : ("-")), "html", null, true);
                echo "</td>
                   <td width=\"10%\">";
                // line 48
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["record"], "inviteTime", array()), "Y-m-d H:i:s"), "html", null, true);
                echo "</td>
               </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 51
            echo "        ";
        } else {
            // line 52
            echo "            <tr>
                <td colspan=\"20\">
                    <div class=\"empty\">
                        ";
            // line 55
            if (((array_key_exists("error", $context)) ? (_twig_default_filter((isset($context["error"]) ? $context["error"] : null))) : (""))) {
                // line 56
                echo "                            ";
                echo twig_escape_filter($this->env, ((array_key_exists("error", $context)) ? (_twig_default_filter((isset($context["error"]) ? $context["error"] : null))) : ("")), "html", null, true);
                echo "
                        ";
            } else {
                // line 58
                echo "                            ";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.datagrid.empty"), "html", null, true);
                echo "
                        ";
            }
            // line 60
            echo "                    </div>
                </td>
            </tr>
        ";
        }
        // line 64
        echo "        </tbody>
    </table>

    ";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin_macro"]) ? $context["admin_macro"] : null), "paginator", array(0 => ((array_key_exists("paginator", $context)) ? (_twig_default_filter((isset($context["paginator"]) ? $context["paginator"] : null))) : (""))), "method"), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "admin/invite/records.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 67,  185 => 64,  179 => 60,  173 => 58,  167 => 56,  165 => 55,  160 => 52,  157 => 51,  148 => 48,  144 => 47,  140 => 46,  136 => 45,  132 => 44,  128 => 43,  124 => 42,  121 => 41,  118 => 40,  115 => 39,  110 => 38,  108 => 37,  101 => 33,  97 => 32,  93 => 31,  89 => 30,  85 => 29,  81 => 28,  77 => 27,  70 => 22,  68 => 17,  64 => 16,  57 => 14,  49 => 11,  41 => 8,  36 => 7,  33 => 6,  29 => 1,  27 => 4,  25 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/invite/records.html.twig", "/var/www/edusoho/app/Resources/views/admin/invite/records.html.twig");
    }
}
