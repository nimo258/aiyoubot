<?php

/* admin/block/list-tr.html.twig */
class __TwigTemplate_80d0c982549f21c03601c2d0710fd25d0f1e8f0dc7fbdc452830d215e4d18d78 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["admin_macro"] = $this->loadTemplate("admin/macro.html.twig", "admin/block/list-tr.html.twig", 1);
        // line 2
        echo "<tr id=\"blockitem";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["blockTemplate"]) ? $context["blockTemplate"] : null), "id", array()), "html", null, true);
        echo "\">
    <td>
        ";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["blockTemplate"]) ? $context["blockTemplate"] : null), "title", array()), "html", null, true);
        echo "
        <br>
        <span class=\"text-muted text-sm\">";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.code", array("%code%" => $this->getAttribute((isset($context["blockTemplate"]) ? $context["blockTemplate"] : null), "code", array()))), "html", null, true);
        echo "</span>
    </td>
    <td>
        ";
        // line 9
        if ((($this->getAttribute((isset($context["latestHistory"]) ? $context["latestHistory"] : null), "userId", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["latestHistory"]) ? $context["latestHistory"] : null), "userId", array()), null)) : (null))) {
            // line 10
            echo "        ";
            echo $context["admin_macro"]->getuser_link(((array_key_exists("latestUpdateUser", $context)) ? (_twig_default_filter((isset($context["latestUpdateUser"]) ? $context["latestUpdateUser"] : null), null)) : (null)));
            echo "
        ";
        } else {
            // line 12
            echo "        <span class=\"text-muted\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.no_edit"), "html", null, true);
            echo "</span>
        ";
        }
        // line 14
        echo "        <br>
        ";
        // line 15
        if ((($this->getAttribute((isset($context["latestHistory"]) ? $context["latestHistory"] : null), "createdTime", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["latestHistory"]) ? $context["latestHistory"] : null), "createdTime", array()), null)) : (null))) {
            // line 16
            echo "        <span class=\"text-muted text-sm\">";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["latestHistory"]) ? $context["latestHistory"] : null), "createdTime", array()), "Y-n-d H:i:s"), "html", null, true);
            echo "</span>
        ";
        }
        // line 18
        echo "    </td>
    <td>
        ";
        // line 20
        if (($this->getAttribute((isset($context["blockTemplate"]) ? $context["blockTemplate"] : null), "mode", array()) == "html")) {
            // line 21
            echo "            <button class=\"btn btn-sm btn-primary update-btn\" data-url=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_block_update", array("blockTemplateId" => $this->getAttribute((isset($context["blockTemplate"]) ? $context["blockTemplate"] : null), "id", array()))), "html", null, true);
            echo "\" data-toggle=\"modal\" data-target=\"#modal\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.edit_html"), "html", null, true);
            echo "</button>
        ";
        }
        // line 23
        echo "
        ";
        // line 24
        if (($this->getAttribute((isset($context["blockTemplate"]) ? $context["blockTemplate"] : null), "mode", array()) == "template")) {
            // line 25
            echo "            <a class=\"btn btn-sm btn-primary update-btn\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_block_visual_edit", array("blockTemplateId" => $this->getAttribute((isset($context["blockTemplate"]) ? $context["blockTemplate"] : null), "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.edit_content"), "html", null, true);
            echo "</a>
        ";
        }
        // line 27
        echo "
        ";
        // line 28
        if ($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("developer.debug")) {
            // line 29
            echo "        
        <button class=\"btn btn-sm btn-default edit-btn hidden\" data-url=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_block_edit", array("blockTemplateId" => $this->getAttribute((isset($context["blockTemplate"]) ? $context["blockTemplate"] : null), "id", array()))), "html", null, true);
            echo "\" data-toggle=\"modal\" data-target=\"#modal\" >";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.set_up"), "html", null, true);
            echo "</button>
        <button class=\"btn btn-sm btn-default delete-btn\" data-url=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_block_delete", array("id" => $this->getAttribute((isset($context["blockTemplate"]) ? $context["blockTemplate"] : null), "id", array()))), "html", null, true);
            echo "\" data-target=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["blockTemplate"]) ? $context["blockTemplate"] : null), "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.delete"), "html", null, true);
            echo "</button>
        ";
        }
        // line 33
        echo "    </td>
</tr>";
    }

    public function getTemplateName()
    {
        return "admin/block/list-tr.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 33,  104 => 31,  98 => 30,  95 => 29,  93 => 28,  90 => 27,  82 => 25,  80 => 24,  77 => 23,  69 => 21,  67 => 20,  63 => 18,  57 => 16,  55 => 15,  52 => 14,  46 => 12,  40 => 10,  38 => 9,  32 => 6,  27 => 4,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/block/list-tr.html.twig", "/var/www/edusoho/app/Resources/views/admin/block/list-tr.html.twig");
    }
}
