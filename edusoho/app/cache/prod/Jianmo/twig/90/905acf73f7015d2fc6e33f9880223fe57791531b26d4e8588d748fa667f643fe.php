<?php

/* admin/block/block-visual-layout.html.twig */
class __TwigTemplate_217c5d3a84af10524da69271ae96ce5ede025fa79b11b423981ffff97759c688 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/block/block-visual-layout.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
            'blockVisual' => array($this, 'block_blockVisual'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "  <ul class=\"nav nav-pills mbl small\">
    <li ";
        // line 5
        if (((isset($context["currentPage"]) ? $context["currentPage"] : null) == "admin_block_visual_edit")) {
            echo "class=\"active\"";
        }
        echo ">
      <a href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_block_visual_edit", array("blockTemplateId" => $this->getAttribute((isset($context["block"]) ? $context["block"] : null), "blockTemplateId", array()))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit"), "html", null, true);
        echo "</a>

    </li>
    <li ";
        // line 9
        if (((isset($context["currentPage"]) ? $context["currentPage"] : null) == "admin_block_visual_edit_history")) {
            echo "class=\"active\"";
        }
        echo ">
      <a href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_block_visual_edit_history", array("blockTemplateId" => $this->getAttribute((isset($context["block"]) ? $context["block"] : null), "blockTemplateId", array()))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit_history"), "html", null, true);
        echo "</a>
    </li>
  </ul>
  ";
        // line 13
        $this->displayBlock('blockVisual', $context, $blocks);
    }

    public function block_blockVisual($context, array $blocks = array())
    {
        echo " ";
    }

    public function getTemplateName()
    {
        return "admin/block/block-visual-layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 13,  55 => 10,  49 => 9,  41 => 6,  35 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/block/block-visual-layout.html.twig", "/var/www/edusoho/app/Resources/views/admin/block/block-visual-layout.html.twig");
    }
}
