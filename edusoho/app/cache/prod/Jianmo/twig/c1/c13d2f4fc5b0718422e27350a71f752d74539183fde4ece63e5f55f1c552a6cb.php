<?php

/* attachment/form-fields.html.twig */
class __TwigTemplate_f7d44ef93f610b59a8776bd363670a38c9d1bcaeb23e7069c2323f176b16dae4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("cloud_attachment.enable") && $this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting(("cloud_attachment." . (isset($context["target"]) ? $context["target"] : null)))) && ($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("storage.upload_mode") == "cloud"))) {
            // line 2
            echo "  ";
            $context["ids_class"] = ((((isset($context["useType"]) ? $context["useType"] : null) == true)) ? (("js-attachment-ids-" . (isset($context["fileType"]) ? $context["fileType"] : null))) : ("js-attachment-ids"));
            // line 3
            echo "  ";
            $context["list_class"] = ((((isset($context["useType"]) ? $context["useType"] : null) == true)) ? (("js-attachment-list-" . (isset($context["fileType"]) ? $context["fileType"] : null))) : ("js-attachment-list"));
            // line 4
            echo "  ";
            $context["reupload"] = twig_length_filter($this->env, ((array_key_exists("attachments", $context)) ? (_twig_default_filter((isset($context["attachments"]) ? $context["attachments"] : null))) : ("")));
            // line 5
            echo "
  ";
            // line 6
            if ((((array_key_exists("bundle_namespace", $context)) ? (_twig_default_filter((isset($context["bundle_namespace"]) ? $context["bundle_namespace"] : null), null)) : (null)) == "admin")) {
                // line 7
                echo "    ";
                $this->loadTemplate("seajs_loader_compatible.html.twig", "attachment/form-fields.html.twig", 7)->display(array_merge($context, array("topxiaadminbundle" => true)));
                // line 8
                echo "  ";
            } else {
                // line 9
                echo "    ";
                $this->loadTemplate("seajs_loader_compatible.html.twig", "attachment/form-fields.html.twig", 9)->display(array_merge($context, array("topxiawebbundle" => true)));
                // line 10
                echo "  ";
            }
            echo "  
  
  <div class=\"form-group\">
    ";
            // line 13
            if (((array_key_exists("showLabel", $context)) ? (_twig_default_filter((isset($context["showLabel"]) ? $context["showLabel"] : null), false)) : (false))) {
                // line 14
                echo "      <label class=\"col-xs-2 control-label\" for=\"thread_title\">";
                if (((isset($context["targetType"]) ? $context["targetType"] : null) == "question.stem")) {
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.thread.create.question_stem_attachment"), "html", null, true);
                } elseif (((isset($context["targetType"]) ? $context["targetType"] : null) == "question.analysis")) {
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.thread.create.analysis_attachment"), "html", null, true);
                } else {
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.thread.create.attachments"), "html", null, true);
                }
                echo "</label>
      <div class=\"col-xs-7 controls\">
    ";
            } else {
                // line 17
                echo "      <div class=\"controls\"> 
    ";
            }
            // line 19
            echo "      <div class=\"js-attachment-list ";
            echo twig_escape_filter($this->env, (isset($context["list_class"]) ? $context["list_class"] : null), "html", null, true);
            echo "\" style=\"line-height:60px\">
        ";
            // line 20
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attachments"]) ? $context["attachments"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["attachment"]) {
                if ($this->getAttribute($context["attachment"], "file", array())) {
                    // line 21
                    echo "          <div class=\"well well-sm\">
            <img class=\"mrm\" src=\"";
                    // line 22
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl((("assets/img/default/cloud_" . $this->getAttribute($this->getAttribute($context["attachment"], "file", array()), "type", array())) . ".png")), "html", null, true);
                    echo "\" height=\"60px\" width=\"107px\">
            ";
                    // line 23
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["attachment"], "file", array()), "filename", array()), "html", null, true);
                    echo "
            <button class=\"btn btn-link js-attachment-delete pull-right\" data-url=\"";
                    // line 24
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("attachment_delete", array("id" => $this->getAttribute($context["attachment"], "id", array()))), "html", null, true);
                    echo "\" type=\"button\" style=\"margin-top:13px\" data-role=\"delte-item\" data-loading-text=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.delete.submiting"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.delete"), "html", null, true);
                    echo "</button> 
            <a class=\"btn btn-link pull-right\" href=\"javascript:;\" style=\"margin-top:13px\" data-url=\"";
                    // line 25
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("attachment_preview", array("id" => $this->getAttribute($context["attachment"], "id", array()))), "html", null, true);
                    echo "\" data-toggle=\"modal\" data-target=\"#modal\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.thread.create.preview"), "html", null, true);
                    echo "</a>
          </div>
        ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attachment'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "      </div>
      
      <a class=\"cd-btn cd-btn-primary js-upload-file\" ";
            // line 30
            if ((isset($context["reupload"]) ? $context["reupload"] : null)) {
                echo "style=\"display: none;\"";
            }
            echo " data-toggle=\"modal\" data-backdrop=\"static\"
         data-target=\"#attachment-modal\"
         data-url=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("attachment_upload", array("useSeajs" => ((array_key_exists("useSeajs", $context)) ? (_twig_default_filter((isset($context["useSeajs"]) ? $context["useSeajs"] : null), false)) : (false)), "idsClass" => (isset($context["ids_class"]) ? $context["ids_class"] : null), "listClass" => (isset($context["list_class"]) ? $context["list_class"] : null), "token" => $this->env->getExtension('AppBundle\Twig\UploaderExtension')->makeUpoaderToken("attachment", $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()), "private"), "currentTarget" => (isset($context["currentTarget"]) ? $context["currentTarget"] : null))), "html", null, true);
            echo "\"
         title=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("attachments.upload"), "html", null, true);
            echo "\" data-placement=\"bottom\" data-title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("attachments.upload"), "html", null, true);
            echo "\">
         ";
            // line 34
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("attachments.upload"), "html", null, true);
            echo "
      </a>
    </div>
    ";
            // line 38
            echo "    
    <input class=\"";
            // line 39
            echo twig_escape_filter($this->env, (isset($context["ids_class"]) ? $context["ids_class"] : null), "html", null, true);
            echo "\" 
      value=\"";
            // line 40
            echo twig_escape_filter($this->env, twig_join_filter($this->env->getExtension('AppBundle\Twig\WebExtension')->arrayColumn((isset($context["attachments"]) ? $context["attachments"] : null), "fileId"), ","), "html", null, true);
            echo "\" 
      name=\"";
            // line 41
            if (((array_key_exists("useType", $context)) ? (_twig_default_filter((isset($context["useType"]) ? $context["useType"] : null), false)) : (false))) {
                echo "attachment[";
                echo twig_escape_filter($this->env, (isset($context["fileType"]) ? $context["fileType"] : null), "html", null, true);
                echo "][fileIds]";
            } else {
                echo "attachment[fileIds]";
            }
            echo "\" 
      type=\"hidden\" data-role=\"fileId\">

    <input class=\"js-file-target-type\" 
      value=\"";
            // line 45
            echo twig_escape_filter($this->env, (isset($context["targetType"]) ? $context["targetType"] : null), "html", null, true);
            echo "\" 
      name=\"";
            // line 46
            if (((array_key_exists("useType", $context)) ? (_twig_default_filter((isset($context["useType"]) ? $context["useType"] : null), false)) : (false))) {
                echo "attachment[";
                echo twig_escape_filter($this->env, (isset($context["fileType"]) ? $context["fileType"] : null), "html", null, true);
                echo "][targetType]";
            } else {
                echo "attachment[targetType]";
            }
            echo "\"
      type=\"hidden\">

    <input class=\"js-file-type\" 
      value=\"";
            // line 50
            echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : null), "html", null, true);
            echo "\" 
      name=\"";
            // line 51
            if (((array_key_exists("useType", $context)) ? (_twig_default_filter((isset($context["useType"]) ? $context["useType"] : null), false)) : (false))) {
                echo "attachment[";
                echo twig_escape_filter($this->env, (isset($context["fileType"]) ? $context["fileType"] : null), "html", null, true);
                echo "][type]";
            } else {
                echo "attachment[type]";
            }
            echo "\"
      type=\"hidden\">
  </div>
  ";
        }
    }

    public function getTemplateName()
    {
        return "attachment/form-fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 51,  179 => 50,  166 => 46,  162 => 45,  149 => 41,  145 => 40,  141 => 39,  138 => 38,  132 => 34,  126 => 33,  122 => 32,  115 => 30,  111 => 28,  99 => 25,  91 => 24,  87 => 23,  83 => 22,  80 => 21,  75 => 20,  70 => 19,  66 => 17,  53 => 14,  51 => 13,  44 => 10,  41 => 9,  38 => 8,  35 => 7,  33 => 6,  30 => 5,  27 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "attachment/form-fields.html.twig", "/var/www/edusoho/app/Resources/views/attachment/form-fields.html.twig");
    }
}
