<?php

/* admin/course-thread/index.html.twig */
class __TwigTemplate_13bd24ec43bf3a9efefe35ab653cee921674298328124b37c586c102f518a5d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/course-thread/index.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["script_controller"] = "course/threads";
        // line 5
        $context["menu"] = "admin_course_thread_manage";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        // line 8
        echo "
<div class=\"well well-sm\">
  <form class=\"form-inline\">
    <div class=\"form-group\">
      <select class=\"form-control\" name=\"type\">
        ";
        // line 13
        echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->selectOptions($this->env->getExtension('Codeages\PluginBundle\Twig\DictExtension')->getDict("threadType"), $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "type"), "method"), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.course_thread_manage_tab.form.type_option"));
        echo "
      </select>
    </div>
    
    <span class=\"divider\"></span>

    <div class=\"form-group\">
      <select class=\"form-control\" name=\"threadType\">
        ";
        // line 21
        echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->selectOptions(array("isStick" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.course_thread_manage_tab.form.thread_type.isStick_option"), "isElite" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.course_thread_manage_tab.form.thread_type.isElite_option")), $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "threadType"), "method"), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.course_thread_manage_tab.form.thread_type_option"));
        echo "
      </select>
    </div>

    <span class=\"divider\"></span>

    <div class=\"form-group\">
      <select class=\"form-control\" name=\"keywordType\">
        ";
        // line 29
        echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->selectOptions(array("title" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.form.keyword_type.title_option"), "content" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.form.keyword_type.content_option"), "courseId" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.form.keyword_type.courseId_option"), "courseTitle" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.form.keyword_type.courseTitle_option")), $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "keywordType"), "method"));
        echo "
      </select>
    </div>

    <div class=\"form-group\">
      <input class=\"form-control\" type=\"text\" placeholder=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.form.input.keyword.placeholer"), "html", null, true);
        echo "\" name=\"keyword\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "keyword"), "method"), "html", null, true);
        echo "\">
    </div>

    <div class=\"form-group\">
      <input class=\"form-control\" type=\"text\" placeholder=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.form.input.author.placeholer"), "html", null, true);
        echo "\" name=\"author\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "author"), "method"), "html", null, true);
        echo "\">
    </div>

    <button class=\"btn btn-primary\" type=\"submit\">";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.search"), "html", null, true);
        echo "</button>
  </form>
</div>

  <div id=\"thread-table-container\">
    <table class=\"table table-striped table-hover\">
      <thead>
        <tr>
          <th width=\"5%\"><input type=\"checkbox\" data-role=\"batch-select\"></th>
          <th width=\"50%\">";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.post_th"), "html", null, true);
        echo "</th>
          <th width=\"10%\">";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.reply_or_review_th"), "html", null, true);
        echo "</th>
          <th width=\"10%\">";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.type_th"), "html", null, true);
        echo "</th>
          <th width=\"15%\">";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.author_th"), "html", null, true);
        echo "</th>
          <th width=\"10%\">";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.operation_th"), "html", null, true);
        echo "</th>
        </tr>
      </thead>
      <body>
        ";
        // line 58
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["threads"]) ? $context["threads"] : null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["thread"]) {
            // line 59
            echo "          ";
            $context["author"] = (($this->getAttribute((isset($context["users"]) ? $context["users"] : null), $this->getAttribute($context["thread"], "userId", array()), array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["users"]) ? $context["users"] : null), $this->getAttribute($context["thread"], "userId", array()), array(), "array"), null)) : (null));
            // line 60
            echo "          ";
            $context["courseSet"] = (($this->getAttribute((isset($context["courseSets"]) ? $context["courseSets"] : null), $this->getAttribute($context["thread"], "courseSetId", array()), array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["courseSets"]) ? $context["courseSets"] : null), $this->getAttribute($context["thread"], "courseSetId", array()), array(), "array"), null)) : (null));
            // line 61
            echo "          ";
            $context["course"] = (($this->getAttribute((isset($context["courses"]) ? $context["courses"] : null), $this->getAttribute($context["thread"], "courseId", array()), array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["courses"]) ? $context["courses"] : null), $this->getAttribute($context["thread"], "courseId", array()), array(), "array"), null)) : (null));
            // line 62
            echo "          ";
            $context["task"] = (($this->getAttribute((isset($context["tasks"]) ? $context["tasks"] : null), $this->getAttribute($context["thread"], "taskId", array()), array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["tasks"]) ? $context["tasks"] : null), $this->getAttribute($context["thread"], "taskId", array()), array(), "array"), null)) : (null));
            // line 63
            echo "          ";
            if ((($this->getAttribute($context["thread"], "type", array()) == "question") && ($this->getAttribute($context["thread"], "title", array()) == ""))) {
                // line 64
                echo "            ";
                if (($this->getAttribute($context["thread"], "questionType", array()) == "video")) {
                    // line 65
                    echo "              ";
                    $context["questionType"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question_type.video");
                    // line 66
                    echo "            ";
                } elseif (($this->getAttribute($context["thread"], "questionType", array()) == "image")) {
                    // line 67
                    echo "              ";
                    $context["questionType"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question_type.image");
                    // line 68
                    echo "            ";
                } elseif (($this->getAttribute($context["thread"], "questionType", array()) == "audio")) {
                    // line 69
                    echo "              ";
                    $context["questionType"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question_type.audio");
                    // line 70
                    echo "            ";
                } elseif (($this->getAttribute($context["thread"], "questionType", array()) == "content")) {
                    // line 71
                    echo "              ";
                    $context["questionType"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question_type.content");
                    // line 72
                    echo "            ";
                }
                // line 73
                echo "          ";
            }
            // line 74
            echo "          <tr data-role=\"item\">
            <td><input value=\"";
            // line 75
            echo twig_escape_filter($this->env, $this->getAttribute($context["thread"], "id", array()), "html", null, true);
            echo "\" type=\"checkbox\" data-role=\"batch-item\"> </td>
            <td>
              ";
            // line 77
            if (($this->getAttribute($context["thread"], "type", array()) == "question")) {
                // line 78
                echo "                <span class=\"label label-info\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.question_td_label"), "html", null, true);
                echo "</span>
              ";
            }
            // line 80
            echo "
              <a href=\"";
            // line 81
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_thread_show", array("courseId" => $this->getAttribute($context["thread"], "courseId", array()), "threadId" => $this->getAttribute($context["thread"], "id", array()))), "html", null, true);
            echo "\" target=\"_blank\"><strong>";
            echo twig_escape_filter($this->env, (($this->getAttribute($context["thread"], "title", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["thread"], "title", array()), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question.title", array("%questionType%" => ((array_key_exists("questionType", $context)) ? (_twig_default_filter((isset($context["questionType"]) ? $context["questionType"] : null), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question_type.content"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question_type.content"))))))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question.title", array("%questionType%" => ((array_key_exists("questionType", $context)) ? (_twig_default_filter((isset($context["questionType"]) ? $context["questionType"] : null), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question_type.content"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question_type.content"))))))), "html", null, true);
            echo "</strong></a>

              <div class=\"short-long-text\">
                <div class=\"short-text text-sm text-muted\">";
            // line 84
            echo $this->env->getExtension('AppBundle\Twig\WebExtension')->plainTextFilter($this->getAttribute($context["thread"], "content", array()), 60);
            echo " <span class=\"trigger\">(";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.expand_td"), "html", null, true);
            echo ")</span></div>
                <div class=\"long-text\">";
            // line 85
            echo $this->getAttribute($context["thread"], "content", array());
            echo " <span class=\"trigger\">(";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.collapse_td"), "html", null, true);
            echo ")</span></div>
              </div>
              
              <div class=\"text-sm mts\">
                ";
            // line 89
            if ((isset($context["courseSet"]) ? $context["courseSet"] : null)) {
                // line 90
                echo "                  <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_show", array("id" => $this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "defaultCourseId", array()))), "html", null, true);
                echo "\" class=\"text-success\" target=\"_blank\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "title", array()), "html", null, true);
                echo "</a>
                ";
            }
            // line 92
            echo "
                ";
            // line 93
            if ((isset($context["course"]) ? $context["course"] : null)) {
                // line 94
                echo "                  <span class=\"text-muted mhs\">&raquo;</span>
                  <a href=\"";
                // line 95
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_show", array("id" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "id", array()))), "html", null, true);
                echo "\" class=\"text-success\" target=\"_blank\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "title", array()), "html", null, true);
                echo "</a>
                ";
            }
            // line 97
            echo "
                ";
            // line 98
            if ((isset($context["task"]) ? $context["task"] : null)) {
                // line 99
                echo "                  <span class=\"text-muted mhs\">&raquo;</span>
                  <a class=\"text-success\"  href=\"";
                // line 100
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_task_show", array("courseId" => $this->getAttribute($context["thread"], "courseId", array()), "id" => $this->getAttribute((isset($context["task"]) ? $context["task"] : null), "id", array()))), "html", null, true);
                echo "\" target=\"_blank\">";
                echo twig_escape_filter($this->env, _twig_default_filter($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("course.task_name"), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_course.task")), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["task"]) ? $context["task"] : null), "number", array()), "html", null, true);
                echo "：";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["task"]) ? $context["task"] : null), "title", array()), "html", null, true);
                echo "</a>
                ";
            }
            // line 102
            echo "              </div>
            </td>
            <td><span class=\"text-sm\">";
            // line 104
            echo twig_escape_filter($this->env, $this->getAttribute($context["thread"], "postNum", array()), "html", null, true);
            echo " / ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["thread"], "hitNum", array()), "html", null, true);
            echo "</span></td>
            <td>
              ";
            // line 106
            if ((isset($context["course"]) ? $context["course"] : null)) {
                // line 107
                echo "                <a href=\"javascript:;\" data-set-url=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_thread_elite", array("courseId" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "id", array()), "threadId" => $this->getAttribute($context["thread"], "id", array()))), "html", null, true);
                echo "\" data-cancel-url=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_thread_unelite", array("courseId" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "id", array()), "threadId" => $this->getAttribute($context["thread"], "id", array()))), "html", null, true);
                echo "\" class=\"promoted-label\">
                  <span class=\"label ";
                // line 108
                if ($this->getAttribute($context["thread"], "isElite", array())) {
                    echo "label-success";
                } else {
                    echo "label-default";
                }
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.course_thread_manage_tab.isElite_btn"), "html", null, true);
                echo "</span>
                </a>
              
                <a href=\"javascript:;\" data-set-url=\"";
                // line 111
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_thread_stick", array("courseId" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "id", array()), "threadId" => $this->getAttribute($context["thread"], "id", array()))), "html", null, true);
                echo "\" data-cancel-url=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_thread_unstick", array("courseId" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "id", array()), "threadId" => $this->getAttribute($context["thread"], "id", array()))), "html", null, true);
                echo "\" class=\"promoted-label\">
                  <span class=\"label ";
                // line 112
                if ($this->getAttribute($context["thread"], "isStick", array())) {
                    echo "label-success";
                } else {
                    echo "label-default";
                }
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.course_thread_manage_tab.isStick_btn"), "html", null, true);
                echo "</span>
                </a>
              ";
            }
            // line 115
            echo "            </td>
            <td>
              ";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin_macro"]) ? $context["admin_macro"] : null), "user_link", array(0 => (isset($context["author"]) ? $context["author"] : null)), "method"), "html", null, true);
            echo " <br />
              <span class=\"text-muted text-sm\">";
            // line 118
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["thread"], "createdTime", array()), "Y-n-d H:i:s"), "html", null, true);
            echo "</span>
            </td>
            <td>
              <div class=\"btn-group\">
                <a href=\"javascript:;\" data-role=\"item-delete\" data-url=\"";
            // line 122
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_thread_delete", array("id" => $this->getAttribute($context["thread"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default btn-sm\" 
                data-name=\"";
            // line 123
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.post_th"), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.delete"), "html", null, true);
            echo "</a>
              </div>
            </td>
          </tr>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 128
            echo "          <tr><td colspan=\"20\"><div class=\"empty\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.datagrid.empty"), "html", null, true);
            echo "</div></td></tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['thread'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 130
        echo "      </body>
    </table>

    <div class=\"mbm\">
        <label class=\"checkbox-inline\"><input type=\"checkbox\" data-role=\"batch-select\"> ";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.select_all_btn"), "html", null, true);
        echo "</label>
        <button class=\"btn btn-default btn-sm mlm\" data-role=\"batch-delete\" data-name=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.post_th"), "html", null, true);
        echo "\" data-url=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_thread_batch_delete");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.delete"), "html", null, true);
        echo "</button>
    </div>

  </div>
    
  ";
        // line 140
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin_macro"]) ? $context["admin_macro"] : null), "paginator", array(0 => (isset($context["paginator"]) ? $context["paginator"] : null)), "method"), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "admin/course-thread/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  372 => 140,  360 => 135,  356 => 134,  350 => 130,  341 => 128,  329 => 123,  325 => 122,  318 => 118,  314 => 117,  310 => 115,  298 => 112,  292 => 111,  280 => 108,  273 => 107,  271 => 106,  264 => 104,  260 => 102,  250 => 100,  247 => 99,  245 => 98,  242 => 97,  235 => 95,  232 => 94,  230 => 93,  227 => 92,  219 => 90,  217 => 89,  208 => 85,  202 => 84,  194 => 81,  191 => 80,  185 => 78,  183 => 77,  178 => 75,  175 => 74,  172 => 73,  169 => 72,  166 => 71,  163 => 70,  160 => 69,  157 => 68,  154 => 67,  151 => 66,  148 => 65,  145 => 64,  142 => 63,  139 => 62,  136 => 61,  133 => 60,  130 => 59,  125 => 58,  118 => 54,  114 => 53,  110 => 52,  106 => 51,  102 => 50,  90 => 41,  82 => 38,  73 => 34,  65 => 29,  54 => 21,  43 => 13,  36 => 8,  33 => 7,  29 => 1,  27 => 5,  25 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/course-thread/index.html.twig", "/var/www/edusoho/app/Resources/views/admin/course-thread/index.html.twig");
    }
}
