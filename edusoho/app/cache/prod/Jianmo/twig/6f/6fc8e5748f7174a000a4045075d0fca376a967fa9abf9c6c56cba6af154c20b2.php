<?php

/* admin/block/label/link.html.twig */
class __TwigTemplate_3c08048efe4db824232355a5492fd25bb7d10c82eb767ac048a71c72add8e51d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<a class=\"title-label js-title-label ellipsis\" style=\"color: #428bca;\" href=\"";
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "href", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "href", array()), null)) : (null)), "html", null, true);
        echo "\" target=\"_blank\">";
        echo $this->env->getExtension('AppBundle\Twig\WebExtension')->plainTextFilter((($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "value", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "value", array()), null)) : (null)), 40);
        echo "</a>
";
    }

    public function getTemplateName()
    {
        return "admin/block/label/link.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/block/label/link.html.twig", "/var/www/edusoho/app/Resources/views/admin/block/label/link.html.twig");
    }
}
