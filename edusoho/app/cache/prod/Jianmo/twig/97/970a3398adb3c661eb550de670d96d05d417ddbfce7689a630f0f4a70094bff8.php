<?php

/* admin/theme/base-edit.html.twig */
class __TwigTemplate_53ebea8090235ff3d80e829100c1aea81018ef561379ef7ab0819625b01be7a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/theme/base-edit.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'theme_nav' => array($this, 'block_theme_nav'),
            'theme_panes' => array($this, 'block_theme_panes'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["script_controller"] = "topxiaadminbundle/controller/theme/edit";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage"), "html", null, true);
        echo "
";
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "  <style>
    body {
      padding-bottom: 0
    }
    .list-group {
      height: 244px;
      overflow:auto;
    }

    .iframe-box {
      z-index: 1024;
      position: relative;
      border: 1px solid #ccc;
    }

    .iframe-box:after {
      content: '';
      position: absolute;
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
    }

    .radios, .checkboxs {
      padding-top: 20px;
      vertical-align: middle;
    }

    .mod-cpanel-basic-color {
      width: 85px;
      height: 25px;
      border: 1px solid #aaa;
      cursor: pointer;
    }

    .edit-sort {
      color: #aaa;
      margin-right: 20px;
    }

    .edit-sort:hover {
      color: #444;
    }
  </style>
  <body style=\"background: #eeeff0\">
    <script id=\"theme-config\" type=\"text/x-handlebars-template\">";
        // line 53
        echo twig_jsonencode_filter((isset($context["themeConfig"]) ? $context["themeConfig"] : null));
        echo "</script>
    <script id=\"theme-all-config\" type=\"text/x-handlebars-template\">";
        // line 54
        echo twig_jsonencode_filter((isset($context["allConfig"]) ? $context["allConfig"] : null));
        echo "</script>

    <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
      <div class=\"container\">
        <div class=\"col-md-12\">
          <a class=\"navbar-brand\">";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.theme_edit"), "html", null, true);
        echo "</a>
          <ul class=\"nav navbar-nav navbar-right\">
          <li><a href=\"";
        // line 61
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_setting_theme");
        echo "\"><i class=\"glyphicon glyphicon-remove\"></i>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.theme_cancel"), "html", null, true);
        echo "</a></li>
          <li><a href=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_confirm_themes_config", array("uri" => (isset($context["themeUri"]) ? $context["themeUri"] : null))), "html", null, true);
        echo "\"><i class=\"glyphicon glyphicon-ok\"></i> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.theme_ok"), "html", null, true);
        echo "</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <div class=\"container\">
      <div class=\"col-md-12\">
        <!-- Nav tabs -->
        <ul class=\"nav nav-tabs\" role=\"tablist\">
          ";
        // line 71
        $this->displayBlock('theme_nav', $context, $blocks);
        // line 73
        echo "          <a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_reset_currentTheme_config", array("uri" => (isset($context["themeUri"]) ? $context["themeUri"] : null))), "html", null, true);
        echo "\" class=\"btn btn-primary pull-right\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.all_reset"), "html", null, true);
        echo "</a>
        </ul>

        <!-- Tab panes -->
        <div class=\"tab-content\" id=\"theme-edit-content\" data-url=\"";
        // line 77
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_save_themes_config");
        echo "\">
          ";
        // line 78
        $this->displayBlock('theme_panes', $context, $blocks);
        // line 80
        echo "        </div>
        
      </div>
    </div>

    ";
        // line 85
        $context["iframeUrl"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_themes_show");
        // line 86
        echo "    <div class=\"iframe-box\">
      <iframe src=\"";
        // line 87
        echo twig_escape_filter($this->env, (isset($context["iframeUrl"]) ? $context["iframeUrl"] : null), "html", null, true);
        echo "\" marginheight=\"0\" marginwidth=\"0\" frameborder=\"0\" scrolling=\"no\" width=\"100%\" id=\"iframepage\" name=\"iframepage\">
      </iframe>
    </div>
    ";
        // line 90
        $this->loadTemplate("old-script_boot.html.twig", "admin/theme/base-edit.html.twig", 90)->display(array_merge($context, array("script_main" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/topxiaadmin/js/admin-app.js"))));
        // line 91
        echo "    <div id=\"modal\" class=\"modal\" ></div>
  </body>
";
    }

    // line 71
    public function block_theme_nav($context, array $blocks = array())
    {
        // line 72
        echo "          ";
    }

    // line 78
    public function block_theme_panes($context, array $blocks = array())
    {
        // line 79
        echo "          ";
    }

    public function getTemplateName()
    {
        return "admin/theme/base-edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 79,  183 => 78,  179 => 72,  176 => 71,  170 => 91,  168 => 90,  162 => 87,  159 => 86,  157 => 85,  150 => 80,  148 => 78,  144 => 77,  134 => 73,  132 => 71,  118 => 62,  112 => 61,  107 => 59,  99 => 54,  95 => 53,  47 => 7,  44 => 6,  37 => 4,  34 => 3,  30 => 1,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/theme/base-edit.html.twig", "/var/www/edusoho/app/Resources/views/admin/theme/base-edit.html.twig");
    }
}
