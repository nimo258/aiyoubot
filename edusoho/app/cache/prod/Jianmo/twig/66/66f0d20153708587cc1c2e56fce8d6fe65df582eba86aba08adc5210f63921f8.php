<?php

/* admin/wechat-notification/manage.html.twig */
class __TwigTemplate_c83c55ddab17b8205a7f12eaa70e92d059094023b2350000f3d823f2a40fd8c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/wechat-notification/manage.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["script_controller"] = "wechat-notification/index.js";
        // line 5
        $context["menu"] = "admin_operation_wechat_notification_manage";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        // line 8
        echo "  ";
        if ( !(isset($context["isCloudOpen"]) ? $context["isCloudOpen"] : null)) {
            // line 9
            echo "    <div class=\"alert alert-danger js-click-enable\">
      ";
            // line 10
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("wechat.notification.cloud.tips"), "html", null, true);
            echo "
      <a href=\"";
            // line 11
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_my_cloud_overview");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("wechat.notification.cloud_open"), "html", null, true);
            echo "</a>
    </div>
  ";
        }
        // line 14
        echo "  <div class=\"form-horizontal\">
    <fieldset>
      <legend>";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_notification.switch"), "html", null, true);
        echo "</legend>
      <div class=\"form-group\">
        <div class=\"col-md-3 control-label\">
          <label>";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.wechat_notification"), "html", null, true);
        echo "</label>
        </div>
        <div class=\"controls col-md-1\">
          <p class=\"form-control-static\">
            ";
        // line 23
        if ($this->getAttribute((isset($context["wechatSetting"]) ? $context["wechatSetting"] : null), "wechat_notification_enabled", array())) {
            // line 24
            echo "              ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.payment_setting.api_open"), "html", null, true);
            echo "
            ";
        } else {
            // line 26
            echo "              ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.payment_setting.api_close"), "html", null, true);
            echo "
            ";
        }
        // line 28
        echo "          </p>
        </div>
        <div class=\"controls col-md-2 form-control-static\">
          <a target=\"_blank\" href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_setting_wechat_auth");
        echo "\">
          ";
        // line 32
        if ($this->getAttribute((isset($context["wechatSetting"]) ? $context["wechatSetting"] : null), "wechat_notification_enabled", array())) {
            // line 33
            echo "            ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.payment_setting.api_close.direct"), "html", null, true);
            echo "
          ";
        } else {
            // line 35
            echo "            ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.payment_setting.api_open.direct"), "html", null, true);
            echo "
          ";
        }
        // line 37
        echo "          </a>
        </div>
      </div>
    </fieldset>

    <fieldset>
      <legend>";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_notification.template"), "html", null, true);
        echo "</legend>
      <table class=\"table table-striped table-hover\">
        <thead>
          <tr>
            <th width=\"20%\">";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_notification.template_name"), "html", null, true);
        echo "</th>
            <th width=\"55%\">";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_notification.template_content_sample"), "html", null, true);
        echo "</th>
            <th width=\"15%\">";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_notification.send_object"), "html", null, true);
        echo "</th>
            <th width=\"10%\">";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_notification.operation"), "html", null, true);
        echo "</th>
          </tr>
        </thead>
        <tbody>
          ";
        // line 54
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["templates"]) ? $context["templates"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["template"]) {
            // line 55
            echo "            <tr>
              <td width=\"20%\">";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($context["template"], "name", array()), "html", null, true);
            echo "</td>
              <td width=\"55%\"><a class=\"alert-link\" href=\"#modal\" data-toggle=\"modal\" data-url=\"";
            // line 57
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_wechat_notification_show", array("key" => $context["key"])), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('AppBundle\Twig\WebExtension')->subTextFilter($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($context["template"], "content", array())), 35);
            echo "</td>
              <td width=\"15%\">";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($context["template"], "object", array()), "html", null, true);
            echo "</td>
              <td width=\"10%\">
                <label class=\"form-switch setting-switch ";
            // line 60
            if ($this->getAttribute($context["template"], "status", array())) {
                echo "checked";
            }
            echo "\">
                  <input type=\"checkbox\" name=\"switch\" value=\"";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($context["template"], "status", array()), "html", null, true);
            echo "\" id=\"weixinweb_enabled\" data-toggle=\"switch\" data-url=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_wechat_notification_template_status", array("key" => $context["key"])), "html", null, true);
            echo "\">
                </label>
              </td>
            </tr>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['template'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "        </tbody>
      </table>
    </fieldset>
  </div>
";
    }

    public function getTemplateName()
    {
        return "admin/wechat-notification/manage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 66,  170 => 61,  164 => 60,  159 => 58,  153 => 57,  149 => 56,  146 => 55,  142 => 54,  135 => 50,  131 => 49,  127 => 48,  123 => 47,  116 => 43,  108 => 37,  102 => 35,  96 => 33,  94 => 32,  90 => 31,  85 => 28,  79 => 26,  73 => 24,  71 => 23,  64 => 19,  58 => 16,  54 => 14,  46 => 11,  42 => 10,  39 => 9,  36 => 8,  33 => 7,  29 => 1,  27 => 5,  25 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/wechat-notification/manage.html.twig", "/var/www/edusoho/app/Resources/views/admin/wechat-notification/manage.html.twig");
    }
}
