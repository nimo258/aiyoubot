<?php

/* script/script-custom.html.twig */
class __TwigTemplate_2bb30a2c67c043693e96be87605e6f0cc2d47e4e855e6a8aaf7bc5a276dde3d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->getExtension('Codeages\PluginBundle\Twig\HtmlExtension')->script(array(0 => "jianmotheme/js/main.js"), 999);
    }

    public function getTemplateName()
    {
        return "script/script-custom.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "script/script-custom.html.twig", "/var/www/edusoho/web/themes/jianmo/views/script/script-custom.html.twig");
    }
}
