<?php

/* admin/announcement/index.html.twig */
class __TwigTemplate_f3b6270df9769840da074e27f7149f38aae15b0f7ec1b407754cf6757e976397 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/announcement/index.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["script_controller"] = "announcement/index";
        // line 5
        $context["menu"] = "admin_announcement_manage";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        // line 8
        if ($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("magic.enable_org", "0")) {
            // line 9
            echo "  <form id=\"announcement-search-form\" class=\"form-inline well well-sm\" action=\"\" method=\"get\" novalidate>
    ";
            // line 10
            $this->loadTemplate("org/org-tree-select.html.twig", "admin/announcement/index.html.twig", 10)->display(array_merge($context, array("orgCode" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "orgCode"), "method"), "modal" => "list")));
            // line 11
            echo "    <button class=\"btn btn-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.search"), "html", null, true);
            echo "</button>
  </form>
";
        }
        // line 14
        echo "
  <table id=\"announcement-table\" class=\"table table-striped\">
    <thead>
    <tr>
      ";
        // line 18
        if ($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("magic.enable_org", "0")) {
            // line 19
            echo "        <th><input type=\"checkbox\"  data-role=\"batch-select\"></th>
      ";
        }
        // line 21
        echo "      <th width=\"25%\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_announcement.content_th"), "html", null, true);
        echo "</th>
      ";
        // line 22
        $this->loadTemplate("org/parts/table-thead-tr.html.twig", "admin/announcement/index.html.twig", 22)->display($context);
        // line 23
        echo "      <th>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_announcement.createTime_th"), "html", null, true);
        echo "</th>
      <th>";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_announcement.endTime_th"), "html", null, true);
        echo "</th>
      <th>";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_announcement.status_th"), "html", null, true);
        echo "</th>
      <th>";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_announcement.nickname_th"), "html", null, true);
        echo "</th>
      <th>";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_announcement.operation_th"), "html", null, true);
        echo "</th>
    </tr>
    </thead>
    <tbody>
    ";
        // line 31
        if ((isset($context["announcements"]) ? $context["announcements"] : null)) {
            // line 32
            echo "      ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["announcements"]) ? $context["announcements"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["announcement"]) {
                // line 33
                echo "        <tr id =\"announcement-tr-";
                echo twig_escape_filter($this->env, $this->getAttribute($context["announcement"], "id", array()), "html", null, true);
                echo "\">
          ";
                // line 34
                $this->loadTemplate("org/parts/table-body-checkbox.html.twig", "admin/announcement/index.html.twig", 34)->display($context);
                // line 35
                echo "          <td><a href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["announcement"], "url", array()), "html", null, true);
                echo "\" target=\"_content\">";
                echo $this->getAttribute($context["announcement"], "content", array());
                echo "</td>
          ";
                // line 36
                $this->loadTemplate("org/parts/table-body-td.html.twig", "admin/announcement/index.html.twig", 36)->display(array_merge($context, array("orgCode" => $this->getAttribute($context["announcement"], "orgCode", array()))));
                // line 37
                echo "          <td>";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["announcement"], "startTime", array()), "Y-m-d H:i"), "html", null, true);
                echo "</td>
          <td>";
                // line 38
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["announcement"], "endTime", array()), "Y-m-d H:i"), "html", null, true);
                echo "</td>
          <td>
            ";
                // line 40
                if (((isset($context["now"]) ? $context["now"] : null) < $this->getAttribute($context["announcement"], "startTime", array()))) {
                    // line 41
                    echo "              ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_announcement.status.readyStart"), "html", null, true);
                    echo "
            ";
                } elseif ((                // line 42
(isset($context["now"]) ? $context["now"] : null) > $this->getAttribute($context["announcement"], "endTime", array()))) {
                    // line 43
                    echo "              ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_announcement.status.finished"), "html", null, true);
                    echo "
            ";
                } else {
                    // line 45
                    echo "              ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_announcement.status.display"), "html", null, true);
                    echo "
            ";
                }
                // line 47
                echo "          </td>
          <td>";
                // line 48
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["users"]) ? $context["users"] : null), $this->getAttribute($context["announcement"], "userId", array()), array(), "array"), "nickname", array()), "html", null, true);
                echo "</td>
          <td>
            <div class=\"btn-group\">
              <a data-url=\"";
                // line 51
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_announcement_edit", array("id" => $this->getAttribute($context["announcement"], "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-default btn-sm\"
                 data-toggle=\"modal\" data-target=\"#modal\">";
                // line 52
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_announcement.edit_btn"), "html", null, true);
                echo "</a>
              <a href=\"#\" type=\"button\" class=\"btn btn-default btn-sm dropdown-toggle\" data-toggle=\"dropdown\">
                <span class=\"caret\"></span>
              </a>
              <ul class=\"dropdown-menu\">
                <li>
                  <a href=\"javascript:;\" data-url=\"";
                // line 58
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_announcement_delete", array("id" => $this->getAttribute($context["announcement"], "id", array()))), "html", null, true);
                echo "\"
                       class=\"delete-btn\"><span class=\"glyphicon glyphicon-trash\"></span> ";
                // line 59
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_announcement.delete_btn"), "html", null, true);
                echo "</a>
                </li>
              </ul>
            </div>
          </td>
        </tr>
      ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['announcement'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 66
            echo "    ";
        }
        // line 67
        echo "    </tbody>
  </table>
  ";
        // line 69
        $this->loadTemplate("org/batch-update-org-btn.html.twig", "admin/announcement/index.html.twig", 69)->display(array_merge($context, array("module" => "announcement", "formId" => "announcement-table")));
        // line 70
        echo "  ";
        if ( !(isset($context["announcements"]) ? $context["announcements"] : null)) {
            // line 71
            echo "    <div class=\"empty\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.datagrid.empty"), "html", null, true);
            echo "</div>
  ";
        }
        // line 73
        echo "  ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin_macro"]) ? $context["admin_macro"] : null), "paginator", array(0 => (isset($context["paginator"]) ? $context["paginator"] : null)), "method"), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "admin/announcement/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 73,  219 => 71,  216 => 70,  214 => 69,  210 => 67,  207 => 66,  186 => 59,  182 => 58,  173 => 52,  169 => 51,  163 => 48,  160 => 47,  154 => 45,  148 => 43,  146 => 42,  141 => 41,  139 => 40,  134 => 38,  129 => 37,  127 => 36,  120 => 35,  118 => 34,  113 => 33,  95 => 32,  93 => 31,  86 => 27,  82 => 26,  78 => 25,  74 => 24,  69 => 23,  67 => 22,  62 => 21,  58 => 19,  56 => 18,  50 => 14,  43 => 11,  41 => 10,  38 => 9,  36 => 8,  33 => 7,  29 => 1,  27 => 5,  25 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/announcement/index.html.twig", "/var/www/edusoho/app/Resources/views/admin/announcement/index.html.twig");
    }
}
