<?php

/* admin/system/payment.html.twig */
class __TwigTemplate_c4a858a621dee2ee8dfb0d7c80751ae64581aea3db8d5ea7dfd1037729408ad1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/system/payment.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["script_controller"] = "system/payment";
        // line 5
        $context["menu"] = "admin_payment";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        // line 8
        echo "
";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["web_macro"]) ? $context["web_macro"] : null), "flash_messages", array(), "method"), "html", null, true);
        echo "

<form class=\"form-horizontal\" method=\"post\" id=\"payment-form\" novalidate >
  <div class=\"form-group\">
    <div class=\"col-md-3 control-label\">
      <label >";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.payment_setting.payment"), "html", null, true);
        echo "</label>
    </div>
    <div class=\"controls col-md-8 radios\">
      ";
        // line 17
        echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->radios("enabled", array(1 => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.radio_btn.open"), 0 => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.radio_btn.close")), $this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "enabled", array()));
        echo "

      <div class=\"text-muted payment-close ";
        // line 19
        if ($this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "enabled", array())) {
            echo " hidden ";
        }
        echo "\">
          ";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.payment_setting.close_help.tips"), "html", null, true);
        echo "
      </div>
      <div class=\"text-muted payment-open ";
        // line 22
        if ( !$this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "enabled", array())) {
            echo " hidden ";
        }
        echo "\">
          ";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.payment_setting.open_help.tips"), "html", null, true);
        echo "
      </div>
    </div>
  </div>


    <div class=\"form-group payment-close ";
        // line 29
        if ($this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "enabled", array())) {
            echo " hidden ";
        }
        echo "\">
      <div class=\"col-md-3 control-label\">
        <label for=\"disabled_message\">";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.payment_setting.pay_disabled_tips"), "html", null, true);
        echo "</label>
      </div>
      <div class=\"controls col-md-8\">
        <textarea id=\"disabled_message\" name=\"disabled_message\" class=\"form-control\">";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "disabled_message", array()), "html", null, true);
        echo "</textarea>
      </div>
    </div>

    <div class=\"payment-open ";
        // line 38
        if ( !$this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "enabled", array())) {
            echo " hidden ";
        }
        echo "\">
      ";
        // line 39
        $this->loadTemplate("admin/system/pay/alipay.html.twig", "admin/system/payment.html.twig", 39)->display($context);
        // line 40
        echo "      ";
        $this->loadTemplate("admin/system/pay/wxpay.html.twig", "admin/system/payment.html.twig", 40)->display($context);
        // line 41
        echo "      ";
        // line 43
        echo "      ";
        $this->loadTemplate("admin/system/pay/llpay.html.twig", "admin/system/payment.html.twig", 43)->display($context);
        // line 44
        echo "    </div>

  <div class=\"form-group\">
    <div class=\"col-md-3 control-label\"></div>
    <div class=\"controls col-md-8\">
      <button type=\"submit\" id=\"payment-btn\" class=\"btn btn-primary\">";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.submit"), "html", null, true);
        echo "</button>
    </div>
    
  </div>
  <div class=\"form-group submit-error hidden\"> 
    <div class=\"col-md-3 control-label\"></div>
    <div class=\"controls col-md-8\">
       <div class=\"help-block\"><span class=\"text-danger\"> ";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.payment_setting.submit_error.tips"), "html", null, true);
        echo "</span></div>
    </div>
  </div>
  
  <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderCsrfToken("site"), "html", null, true);
        echo "\">
</form>
";
    }

    public function getTemplateName()
    {
        return "admin/system/payment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 60,  137 => 56,  127 => 49,  120 => 44,  117 => 43,  115 => 41,  112 => 40,  110 => 39,  104 => 38,  97 => 34,  91 => 31,  84 => 29,  75 => 23,  69 => 22,  64 => 20,  58 => 19,  53 => 17,  47 => 14,  39 => 9,  36 => 8,  33 => 7,  29 => 1,  27 => 5,  25 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/system/payment.html.twig", "/var/www/edusoho/app/Resources/views/admin/system/payment.html.twig");
    }
}
