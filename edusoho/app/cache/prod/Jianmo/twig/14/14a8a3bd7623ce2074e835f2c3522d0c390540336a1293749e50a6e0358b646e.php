<?php

/* css_loader.html.twig */
class __TwigTemplate_c5c463d7f2c444cdbe699cdb90a02e0d841ecce8a3c9acacbda72796067759b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('Codeages\PluginBundle\Twig\HtmlExtension')->css());
        foreach ($context['_seq'] as $context["_key"] => $context["path"]) {
            // line 2
            echo "  ";
            if ((is_string($__internal_513d30f82e6a64f9631cc7238a6b6f93c08aedb6fc7fdb314d08d987e2b7d179 = $context["path"]) && is_string($__internal_423bd2d426da2c8503b9f836b524a133f01ac19f0810900e8f9d62ac7f8ce8f8 = "http://") && ('' === $__internal_423bd2d426da2c8503b9f836b524a133f01ac19f0810900e8f9d62ac7f8ce8f8 || 0 === strpos($__internal_513d30f82e6a64f9631cc7238a6b6f93c08aedb6fc7fdb314d08d987e2b7d179, $__internal_423bd2d426da2c8503b9f836b524a133f01ac19f0810900e8f9d62ac7f8ce8f8)))) {
                // line 3
                echo "    <link href=\"";
                echo twig_escape_filter($this->env, $context["path"], "html", null, true);
                echo "\" rel=\"stylesheet\" />
  ";
            } else {
                // line 5
                echo "    <link href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("static-dist/" . $context["path"])), "html", null, true);
                echo "\" rel=\"stylesheet\" />
  ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['path'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "css_loader.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 5,  26 => 3,  23 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "css_loader.html.twig", "/var/www/edusoho/app/Resources/views/css_loader.html.twig");
    }
}
