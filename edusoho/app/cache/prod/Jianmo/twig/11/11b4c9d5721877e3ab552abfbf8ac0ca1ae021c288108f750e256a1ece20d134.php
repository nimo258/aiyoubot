<?php

/* admin/system/log/logs.html.twig */
class __TwigTemplate_35fb49ff5eaac12cc57e176aa671b0c45f1c09577d2221d8fcc58d6ef8439941 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/system/log/logs.html.twig", 2);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["dict_macro"] = $this->loadTemplate("common/data-dict-macro.html.twig", "admin/system/log/logs.html.twig", 1);
        // line 4
        $context["menu"] = "admin_logs_query";
        // line 6
        $context["script_controller"] = "log/list";
        // line 2
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_main($context, array $blocks = array())
    {
        // line 9
        echo "
";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["web_macro"]) ? $context["web_macro"] : null), "flash_messages", array(), "method"), "html", null, true);
        echo "

<form class=\"well well-sm form-inline\" id=\"search-form\">

  <div class=\"form-group\">
    <select class=\"form-control\" name=\"level\">
        ";
        // line 16
        echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->selectOptions($this->env->getExtension('Codeages\PluginBundle\Twig\DictExtension')->getDict("logLevel"), $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "query", array()), "get", array(0 => "level"), "method"), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.system_log.all_log_level_option"));
        echo "
    </select>
  </div>

  <div class=\"form-group\">
    <input type=\"text\" id=\"startDateTime\" value=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "query", array()), "get", array(0 => "startDateTime"), "method"), "html", null, true);
        echo "\" name=\"startDateTime\" class=\"form-control\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.system_log.start_time_placeholder"), "html", null, true);
        echo "\" style=\"width:150px;\">
  </div>

  <div class=\"form-group\">
    <input type=\"text\" id=\"endDateTime\" value=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "query", array()), "get", array(0 => "endDateTime"), "method"), "html", null, true);
        echo "\" name=\"endDateTime\" class=\"form-control\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.system_log.end_time_placeholder"), "html", null, true);
        echo "\" style=\"width:150px;\">
  </div>

  <div class=\"form-group\">
    <input type=\"hidden\" id=\"hasSystemOperation\" value=\"";
        // line 29
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array(), "any", false, true), "query", array(), "any", false, true), "get", array(0 => "hasSystemOperation"), "method", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array(), "any", false, true), "query", array(), "any", false, true), "get", array(0 => "hasSystemOperation"), "method"), 0)) : (0)), "html", null, true);
        echo "\" name=\"hasSystemOperation\" class=\"form-control\" >
  </div>

  <div class=\"form-group\">
    <select class=\"form-control\" name=\"module\" id=\"log-module\">
        <option value=\"\">";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.system_log.all_module_option"), "html", null, true);
        echo "</option>
        ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["modules"]) ? $context["modules"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
            // line 36
            echo "          <option value=\"";
            echo twig_escape_filter($this->env, $context["module"], "html", null, true);
            echo "\" ";
            if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "query", array()), "get", array(0 => "module"), "method") == $context["module"])) {
                echo " selected=\"";
                echo twig_escape_filter($this->env, $context["module"], "html", null, true);
                echo "\" ";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(("log.module." . $context["module"])), "html", null, true);
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "    </select>
  </div>

  <div class=\"form-group\">
    <select class=\"form-control\" name=\"action\" id=\"log-action\" data-url=\"";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_logs_action_dicts");
        echo "\">
       ";
        // line 43
        if (((array_key_exists("actions", $context)) ? (_twig_default_filter((isset($context["actions"]) ? $context["actions"] : null), null)) : (null))) {
            // line 44
            echo "          <option value=\"\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.system_log.all_operation_option"), "html", null, true);
            echo "</option>
          ";
            // line 45
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["actions"]) ? $context["actions"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["action"]) {
                // line 46
                echo "            <option value=\"";
                echo twig_escape_filter($this->env, $context["action"], "html", null, true);
                echo "\" ";
                if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "query", array()), "get", array(0 => "action"), "method") == $context["action"])) {
                    echo "selected=\"";
                    echo twig_escape_filter($this->env, $context["action"], "html", null, true);
                    echo "\" ";
                }
                echo ">";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(((("log.action." . $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "query", array()), "get", array(0 => "module"), "method")) . ".") . $context["action"])), "html", null, true);
                echo "</option>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['action'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "        ";
        } else {
            // line 49
            echo "          <option value=\"\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.system_log.all_operation_option"), "html", null, true);
            echo "</option>
      ";
        }
        // line 51
        echo "    </select>
  ";
        // line 53
        echo "  </div>

  <div class=\"form-group\">
    ";
        // line 56
        $context["url"] = ($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_logs_username_match") . "?nickname={{query}}");
        // line 57
        echo "    <input class=\"form-control quicksearch width-input-medium-important\" id=\"nickname\" name=\"nickname\" data-role=\"item-input\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.system_log.username_placeholder"), "html", null, true);
        echo "\" data-url='";
        echo twig_escape_filter($this->env, (isset($context["url"]) ? $context["url"] : null), "html", null, true);
        echo "' value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "query", array()), "get", array(0 => "nickname"), "method"), "html", null, true);
        echo "\">
  </div>

  <button class=\"btn btn-primary\">";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.search"), "html", null, true);
        echo "</button>

  <div class=\"text-muted admin-common-switch mtl\">

    ";
        // line 65
        echo "    ";
        // line 66
        echo "      ";
        // line 67
        echo "    ";
        // line 68
        echo "    ";
        // line 69
        echo "      ";
        // line 70
        echo "    ";
        // line 71
        echo "
    <label class=\"mrl\">
      <a class=\"mts pull-left\" href='";
        // line 73
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_logs_old");
        echo "' target=\"_blank\" >";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("log.show.old.logs"), "html", null, true);
        echo "</a>
      ";
        // line 74
        $this->loadTemplate("admin/widget/tooltip-widget.html.twig", "admin/system/log/logs.html.twig", 74)->display(array_merge($context, array("icon" => "glyphicon-question-sign", "content" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("log.show.old.logs.tips"), "placement" => "right")));
        // line 75
        echo "    </label>
  </div>

</form>

  <p>
    <span class=\"mrl\">
      ";
        // line 82
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.system_log.log_num", array("%itemCount%" => $this->getAttribute((isset($context["paginator"]) ? $context["paginator"] : null), "itemCount", array())));
        echo "
    </span>
  </p>

  <table class=\"table table-hover\" id=\"log-table\">
    <tr>
      <th width=\"10%\">";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.system_log.log_lenvel"), "html", null, true);
        echo "</th>
      <th width=\"10%\">";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.system_log.username"), "html", null, true);
        echo "</th>
      <th width=\"39%\">";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.system_log.operation"), "html", null, true);
        echo "</th>
      <th width=\"10%\">";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.system_log.module"), "html", null, true);
        echo "</th>
      <th width=\"16%\">";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.system_log.time"), "html", null, true);
        echo "</th>
      <th width=\"15%\">";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.system_log.ip"), "html", null, true);
        echo "</th>
    </tr>
    ";
        // line 95
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["logs"]) ? $context["logs"] : null));
        $context['_iterated'] = false;
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["log"]) {
            // line 96
            echo "      ";
            $context["user"] = (($this->getAttribute((isset($context["users"]) ? $context["users"] : null), $this->getAttribute($context["log"], "userId", array()), array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["users"]) ? $context["users"] : null), $this->getAttribute($context["log"], "userId", array()), array(), "array"), null)) : (null));
            // line 97
            echo "      <tr>
        <td class=\"vertical-middle\">";
            // line 98
            echo $context["dict_macro"]->getlogLevel($this->getAttribute($context["log"], "level", array()));
            echo "</td>
        <td class=\"vertical-middle\">
          ";
            // line 100
            if (((isset($context["user"]) ? $context["user"] : null) && ($this->getAttribute($context["log"], "module", array()) != "crontab"))) {
                // line 101
                echo "            ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin_macro"]) ? $context["admin_macro"] : null), "user_link", array(0 => (isset($context["user"]) ? $context["user"] : null)), "method"), "html", null, true);
                echo "
          ";
            } else {
                // line 103
                echo "            --
          ";
            }
            // line 105
            echo "        <td class=\"vertical-middle\">
          <div style=\"word-break: break-all;word-wrap: break-word;\">

            ";
            // line 108
            $context["module"] = $this->getAttribute($context["log"], "module", array());
            // line 109
            echo "              ";
            $this->loadTemplate("admin/system/log/template.html.twig", "admin/system/log/logs.html.twig", 109)->display($context);
            // line 110
            echo "          </div>
        </td>
        <td class=\"vertical-middle\">
            ";
            // line 113
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(("log.module." . $this->getAttribute($context["log"], "module", array()))), "html", null, true);
            echo "
        </td>
        <td class=\"vertical-middle\">
          <span class=\"\">";
            // line 116
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["log"], "createdTime", array()), "Y-m-d H:i:s"), "html", null, true);
            echo "</span>
        </td>
        <td class=\"vertical-middle\">
          <a  class=\"text-muted text-sm\" href=\"http://www.baidu.com/s?wd=";
            // line 119
            echo twig_escape_filter($this->env, $this->getAttribute($context["log"], "ip", array()), "html", null, true);
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('AppBundle\Twig\WebExtension')->getConvertIP($this->getAttribute($context["log"], "ip", array())), "html", null, true);
            echo "</a>
          <br>
          ";
            // line 121
            echo twig_escape_filter($this->env, $this->getAttribute($context["log"], "ip", array()), "html", null, true);
            echo "
        </td>
      </tr>
    ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 125
            echo "      <tr><td class=\"empty\" colspan=\"20\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.system_log.empty"), "html", null, true);
            echo "</tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['log'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 127
        echo "  </table>

  ";
        // line 129
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin_macro"]) ? $context["admin_macro"] : null), "paginator", array(0 => (isset($context["paginator"]) ? $context["paginator"] : null)), "method"), "html", null, true);
        echo "


";
    }

    public function getTemplateName()
    {
        return "admin/system/log/logs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  356 => 129,  352 => 127,  343 => 125,  326 => 121,  319 => 119,  313 => 116,  307 => 113,  302 => 110,  299 => 109,  297 => 108,  292 => 105,  288 => 103,  282 => 101,  280 => 100,  275 => 98,  272 => 97,  269 => 96,  251 => 95,  246 => 93,  242 => 92,  238 => 91,  234 => 90,  230 => 89,  226 => 88,  217 => 82,  208 => 75,  206 => 74,  200 => 73,  196 => 71,  194 => 70,  192 => 69,  190 => 68,  188 => 67,  186 => 66,  184 => 65,  177 => 60,  166 => 57,  164 => 56,  159 => 53,  156 => 51,  150 => 49,  147 => 48,  130 => 46,  126 => 45,  121 => 44,  119 => 43,  115 => 42,  109 => 38,  92 => 36,  88 => 35,  84 => 34,  76 => 29,  67 => 25,  58 => 21,  50 => 16,  41 => 10,  38 => 9,  35 => 8,  31 => 2,  29 => 6,  27 => 4,  25 => 1,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/system/log/logs.html.twig", "/var/www/edusoho/app/Resources/views/admin/system/log/logs.html.twig");
    }
}
