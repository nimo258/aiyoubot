<?php

/* admin/article-category/modal.html.twig */
class __TwigTemplate_d476c4786264311d1e7e8f87d92481905b1ef89bf6030d9a41848fabb80cfff0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("old-bootstrap-modal-layout.html.twig", "admin/article-category/modal.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "old-bootstrap-modal-layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        $context["modal_class"] = "modal-lg";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        if ($this->getAttribute((isset($context["category"]) ? $context["category"] : null), "id", array())) {
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.category.edit"), "html", null, true);
        } else {
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.category.create"), "html", null, true);
        }
    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        // line 8
        echo "\t<form id=\"category-form\" class=\"form-horizontal\" action=\"";
        if ($this->getAttribute((isset($context["category"]) ? $context["category"] : null), "id", array())) {
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_article_category_edit", array("id" => $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "id", array()))), "html", null, true);
        } else {
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_article_category_create");
        }
        echo "\" method=\"post\">
\t\t<div class=\"form-group\">
\t\t\t<label class=\"col-md-2 control-label\" for=\"category-name-field\">";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.category.name"), "html", null, true);
        echo "</label>
\t\t\t<div class=\"col-md-8 controls\">
\t\t\t\t<input class=\"form-control\" id=\"category-name-field\" type=\"text\" name=\"name\" value=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "name", array()), "html", null, true);
        echo "\" tabindex=\"1\">
\t\t\t</div>
\t\t</div>

\t\t<div class=\"form-group\">
\t\t\t<label class=\"col-md-2 control-label\" for=\"category-code-field\">";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.category.code"), "html", null, true);
        echo "</label>
\t\t\t<div class=\"col-md-8 controls\">
\t\t\t\t<input class=\"form-control\" id=\"category-code-field\" type=\"text\" name=\"code\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "code", array()), "html", null, true);
        echo "\" data-url=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_article_category_checkcode", array("exclude" => $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "code", array()))), "html", null, true);
        echo "\" tabindex=\"2\">
\t\t\t\t<div class=\"help-block\">";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.category.code.tips"), "html", null, true);
        echo "</div>
\t\t\t</div>
\t\t</div>

\t\t<div class=\"form-group\">
\t\t\t<label class=\"col-md-2 control-label\" for=\"category-parentId-field\">";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.category.parent_category"), "html", null, true);
        echo "</label>
\t\t\t<div class=\"col-md-8 controls\">
\t\t\t\t<select class=\"form-control category-select\" id=\"category-parentId-field\" type=\"text\" name=\"parentId\" tabindex=\"2\" data-url=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_article_category_checkparentid", array("currentId" => $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "id", array()))), "html", null, true);
        echo "\">
\t\t\t\t\t<option value=\"0\">--";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.category.choose"), "html", null, true);
        echo "--</option>
\t\t\t\t\t";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categoryTree"]) ? $context["categoryTree"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["tree"]) {
            // line 30
            echo "\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["tree"], "id", array()), "html", null, true);
            echo "\" ";
            if (($this->getAttribute($context["tree"], "id", array()) == $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "parentId", array()))) {
                echo "selected";
            }
            echo ">";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, ($this->getAttribute($context["tree"], "depth", array()) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                if (($this->getAttribute($context["tree"], "depth", array()) > 1)) {
                    echo "　";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo twig_escape_filter($this->env, $this->getAttribute($context["tree"], "name", array()), "html", null, true);
            echo "</option>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tree'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "\t\t\t\t</select>
\t\t\t</div>
\t\t</div>

\t\t<div style=\"display:none;\" class=\"form-group\">
\t\t\t<label class=\"col-md-2 control-label\" for=\"category-publishArticle-field\">";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.category.allow_publish"), "html", null, true);
        echo "</label>
\t\t\t<div class=\"controls radios col-md-8\"><label>
\t\t\t\t\t<input  id=\"category-publishArticle-field\" type=\"radio\" name=\"publishArticle\" tabindex=\"0\" value=1 ";
        // line 39
        if (($this->getAttribute((isset($context["category"]) ? $context["category"] : null), "publishArticle", array()) == 1)) {
            echo "checked";
        }
        echo ">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.datagrid.radios.yes"), "html", null, true);
        echo "</label>
\t\t\t\t<label>
\t\t\t\t\t<input  id=\"category-publishArticle-field\" type=\"radio\" name=\"publishArticle\" tabindex=\"0\" value=0 ";
        // line 41
        if (($this->getAttribute((isset($context["category"]) ? $context["category"] : null), "publishArticle", array()) != 1)) {
            echo "checked";
        }
        echo ">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.datagrid.radios.no"), "html", null, true);
        echo "
\t\t\t\t</label>

\t\t\t</div>
\t\t</div>

\t\t<div class=\"form-group\">
\t\t\t<label class=\"col-md-2 control-label\" for=\"category-seoTitle-field\">";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.category.seo_title"), "html", null, true);
        echo "</label>
\t\t\t<div class=\"col-md-8 controls\">
\t\t\t\t<input class=\"form-control\" id=\"category-seoTitle-field\" type=\"text\" name=\"seoTitle\" value=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "seoTitle", array()), "html", null, true);
        echo "\" tabindex=\"4\">
\t\t\t\t<div class=\"help-block\"></div>
\t\t\t</div>
\t\t</div>

\t\t<div class=\"form-group\">
\t\t\t<label class=\"col-md-2 control-label\" for=\"category-seoKeyword-field\">";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.category.seo_keywords"), "html", null, true);
        echo "</label>
\t\t\t<div class=\"col-md-8 controls\">
\t\t\t\t<input class=\"form-control\" id=\"category-seoKeyword-field\" type=\"text\" name=\"seoKeyword\" value=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "seoKeyword", array()), "html", null, true);
        echo "\" tabindex=\"4\">
\t\t\t\t<div class=\"help-block\"></div>
\t\t\t</div>
\t\t</div>

\t\t<div class=\"form-group\">
\t\t\t<label class=\"col-md-2 control-label\" for=\"category-seoDesc-field\">";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.category.seo_description"), "html", null, true);
        echo "</label>
\t\t\t<div class=\"col-md-8 controls\">
\t\t\t\t<input class=\"form-control\" id=\"category-seoDesc-field\" type=\"text\" name=\"seoDesc\" value=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "seoDesc", array()), "html", null, true);
        echo "\" tabindex=\"4\">
\t\t\t\t<div class=\"help-block\"></div>
\t\t\t</div>
\t\t</div>

\t\t<div class=\"form-group\">
\t\t\t<label class=\"col-md-2 control-label\" for=\"category-published-field\">";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.category.publish"), "html", null, true);
        echo "</label>
\t\t\t<div class=\"controls radios col-md-8\">
\t\t\t\t<label><input id=\"category-published-field\" type=\"radio\" name=\"published\" tabindex=\"0\" value=1 ";
        // line 74
        if (($this->getAttribute((isset($context["category"]) ? $context["category"] : null), "published", array()) == 1)) {
            echo "checked";
        }
        echo ">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.datagrid.radios.yes"), "html", null, true);
        echo "</label>
\t\t\t\t<label><input id=\"category-published-field\" type=\"radio\" name=\"published\" tabindex=\"0\" value=0 ";
        // line 75
        if (($this->getAttribute((isset($context["category"]) ? $context["category"] : null), "published", array()) != 1)) {
            echo "checked";
        }
        echo ">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.datagrid.radios.no"), "html", null, true);
        echo "</label>
\t\t\t\t<div class=\"help-block\">";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.category.publish.tips"), "html", null, true);
        echo "</div>
\t\t\t</div>
\t\t</div>
\t\t<input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderCsrfToken("site"), "html", null, true);
        echo "\">
\t</form>

\t<script type=\"text/javascript\"> app.load('article/category/save-modal'); </script>
";
    }

    // line 85
    public function block_footer($context, array $blocks = array())
    {
        // line 86
        echo "\t";
        if ($this->getAttribute((isset($context["category"]) ? $context["category"] : null), "id", array())) {
            // line 87
            echo "\t\t<button type=\"button\" class=\"btn btn-default pull-left delete-category\" data-url=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_article_category_delete", array("id" => $this->getAttribute((isset($context["category"]) ? $context["category"] : null), "id", array()))), "html", null, true);
            echo "\" tabindex=\"5\"><i class=\"glyphicon glyphicon-trash\"></i> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.delete"), "html", null, true);
            echo "</button>
\t";
        } else {
            // line 89
            echo "
\t";
        }
        // line 91
        echo "  \t<button type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\" tabindex=\"6\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.cancel"), "html", null, true);
        echo "</button>
\t<button id=\"category-save-btn\" data-submiting-text=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.submit.submiting"), "html", null, true);
        echo "\" type=\"submit\" class=\"btn btn-primary\" data-toggle=\"form-submit\" data-target=\"#category-form\" tabindex=\"4\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.add"), "html", null, true);
        echo "</button>
";
    }

    public function getTemplateName()
    {
        return "admin/article-category/modal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  266 => 92,  261 => 91,  257 => 89,  249 => 87,  246 => 86,  243 => 85,  234 => 79,  228 => 76,  220 => 75,  212 => 74,  207 => 72,  198 => 66,  193 => 64,  184 => 58,  179 => 56,  170 => 50,  165 => 48,  151 => 41,  142 => 39,  137 => 37,  130 => 32,  105 => 30,  101 => 29,  97 => 28,  93 => 27,  88 => 25,  80 => 20,  74 => 19,  69 => 17,  61 => 12,  56 => 10,  46 => 8,  43 => 7,  33 => 5,  29 => 1,  27 => 4,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/article-category/modal.html.twig", "/var/www/edusoho/app/Resources/views/admin/article-category/modal.html.twig");
    }
}
