<?php

/* admin/block/block-visual-items.html.twig */
class __TwigTemplate_842bad1eaacd602946661952bed591604a99c5c6e9ccaecf7f49be1a107af157 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((($this->getAttribute($this->getAttribute((isset($context["block"]) ? $context["block"] : null), "meta", array(), "any", false, true), "items", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["block"]) ? $context["block"] : null), "meta", array(), "any", false, true), "items", array()), null)) : (null)));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["code"] => $context["item"]) {
            // line 2
            echo "  ";
            if (($this->getAttribute($context["item"], "type", array()) != "poster")) {
                // line 3
                echo "    <section id=\"js-affix-";
                echo twig_escape_filter($this->env, $context["code"], "html", null, true);
                echo "\">
      <div class=\"item-title\">
        <strong>";
                // line 5
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
                echo "</strong>
        <p class=\"text-muted\">";
                // line 6
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "desc", array()), "html", null, true);
                echo "
          ";
                // line 7
                if (($context["code"] == "carousel")) {
                    // line 8
                    echo "            <a href=\"http://www.qiqiuyu.com/course/373/tasks\" target=\"_blank\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit_img_compress_url"), "html", null, true);
                    echo "</a>
          ";
                }
                // line 10
                echo "        </p>
      </div>

      <div class=\"panel-group sortable-list\" id=\"js-accordion-";
                // line 13
                echo twig_escape_filter($this->env, $context["code"], "html", null, true);
                echo "\" data-code=\"";
                echo twig_escape_filter($this->env, $context["code"], "html", null, true);
                echo "\"
           data-prefix=\"data[";
                // line 14
                echo twig_escape_filter($this->env, $context["code"], "html", null, true);
                echo "]\" data-count=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "count", array()), "html", null, true);
                echo "\" data-role=\"collapse\" role=\"tablist\"
           aria-multiselectable=\"true\">
        ";
                // line 16
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((($this->getAttribute($this->getAttribute((isset($context["block"]) ? $context["block"] : null), "data", array(), "any", false, true), $context["code"], array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["block"]) ? $context["block"] : null), "data", array(), "any", false, true), $context["code"], array(), "array"), null)) : (null)));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["dataId"] => $context["data"]) {
                    // line 17
                    echo "          <div class=\"panel panel-default\">
            <div class=\"panel-heading\" role=\"tab\" id=\"";
                    // line 18
                    echo twig_escape_filter($this->env, $context["code"], "html", null, true);
                    echo "-heading-";
                    echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
                    echo "\" data-toggle=\"collapse\"
                 data-parent=\"#js-accordion-";
                    // line 19
                    echo twig_escape_filter($this->env, $context["code"], "html", null, true);
                    echo "\" href=\"#";
                    echo twig_escape_filter($this->env, $context["code"], "html", null, true);
                    echo "-collapse-";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
                    echo "-";
                    echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
                    echo "\"
                 aria-expanded=\"false\" aria-controls=\"";
                    // line 20
                    echo twig_escape_filter($this->env, $context["code"], "html", null, true);
                    echo "-collapse-";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
                    echo "-";
                    echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
                    echo "\">
              <h4 class=\"panel-title clearfix\">
                <i class=\"js-move-seq glyphicon glyphicon-move pull-left mrs\"></i>
                <span class=\"panel-item-title ellipsis\">";
                    // line 23
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
                    echo "</span>
                <span class=\"pull-left\">&nbsp;-&nbsp;</span>
                ";
                    // line 25
                    $this->loadTemplate((("admin/block/label/" . $this->getAttribute($context["item"], "type", array())) . ".html.twig"), "admin/block/block-visual-items.html.twig", 25)->display($context);
                    // line 26
                    echo "                <i class=\"js-expand-icon glyphicon glyphicon-chevron-down pull-right\" title=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.panel_expand"), "html", null, true);
                    echo "\"></i>
                ";
                    // line 27
                    if (($this->getAttribute($context["item"], "count", array()) != 1)) {
                        // line 28
                        echo "                  <i class=\"mrs js-remove-btn glyphicon glyphicon-trash pull-right\" title=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.panel_delete"), "html", null, true);
                        echo "\"></i>
                ";
                    }
                    // line 30
                    echo "              </h4>

            </div>
            <div id=\"";
                    // line 33
                    echo twig_escape_filter($this->env, $context["code"], "html", null, true);
                    echo "-collapse-";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
                    echo "-";
                    echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
                    echo "\" class=\"panel-collapse collapse\" role=\"tabpanel\"
                 aria-labelledby=\"";
                    // line 34
                    echo twig_escape_filter($this->env, $context["code"], "html", null, true);
                    echo "-heading-";
                    echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
                    echo "\">
              <div class=\"panel-body\">
                ";
                    // line 36
                    $this->loadTemplate((("admin/block/tag/" . $this->getAttribute($context["item"], "type", array())) . ".html.twig"), "admin/block/block-visual-items.html.twig", 36)->display($context);
                    // line 37
                    echo "              </div>
            </div>
          </div>

        ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['dataId'], $context['data'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 42
                echo "      </div>
      ";
                // line 43
                if (($this->getAttribute($context["item"], "count", array()) != 1)) {
                    // line 44
                    echo "        <div class=\"btn btn-default text-center js-add-btn\"
             style=\"width:100%;margin-top:-10px;margin-bottom:20px;\">";
                    // line 45
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.add_panel"), "html", null, true);
                    echo "</div>
      ";
                }
                // line 47
                echo "
    </section>
  ";
            } else {
                // line 50
                echo "    <ul class=\"nav nav-pills mvl sortable-list\" id=\"btn-tabs\">
      ";
                // line 51
                $this->loadTemplate((("admin/block/label/" . $this->getAttribute($context["item"], "type", array())) . ".html.twig"), "admin/block/block-visual-items.html.twig", 51)->display($context);
                // line 52
                echo "    </ul>
    <div class=\"well\">
      <div class=\"tab-content\">
        ";
                // line 55
                $this->loadTemplate((("admin/block/tag/" . $this->getAttribute($context["item"], "type", array())) . ".html.twig"), "admin/block/block-visual-items.html.twig", 55)->display($context);
                // line 56
                echo "      </div>
    </div>
  ";
            }
            // line 59
            echo "
";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['code'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "<div class=\"form-group mtl\">
  <div class=\"col-sm-10 col-sm-offset-2\">
    <input type=\"hidden\" name=\"blockId\" value=\"";
        // line 63
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["block"]) ? $context["block"] : null), "blockId", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["block"]) ? $context["block"] : null), "blockId", array()))) : ("")), "html", null, true);
        echo "\">
    <input type=\"hidden\" name=\"blockTemplateId\" value=\"";
        // line 64
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["block"]) ? $context["block"] : null), "blockTemplateId", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["block"]) ? $context["block"] : null), "blockTemplateId", array()))) : ("")), "html", null, true);
        echo "\">
    <input type=\"hidden\" name=\"templateName\" value=\"";
        // line 65
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["block"]) ? $context["block"] : null), "templateName", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["block"]) ? $context["block"] : null), "templateName", array()))) : ("")), "html", null, true);
        echo "\">
    <input type=\"hidden\" name=\"code\" value=\"";
        // line 66
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["block"]) ? $context["block"] : null), "code", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["block"]) ? $context["block"] : null), "code", array()))) : ("")), "html", null, true);
        echo "\">
    <input type=\"hidden\" name=\"mode\" value=\"";
        // line 67
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["block"]) ? $context["block"] : null), "mode", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["block"]) ? $context["block"] : null), "mode", array()))) : ("")), "html", null, true);
        echo "\">
  </div>
</div>
<input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderCsrfToken("site"), "html", null, true);
        echo "\">
<button id=\"block-save-btn\" data-submiting-text=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.submit.submiting"), "html", null, true);
        echo "\" type=\"submit\" class=\"btn btn-primary mrm\"
        data-toggle=\"block-edit-form\" data-target=\"#block-edit-form\">";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.save"), "html", null, true);
        echo "</button>
";
    }

    public function getTemplateName()
    {
        return "admin/block/block-visual-items.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  269 => 72,  265 => 71,  261 => 70,  255 => 67,  251 => 66,  247 => 65,  243 => 64,  239 => 63,  235 => 61,  220 => 59,  215 => 56,  213 => 55,  208 => 52,  206 => 51,  203 => 50,  198 => 47,  193 => 45,  190 => 44,  188 => 43,  185 => 42,  167 => 37,  165 => 36,  158 => 34,  150 => 33,  145 => 30,  139 => 28,  137 => 27,  132 => 26,  130 => 25,  125 => 23,  115 => 20,  105 => 19,  99 => 18,  96 => 17,  79 => 16,  72 => 14,  66 => 13,  61 => 10,  55 => 8,  53 => 7,  49 => 6,  45 => 5,  39 => 3,  36 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/block/block-visual-items.html.twig", "/var/www/edusoho/app/Resources/views/admin/block/block-visual-items.html.twig");
    }
}
