<?php

/* admin/tag-group/index.html.twig */
class __TwigTemplate_553f6ade414ff794ccca720f441093a35cb0a6bcb70a89cd492162189a0cabf8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/tag-group/index.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["menu"] = "admin_course_tag_group_manage";
        // line 4
        $context["script_controller"] = "tag-group/list";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        // line 6
        echo "
<table id=\"tag-group-table\" class=\"table table-striped\">
    <thead>
        <th width=\"10%\">";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.tag_manage.tag_group.id_th"), "html", null, true);
        echo "</th>
        <th width=\"15%\">";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.tag_manage.tag_group.name_th"), "html", null, true);
        echo "</th>
        <th width=\"25%\">";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.tag_manage.tag_group.apply_in_th"), "html", null, true);
        echo "</th>
        <th width=\"20%\">";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.tag_manage.tag_group.tag_count_th"), "html", null, true);
        echo "</th>
        <th width=\"20%\">";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.tag_manage.tag_group.create_time_th"), "html", null, true);
        echo "</th>
        <th width=\"10%\">";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.tag_manage.tag_group.operate_th"), "html", null, true);
        echo "</th>
    </thead>
    
    <tbody>
      ";
        // line 18
        if ((isset($context["tagGroups"]) ? $context["tagGroups"] : null)) {
            // line 19
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["tagGroups"]) ? $context["tagGroups"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["tagGroup"]) {
                // line 20
                echo "          ";
                $this->loadTemplate("admin/tag-group/list-tr.html.twig", "admin/tag-group/index.html.twig", 20)->display($context);
                // line 21
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tagGroup'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "      ";
        } else {
            // line 23
            echo "       <tr><td colspan=\"20\"><div class=\"empty\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.tag_manage.tag_group.empty"), "html", null, true);
            echo "</div></td></tr>
      ";
        }
        // line 25
        echo "
    </tbody>
</table>

";
    }

    public function getTemplateName()
    {
        return "admin/tag-group/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 25,  108 => 23,  105 => 22,  91 => 21,  88 => 20,  70 => 19,  68 => 18,  61 => 14,  57 => 13,  53 => 12,  49 => 11,  45 => 10,  41 => 9,  36 => 6,  33 => 5,  29 => 1,  27 => 4,  25 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/tag-group/index.html.twig", "/var/www/edusoho/app/Resources/views/admin/tag-group/index.html.twig");
    }
}
