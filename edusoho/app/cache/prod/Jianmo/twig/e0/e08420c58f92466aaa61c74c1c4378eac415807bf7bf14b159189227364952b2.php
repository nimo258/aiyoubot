<?php

/* admin/course-question/index.html.twig */
class __TwigTemplate_6ba450e9b08a162ebb7f32034a7b5eac0e4f2c00584604688c850751fd9ffcda extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/course-question/index.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["script_controller"] = "course/questions";
        // line 5
        $context["menu"] = "admin_course_question_manage";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        // line 8
        echo "
";
        // line 9
        $this->loadTemplate("admin/course-question/tab.html.twig", "admin/course-question/index.html.twig", 9)->display($context);
        // line 10
        echo "<br>

  <div class=\"well well-sm mtl\">
    <form class=\"form-inline\">

      <div class=\"form-group\">
        <select class=\"form-control\" name=\"keywordType\">
          ";
        // line 17
        echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->selectOptions(array("title" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.form.keyword_type.title_option"), "content" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.form.keyword_type.content_option"), "courseId" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.form.keyword_type.courseId_option"), "courseTitle" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.form.keyword_type.courseTitle_option")), $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "keywordType"), "method"));
        echo "
        </select>
      </div>

      <div class=\"form-group\">
        <input class=\"form-control\" type=\"text\" placeholder=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.form.input.keyword.placeholer"), "html", null, true);
        echo "\" name=\"keyword\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "keyword"), "method"), "html", null, true);
        echo "\">
      </div>

      <span class=\"divider\"></span>

      <div class=\"form-group\">
        <input class=\"form-control\" type=\"text\" placeholder=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.author_th"), "html", null, true);
        echo "\" name=\"author\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "author"), "method"), "html", null, true);
        echo "\">
      </div>


      <button class=\"btn btn-primary\" type=\"submit\">";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.search"), "html", null, true);
        echo "</button>
    </form>
  </div>

  <div id=\"question-table-container\">
    <table id=\"question-table\" class=\"table table-striped table-hover\">
      <thead>
        <tr>
          <th width=\"5%\"><input type=\"checkbox\" data-role=\"batch-select\"></th>
          <th width=\"50%\">";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_question.question_th"), "html", null, true);
        echo "</th>
          ";
        // line 42
        if (((isset($context["type"]) ? $context["type"] : null) == "unPosted")) {
            // line 43
            echo "            <th width=\"10%\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_question.view_th"), "html", null, true);
            echo "</th>
          ";
        } elseif ((        // line 44
(isset($context["type"]) ? $context["type"] : null) == "all")) {
            // line 45
            echo "            <th width=\"10%\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_question.replay_or_review_th"), "html", null, true);
            echo "</th>
          ";
        }
        // line 47
        echo "          <th width=\"10%\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.author_th"), "html", null, true);
        echo "</th>
          <th width=\"10%\">";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_question.createdTime_th"), "html", null, true);
        echo "</th>
          <th width=\"15%\">";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.operation_th"), "html", null, true);
        echo "</th>
        </tr>
      </thead>
      <tbody class=\"tbody\">
        ";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["questions"]) ? $context["questions"] : null));
        $context['_iterated'] = false;
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["question"]) {
            // line 54
            echo "
          ";
            // line 55
            $context["author"] = (($this->getAttribute((isset($context["users"]) ? $context["users"] : null), $this->getAttribute($context["question"], "userId", array()), array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["users"]) ? $context["users"] : null), $this->getAttribute($context["question"], "userId", array()), array(), "array"), null)) : (null));
            // line 56
            echo "          ";
            $context["courseSet"] = (($this->getAttribute((isset($context["courseSets"]) ? $context["courseSets"] : null), $this->getAttribute($context["question"], "courseSetId", array()), array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["courseSets"]) ? $context["courseSets"] : null), $this->getAttribute($context["question"], "courseSetId", array()), array(), "array"), null)) : (null));
            // line 57
            echo "          ";
            $context["course"] = (($this->getAttribute((isset($context["courses"]) ? $context["courses"] : null), $this->getAttribute($context["question"], "courseId", array()), array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["courses"]) ? $context["courses"] : null), $this->getAttribute($context["question"], "courseId", array()), array(), "array"), null)) : (null));
            // line 58
            echo "          ";
            $context["task"] = (($this->getAttribute((isset($context["tasks"]) ? $context["tasks"] : null), $this->getAttribute($context["question"], "taskId", array()), array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["tasks"]) ? $context["tasks"] : null), $this->getAttribute($context["question"], "taskId", array()), array(), "array"), null)) : (null));
            // line 59
            echo "          ";
            if (($this->getAttribute($context["question"], "title", array()) == "")) {
                // line 60
                echo "            ";
                if (($this->getAttribute($context["question"], "questionType", array()) == "video")) {
                    // line 61
                    echo "              ";
                    $context["questionType"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question_type.video");
                    // line 62
                    echo "            ";
                } elseif (($this->getAttribute($context["question"], "questionType", array()) == "image")) {
                    // line 63
                    echo "              ";
                    $context["questionType"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question_type.image");
                    // line 64
                    echo "            ";
                } elseif (($this->getAttribute($context["question"], "questionType", array()) == "audio")) {
                    // line 65
                    echo "              ";
                    $context["questionType"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question_type.audio");
                    // line 66
                    echo "            ";
                } elseif (($this->getAttribute($context["question"], "questionType", array()) == "content")) {
                    // line 67
                    echo "              ";
                    $context["questionType"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question_type.content");
                    // line 68
                    echo "            ";
                }
                // line 69
                echo "          ";
            }
            // line 70
            echo "          <tr data-role=\"item\">
            <td><input value=\"";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "id", array()), "html", null, true);
            echo "\" type=\"checkbox\" data-role=\"batch-item\"> </td>
            <td>
              <a href=\"";
            // line 73
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_thread_show", array("courseId" => $this->getAttribute($context["question"], "courseId", array()), "threadId" => $this->getAttribute($context["question"], "id", array()))), "html", null, true);
            echo "\" target=\"_blank\"><strong>";
            echo twig_escape_filter($this->env, (($this->getAttribute($context["question"], "title", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["question"], "title", array()), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question.title", array("%questionType%" => ((array_key_exists("questionType", $context)) ? (_twig_default_filter((isset($context["questionType"]) ? $context["questionType"] : null), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question_type.content"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question_type.content"))))))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question.title", array("%questionType%" => ((array_key_exists("questionType", $context)) ? (_twig_default_filter((isset($context["questionType"]) ? $context["questionType"] : null), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question_type.content"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.thread.question_type.content"))))))), "html", null, true);
            echo "</strong></a>

              <div class=\"short-long-text\">
                <div class=\"short-text text-sm text-muted\">";
            // line 76
            echo $this->env->getExtension('AppBundle\Twig\WebExtension')->plainTextFilter($this->getAttribute($context["question"], "content", array()), 60);
            echo " <span class=\"trigger\">(";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.expand_td"), "html", null, true);
            echo ")</span></div>
                <div class=\"long-text\">";
            // line 77
            echo $this->getAttribute($context["question"], "content", array());
            echo " <span class=\"trigger\">(";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.collapse_td"), "html", null, true);
            echo ")</span></div>
              </div>

              <div class=\"text-sm mts\">
                ";
            // line 81
            if ((isset($context["courseSet"]) ? $context["courseSet"] : null)) {
                // line 82
                echo "                  <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_show", array("id" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "id", array()))), "html", null, true);
                echo "\" class=\"text-success\" target=\"_blank\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "title", array()), "html", null, true);
                echo "</a>
                ";
            }
            // line 84
            echo "
                ";
            // line 85
            if ((isset($context["course"]) ? $context["course"] : null)) {
                // line 86
                echo "                  <span class=\"text-muted mhs\">&raquo;</span>
                  <a href=\"";
                // line 87
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_show", array("id" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "id", array()))), "html", null, true);
                echo "\" class=\"text-success\" target=\"_blank\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "title", array()), "html", null, true);
                echo "</a>
                ";
            }
            // line 89
            echo "
                ";
            // line 90
            if ((isset($context["task"]) ? $context["task"] : null)) {
                // line 91
                echo "                  <span class=\"text-muted mhs\">&raquo;</span>
                  <a class=\"text-success\"  href=\"";
                // line 92
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_task_show", array("courseId" => $this->getAttribute($context["question"], "courseId", array()), "id" => $this->getAttribute((isset($context["task"]) ? $context["task"] : null), "id", array()))), "html", null, true);
                echo "\" target=\"_blank\">";
                echo twig_escape_filter($this->env, _twig_default_filter($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("course.task_name"), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_course.task")), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["task"]) ? $context["task"] : null), "number", array()), "html", null, true);
                echo "：";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["task"]) ? $context["task"] : null), "title", array()), "html", null, true);
                echo "</a>
                ";
            }
            // line 94
            echo "              </div>
            </td>
            <td>
            <span class=\"text-sm\">
              ";
            // line 98
            if (((isset($context["type"]) ? $context["type"] : null) == "unPosted")) {
                // line 99
                echo "               ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "hitNum", array()), "html", null, true);
                echo "
               ";
            } elseif ((            // line 100
(isset($context["type"]) ? $context["type"] : null) == "all")) {
                // line 101
                echo "              ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "postNum", array()), "html", null, true);
                echo " / ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "hitNum", array()), "html", null, true);
                echo "
                ";
            }
            // line 103
            echo "            </span>
            </td>
            <td>
              ";
            // line 106
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin_macro"]) ? $context["admin_macro"] : null), "user_link", array(0 => (isset($context["author"]) ? $context["author"] : null)), "method"), "html", null, true);
            echo " <br />
            </td>
            <td>
              ";
            // line 109
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["question"], "createdTime", array()), "Y-n-d H:i:s"), "html", null, true);
            echo "
            </td>
            <td>
            ";
            // line 112
            $this->loadTemplate("admin/course-question/td-operations.html.twig", "admin/course-question/index.html.twig", 112)->display($context);
            // line 113
            echo "            </td>
          </tr>
        ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 116
            echo "          <tr><td colspan=\"20\"><div class=\"empty\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.datagrid.empty"), "html", null, true);
            echo "</div></td></tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['question'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 118
        echo "      </tbody>
    </table>

    <div class=\"mbm\">
        <label class=\"checkbox-inline\"><input type=\"checkbox\" data-role=\"batch-select\"> ";
        // line 122
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_thread.select_all_btn"), "html", null, true);
        echo "</label>
        <button class=\"btn btn-default btn-sm mlm\" data-role=\"batch-delete\" data-name=\"";
        // line 123
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_question.question_th"), "html", null, true);
        echo "\" data-url=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_thread_batch_delete");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.delete"), "html", null, true);
        echo "</button>
    </div>
  </div>

  ";
        // line 127
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin_macro"]) ? $context["admin_macro"] : null), "paginator", array(0 => (isset($context["paginator"]) ? $context["paginator"] : null)), "method"), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "admin/course-question/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  351 => 127,  340 => 123,  336 => 122,  330 => 118,  321 => 116,  306 => 113,  304 => 112,  298 => 109,  292 => 106,  287 => 103,  279 => 101,  277 => 100,  272 => 99,  270 => 98,  264 => 94,  254 => 92,  251 => 91,  249 => 90,  246 => 89,  239 => 87,  236 => 86,  234 => 85,  231 => 84,  223 => 82,  221 => 81,  212 => 77,  206 => 76,  198 => 73,  193 => 71,  190 => 70,  187 => 69,  184 => 68,  181 => 67,  178 => 66,  175 => 65,  172 => 64,  169 => 63,  166 => 62,  163 => 61,  160 => 60,  157 => 59,  154 => 58,  151 => 57,  148 => 56,  146 => 55,  143 => 54,  125 => 53,  118 => 49,  114 => 48,  109 => 47,  103 => 45,  101 => 44,  96 => 43,  94 => 42,  90 => 41,  78 => 32,  69 => 28,  58 => 22,  50 => 17,  41 => 10,  39 => 9,  36 => 8,  33 => 7,  29 => 1,  27 => 5,  25 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/course-question/index.html.twig", "/var/www/edusoho/app/Resources/views/admin/course-question/index.html.twig");
    }
}
