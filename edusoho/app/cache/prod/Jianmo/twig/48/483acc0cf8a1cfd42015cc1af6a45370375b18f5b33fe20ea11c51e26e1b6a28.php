<?php

/* admin/system/wechat-setting.html.twig */
class __TwigTemplate_1e2313adab9f004955d5c1441523cb9364ccde3347d8696210f81fe3fc025432 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/system/wechat-setting.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["script_controller"] = "system/wechat-setting";
        // line 5
        $context["menu"] = "admin_setting_wechat_auth";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        // line 8
        echo "  ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["web_macro"]) ? $context["web_macro"] : null), "flash_messages", array(), "method"), "html", null, true);
        echo "
  ";
        // line 9
        if ( !(isset($context["isCloudOpen"]) ? $context["isCloudOpen"] : null)) {
            // line 10
            echo "    <div class=\"alert alert-danger\">
      ";
            // line 11
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("wechat.notification.cloud.tips"), "html", null, true);
            echo "
      <a href=\"";
            // line 12
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_my_cloud_overview");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("wechat.notification.cloud_open"), "html", null, true);
            echo "</a>
    </div>
  ";
        }
        // line 15
        echo "  <form id=\"wechat-setting-form\" class=\"form-horizontal\" method=\"post\" novalidate=\"novalidate\">
    <div class=\"wechat-setting-container\">
      <div class=\"wechat-setting-field\">
        <div class=\"setting-label\">
          <label class=\"setting-title\">";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.web_scan_login"), "html", null, true);
        echo "</label>
          <label class=\"form-switch setting-switch ";
        // line 20
        if ($this->getAttribute((isset($context["loginConnect"]) ? $context["loginConnect"] : null), "weixinweb_enabled", array())) {
            echo "checked";
        }
        echo "\">
            <input type=\"checkbox\" name=\"loginConnect[weixinweb_enabled]\" value=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loginConnect"]) ? $context["loginConnect"] : null), "weixinweb_enabled", array()), "html", null, true);
        echo "\" id=\"weixinweb_enabled\" data-toggle=\"switch\">
            <input type=\"hidden\" name=\"loginConnect[weixinweb_enabled]\" value=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loginConnect"]) ? $context["loginConnect"] : null), "weixinweb_enabled", array()), "html", null, true);
        echo "\">
          </label>
        </div>
        <div class=\"setting-content\">
          <div class=\"setting-content-block\">
            <div class=\"block-scan-login\">";
        // line 27
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.web_scan_login_hint");
        echo "</div>
          </div>
          <div class=\"setting-content-button\">
            <a href=\"http://www.qiqiuyu.com/faq/549/detail\" type=\"button\" class=\"btn btn-default btn-large btn-primary\" target=\"blank\">";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.tutorial_button"), "html", null, true);
        echo "</a>
          </div>
        </div>
      </div>

      <div class=\"wechat-setting-field\">
        <div class=\"setting-label\">
          <label class=\"setting-title\">";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.wechat_auth_login"), "html", null, true);
        echo "</label>
          <label class=\"form-switch setting-switch ";
        // line 38
        if ($this->getAttribute((isset($context["loginConnect"]) ? $context["loginConnect"] : null), "weixinmob_enabled", array())) {
            echo "checked";
        }
        echo "\">
            <input type=\"checkbox\" name=\"loginConnect[weixinmob_enabled]\" value=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loginConnect"]) ? $context["loginConnect"] : null), "weixinmob_enabled", array()), "html", null, true);
        echo "\" id=\"weixinmob_enabled\" data-toggle=\"switch\">
            <input type=\"hidden\" name=\"loginConnect[weixinmob_enabled]\" value=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loginConnect"]) ? $context["loginConnect"] : null), "weixinmob_enabled", array()), "html", null, true);
        echo "\">
          </label>
        </div>
        <div class=\"setting-content\">
          <div class=\"setting-content-block\">
            <div class=\"block-auth-login\">";
        // line 45
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.wechat_auth_login_hint");
        echo "</div>
          </div>
          <div class=\"setting-content-button\">
            <a href=\"http://www.qiqiuyu.com/faq/549/detail\" type=\"button\" class=\"btn btn-default btn-large btn-primary\" target=\"blank\">";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.tutorial_button"), "html", null, true);
        echo "</a>
          </div>
        </div>
      </div>

      <div class=\"wechat-setting-field\">
        <div class=\"setting-label\">
          <label class=\"setting-title\">";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.wechat_notification"), "html", null, true);
        echo "</label>
          <label class=\"form-switch setting-switch ";
        // line 56
        if ($this->getAttribute((isset($context["wechatSetting"]) ? $context["wechatSetting"] : null), "wechat_notification_enabled", array())) {
            echo "checked";
        }
        echo "\">
            <input type=\"checkbox\" name=\"wechatSetting[wechat_notification_enabled]\" value=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wechatSetting"]) ? $context["wechatSetting"] : null), "wechat_notification_enabled", array()), "html", null, true);
        echo "\" id=\"wechat_notification_enabled\" data-toggle=\"switch\" ";
        if ( !(isset($context["isCloudOpen"]) ? $context["isCloudOpen"] : null)) {
            echo "disabled";
        }
        echo ">
            <input type=\"hidden\" name=\"wechatSetting[wechat_notification_enabled]\" value=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wechatSetting"]) ? $context["wechatSetting"] : null), "wechat_notification_enabled", array()), "html", null, true);
        echo "\">
          </label>
        </div>
        <div class=\"setting-content\">
          <div class=\"setting-content-block\">
            <div class=\"block-account-notification\">";
        // line 63
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.wechat_notification_hint");
        echo "</div>
          </div>
          <div class=\"setting-content-button\">
            <a href=\"http://www.qiqiuyu.com/faq/875/detail\" type=\"button\" class=\"btn btn-default btn-large btn-primary\" target=\"blank\">";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.tutorial_button"), "html", null, true);
        echo "</a>
          </div>
        </div>
      </div>

      <div class=\"wechat-setting-field\">
        <div class=\"setting-label\">
          <label class=\"setting-title\">";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.wechat_pay"), "html", null, true);
        echo "</label>
          <label class=\"form-switch setting-switch ";
        // line 74
        if ($this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "wxpay_enabled", array())) {
            echo "checked";
        }
        echo "\">
            <input type=\"checkbox\" name=\"payment[wxpay_enabled]\" value=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "wxpay_enabled", array()), "html", null, true);
        echo "\" id=\"wxpay_enabled\" data-toggle=\"switch\">
            <input type=\"hidden\" name=\"payment[wxpay_enabled]\" value=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "wxpay_enabled", array()), "html", null, true);
        echo "\">
          </label>
        </div>
        <div class=\"setting-content\">
          <div class=\"setting-content-block\">
            <div class=\"block-wechat-pay\">";
        // line 81
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.wechat_pay_hint");
        echo "</div>
          </div>
          <div class=\"setting-content-button\">
            <a href=\"http://www.qiqiuyu.com/faq/259/detail\" type=\"button\" class=\"btn btn-default btn-large btn-primary\" target=\"blank\">";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.tutorial_button"), "html", null, true);
        echo "</a>
          </div>
        </div>
      </div>
    </div>

    <div class=\"wechat-setting-form\">

      <div data-sub=\"weixinweb\" class=\"";
        // line 92
        if ( !$this->getAttribute((isset($context["loginConnect"]) ? $context["loginConnect"] : null), "weixinweb_enabled", array())) {
            echo "hidden";
        }
        echo "\">
        <div class=\"form-group group-field\">
          <div class=\"col-md-4 control-label\">
            <label for=\"weixinweb_key\">";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.web_appid"), "html", null, true);
        echo "</label>
          </div>
          <div class=\"controls col-md-7\">
            <input id=\"weixinweb_key\" name=\"loginConnect[weixinweb_key]\" value=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loginConnect"]) ? $context["loginConnect"] : null), "weixinweb_key", array()), "html", null, true);
        echo "\" class=\"form-control\"></input>
          </div>
        </div>

        <div class=\"form-group group-field\">
          <div class=\"col-md-4 control-label\">
            <label for=\"weixinweb_secret\">";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.web_secret"), "html", null, true);
        echo "</label>
          </div>
          <div class=\"controls col-md-7\">
            <input id=\"weixinweb_secret\" name=\"loginConnect[weixinweb_secret]\" value=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loginConnect"]) ? $context["loginConnect"] : null), "weixinweb_secret", array()), "html", null, true);
        echo "\" class=\"form-control\"></input>
            <div class=\"help-block\">
              <div>";
        // line 109
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.web_secret_hint");
        echo "</div>
            </div>
          </div>
        </div>
      </div>

      <div data-sub=\"weixinmob\" class=\"";
        // line 115
        if (( !$this->getAttribute((isset($context["loginConnect"]) ? $context["loginConnect"] : null), "weixinmob_enabled", array()) &&  !$this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "wxpay_enabled", array()))) {
            echo "hidden";
        }
        echo "\">
        <div class=\"form-group group-field\">
          <div class=\"col-md-4 control-label\">
            <label for=\"weixinmob_key\">";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.account_appid"), "html", null, true);
        echo "</label>
          </div>
          <div class=\"controls col-md-7\">
            <input id=\"weixinmob_key\" name=\"loginConnect[weixinmob_key]\" value=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loginConnect"]) ? $context["loginConnect"] : null), "weixinmob_key", array()), "html", null, true);
        echo "\" class=\"form-control\"></input>
          </div>
        </div>

        <div class=\"form-group group-field\">
          <div class=\"col-md-4 control-label\">
            <label for=\"weixinmob_secret\">";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.account_secret"), "html", null, true);
        echo "</label>
          </div>
          <div class=\"controls col-md-7\">
            <input id=\"weixinmob_secret\" name=\"loginConnect[weixinmob_secret]\" value=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loginConnect"]) ? $context["loginConnect"] : null), "weixinmob_secret", array()), "html", null, true);
        echo "\" class=\"form-control\"></input>
            <div class=\"help-block\">
              <div>";
        // line 132
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.account_secret_hint");
        echo "</div>
            </div>
          </div>
        </div>

        <div class=\"form-group group-field\">
          <div class=\"col-md-4 control-label\">
            <label for=\"wxpay_mp_secret\">";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.mp_token"), "html", null, true);
        echo "</label>
          </div>
          <div class=\"controls col-md-7\">
            <input id=\"wxpay_mp_secret\" name=\"payment[wxpay_mp_secret]\" value=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "wxpay_mp_secret", array()), "html", null, true);
        echo "\" class=\"form-control\"></input>
          </div>
        </div>
      </div>

      <div data-sub=\"account\" class=\"";
        // line 147
        if ( !$this->getAttribute((isset($context["wechatSetting"]) ? $context["wechatSetting"] : null), "wechat_notification_enabled", array())) {
            echo "hidden";
        }
        echo "\">

        <div class=\"form-group group-field\">
          <div class=\"col-md-4 control-label\">
            <label for=\"account_code\">";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.account_code"), "html", null, true);
        echo "</label>
          </div>
          <div class=\"controls col-md-7\">
            <span class=\"es-qrcode top pt5 hidden-xs ";
        // line 154
        if ( !$this->getAttribute((isset($context["wechatSetting"]) ? $context["wechatSetting"] : null), "account_code", array())) {
            echo "hidden";
        }
        echo "\">
              <i class=\"es-icon es-icon-qrcode\"></i>
              <span>
                <img class=\"js-code-img account-code hidden\" src=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wechatSetting"]) ? $context["wechatSetting"] : null), "account_code", array()), "html", null, true);
        echo "\" alt=\"\">
              </span>
              <a class=\"btn btn-default btn-sm js-code-view\">";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.account_code_view"), "html", null, true);
        echo "</a>
            </span>
            <span class=\"code-help-block ";
        // line 161
        if ($this->getAttribute((isset($context["wechatSetting"]) ? $context["wechatSetting"] : null), "account_code", array())) {
            echo "hidden";
        }
        echo "\">";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.account_code_empty");
        echo "</span>
            <a class=\"btn btn-default btn-sm btn-primary\" id=\"qrcode-upload\" data-upload-token=\"";
        // line 162
        echo twig_escape_filter($this->env, $this->env->getExtension('AppBundle\Twig\WebExtension')->makeUpoadToken("system", "image"), "html", null, true);
        echo "\" data-goto-url=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_setting_logo_upload");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.set"), "html", null, true);
        echo "</a>
            <a href=\"http://www.qiqiuyu.com/faq/876/detail\" target=\"blank\">";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.account_code_download"), "html", null, true);
        echo "</a>
            <input type=\"hidden\" id=\"account_code\" name=\"wechatSetting[account_code]\" value=\"";
        // line 164
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["wechatSetting"]) ? $context["wechatSetting"] : null), "account_code", array()), "html", null, true);
        echo "\" class=\"form-control\"></input>
          </div>
        </div>
      </div>

      <div data-sub=\"wxpay\" class=\"";
        // line 169
        if ( !$this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "wxpay_enabled", array())) {
            echo "hidden";
        }
        echo "\">
        <div class=\"form-group group-field\">
          <div class=\"col-md-4 control-label\">
            <label for=\"wxpay_account\">";
        // line 172
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.account_number"), "html", null, true);
        echo "</label>
          </div>
          <div class=\"controls col-md-7\">
            <input id=\"wxpay_account\" name=\"payment[wxpay_account]\" value=\"";
        // line 175
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "wxpay_account", array()), "html", null, true);
        echo "\" class=\"form-control\"></input>
            <div class=\"help-block\">
              <div>";
        // line 177
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.account_number_hint");
        echo "</div>
            </div>
          </div>
        </div>

        <div class=\"form-group group-field\">
          <div class=\"col-md-4 control-label\">
            <label for=\"wxpay_key\">";
        // line 184
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.api_secret_key"), "html", null, true);
        echo "</label>
          </div>
          <div class=\"controls col-md-7\">
            <input id=\"wxpay_key\" name=\"payment[wxpay_key]\" value=\"";
        // line 187
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["payment"]) ? $context["payment"] : null), "wxpay_key", array()), "html", null, true);
        echo "\" class=\"form-control\"></input>
            <div class=\"help-block\">
              <div>";
        // line 189
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting_wechat_auth.api_secret_key_hint");
        echo "</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 195
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderCsrfToken("site"), "html", null, true);
        echo "\">
    <div class=\"setting-submit-field\">
      <button type=\"submit\" class=\"setting-form-submit\">";
        // line 197
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.submit"), "html", null, true);
        echo "</button>
    </div>
  </form>
";
    }

    public function getTemplateName()
    {
        return "admin/system/wechat-setting.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  437 => 197,  432 => 195,  423 => 189,  418 => 187,  412 => 184,  402 => 177,  397 => 175,  391 => 172,  383 => 169,  375 => 164,  371 => 163,  363 => 162,  355 => 161,  350 => 159,  345 => 157,  337 => 154,  331 => 151,  322 => 147,  314 => 142,  308 => 139,  298 => 132,  293 => 130,  287 => 127,  278 => 121,  272 => 118,  264 => 115,  255 => 109,  250 => 107,  244 => 104,  235 => 98,  229 => 95,  221 => 92,  210 => 84,  204 => 81,  196 => 76,  192 => 75,  186 => 74,  182 => 73,  172 => 66,  166 => 63,  158 => 58,  150 => 57,  144 => 56,  140 => 55,  130 => 48,  124 => 45,  116 => 40,  112 => 39,  106 => 38,  102 => 37,  92 => 30,  86 => 27,  78 => 22,  74 => 21,  68 => 20,  64 => 19,  58 => 15,  50 => 12,  46 => 11,  43 => 10,  41 => 9,  36 => 8,  33 => 7,  29 => 1,  27 => 5,  25 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/system/wechat-setting.html.twig", "/var/www/edusoho/app/Resources/views/admin/system/wechat-setting.html.twig");
    }
}
