<?php

/* admin/classroom/index.html.twig */
class __TwigTemplate_c11d03e93ae9defa2467b814fed7c0c976184d05d5000c32bd79242ab6ce1476 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/classroom/index.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["menu"] = "admin_classroom_manage";
        // line 5
        $context["script_controller"] = "topxiaadminbundle/controller/classroom/classroom";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_main($context, array $blocks = array())
    {
        // line 7
        echo "
  <form id=\"message-search-form\" class=\"form-inline well well-sm\" action=\"\" method=\"get\" novalidate>
    ";
        // line 9
        $this->loadTemplate("org/org-tree-select.html.twig", "admin/classroom/index.html.twig", 9)->display(array_merge($context, array("orgCode" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "orgCode"), "method"), "modal" => "list")));
        // line 10
        echo "
    <div class=\"form-group\">
      <input class=\"form-control\" type=\"text\" placeholder=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.classroom_manage.manage.name_placeholder", array("%name%" => _twig_default_filter($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("classroom.name"), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("classroom")))), "html", null, true);
        echo "\" name=\"titleLike\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "titleLike"), "method"), "html", null, true);
        echo "\">
    </div>

    <button class=\"btn btn-primary\">";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.search"), "html", null, true);
        echo "</button>
    ";
        // line 16
        $this->loadTemplate("admin/widget/tooltip-widget.html.twig", "admin/classroom/index.html.twig", 16)->display(array_merge($context, array("icon" => "glyphicon-question-sign", "content" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.classroom_manage.manage.describe_tips"), "placement" => "left")));
        // line 17
        echo "
  </form>

  <p class=\"text-muted\">
    <span class=\"mrl\">";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.classroom_manage.manage.count", array("%courseNum%" => $this->getAttribute((isset($context["classroomStatusNum"]) ? $context["classroomStatusNum"] : null), "total", array())));
        echo "</span>
    <span class=\"mrl\">";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.classroom_manage.manage.published_count", array("%publishedNum%" => $this->getAttribute((isset($context["classroomStatusNum"]) ? $context["classroomStatusNum"] : null), "published", array())));
        echo "</span>
    <span class=\"mrl\">";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.classroom_manage.manage.closed_count", array("%closedNum%" => $this->getAttribute((isset($context["classroomStatusNum"]) ? $context["classroomStatusNum"] : null), "closed", array())));
        echo "</span>
    <span class=\"mrl\">";
        // line 24
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.classroom_manage.manage.unpublish_count", array("%unPublishedNum%" => $this->getAttribute((isset($context["classroomStatusNum"]) ? $context["classroomStatusNum"] : null), "draft", array())));
        echo "</span>
  </p>
  
  ";
        // line 27
        if ((isset($context["classroomInfo"]) ? $context["classroomInfo"] : null)) {
            // line 28
            echo "  <table class=\"table table-striped table-hover 111\" id=\"classroom-table\">
    <thead>
    <tr>
      ";
            // line 31
            if ($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("magic.enable_org", "0")) {
                // line 32
                echo "        <th><input type=\"checkbox\"  data-role=\"batch-select\"></th>
      ";
            }
            // line 34
            echo "      <th>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.classroom_manage.manage.number_th", array("%title%" => _twig_default_filter($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("calssroom.title"), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("classroom")))), "html", null, true);
            echo "</th>
      <th width=\"22%\">";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.classroom_manage.manage.name_th", array("%name%" => _twig_default_filter($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("classroom.name"), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("classroom")))), "html", null, true);
            echo "</th>
      ";
            // line 36
            $this->loadTemplate("org/parts/table-thead-tr.html.twig", "admin/classroom/index.html.twig", 36)->display($context);
            // line 37
            echo "      <th>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.classroom_manage.manage.course_number_th"), "html", null, true);
            echo "</th>
      <th>";
            // line 38
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.classroom_manage.manage.student_number_th"), "html", null, true);
            echo "</th>
      <th width=\"12%\">";
            // line 39
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.classroom_manage.manage.price_th"), "html", null, true);
            echo "</th>
      <th>";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.classroom_manage.manage.status_th"), "html", null, true);
            echo "</th>
      <th>";
            // line 41
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.classroom_manage.manage.operate_th"), "html", null, true);
            echo "</th>
    </tr>
    </thead>
    <tbody>

   ";
            // line 46
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["classroomInfo"]) ? $context["classroomInfo"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["classroom"]) {
                // line 47
                echo "      ";
                $context["category"] = (($this->getAttribute((isset($context["categories"]) ? $context["categories"] : null), $this->getAttribute($context["classroom"], "categoryId", array()), array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["categories"]) ? $context["categories"] : null), $this->getAttribute($context["classroom"], "categoryId", array()), array(), "array"), null)) : (null));
                // line 48
                echo "      ";
                $this->loadTemplate("admin/classroom/table-tr.html.twig", "admin/classroom/index.html.twig", 48)->display(array_merge($context, array("classroom" => $context["classroom"], "category" => (isset($context["category"]) ? $context["category"] : null))));
                // line 49
                echo "
   ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['classroom'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 51
            echo "
    </tbody>

  </table>
  ";
        } else {
            // line 56
            echo "    <div class=\"empty\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.classroom_manage.manage.empty", array("%name%" => _twig_default_filter($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("classroom.name"), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("classroom")))), "html", null, true);
            echo "</div>
  ";
        }
        // line 58
        echo "  ";
        $this->loadTemplate("org/batch-update-org-btn.html.twig", "admin/classroom/index.html.twig", 58)->display(array_merge($context, array("module" => "classroom", "formId" => "classroom-table")));
        // line 59
        echo "  <div class=\"pull-right\">
   ";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin_macro"]) ? $context["admin_macro"] : null), "paginator", array(0 => (isset($context["paginator"]) ? $context["paginator"] : null)), "method"), "html", null, true);
        echo "
  </div>


";
    }

    public function getTemplateName()
    {
        return "admin/classroom/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 60,  187 => 59,  184 => 58,  178 => 56,  171 => 51,  156 => 49,  153 => 48,  150 => 47,  133 => 46,  125 => 41,  121 => 40,  117 => 39,  113 => 38,  108 => 37,  106 => 36,  102 => 35,  97 => 34,  93 => 32,  91 => 31,  86 => 28,  84 => 27,  78 => 24,  74 => 23,  70 => 22,  66 => 21,  60 => 17,  58 => 16,  54 => 15,  46 => 12,  42 => 10,  40 => 9,  36 => 7,  33 => 6,  29 => 1,  27 => 5,  25 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/classroom/index.html.twig", "/var/www/edusoho/app/Resources/views/admin/classroom/index.html.twig");
    }
}
