<?php

/* admin/theme/tab-panel/nav.html.twig */
class __TwigTemplate_2c6d89d069d04a8a6d6876a16bf9abb6d98b5a744761fbc0510d3bc40cb1e4e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->env->getExtension('AppBundle\Twig\WebExtension')->loadScript("topxiaadminbundle/controller/theme/theme-setting/nav.js");
        // line 3
        echo "<div class=\"tab-pane\" id=\"top\">
  <div class=\"panel panel-default\">
      <div class=\"panel-heading\">";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.setting_navigation"), "html", null, true);
        echo "</div>
      <div class=\"panel-body \">
          ";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.navigation_num_tips", array("%value%" => (($this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "topNavNum", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["navigation"]) ? $context["navigation"] : null), "topNavNum", array()), 5)) : (5))));
        echo "
          <div class=\"help color-danger hide mtm\">";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.navigation_num_validation"), "html", null, true);
        echo "</div>
      </div>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "admin/theme/tab-panel/nav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 8,  33 => 7,  28 => 5,  24 => 3,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/theme/tab-panel/nav.html.twig", "/var/www/edusoho/app/Resources/views/admin/theme/tab-panel/nav.html.twig");
    }
}
