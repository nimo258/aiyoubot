<?php

/* admin/block/tag/poster.html.twig */
class __TwigTemplate_a55880c0d8322c3f33ddb2051d5cc3cbbb6525cfc17fd02b73286c26b7e8b84f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["first"] = 1;
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((($this->getAttribute($this->getAttribute((isset($context["block"]) ? $context["block"] : null), "data", array(), "any", false, true), (isset($context["code"]) ? $context["code"] : null), array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["block"]) ? $context["block"] : null), "data", array(), "any", false, true), (isset($context["code"]) ? $context["code"] : null), array(), "array"), null)) : (null)));
        foreach ($context['_seq'] as $context["dataId"] => $context["data"]) {
            // line 3
            echo "  <div role=\"tabpanel\" class=\"tab-pane ";
            if (((isset($context["first"]) ? $context["first"] : null) == 1)) {
                echo "active";
            }
            echo "\" id=\"poster-";
            echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
            echo "\">
    <div class=\"form-group\">
      <label class=\"col-sm-2 control-label\">";
            // line 5
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.poster_open_btn"), "html", null, true);
            echo "</label>
      <label class=\"checkbox-inline\">
        <input class=\"status-input\" type=\"radio\" value=\"1\" ";
            // line 7
            if (($this->getAttribute($context["data"], "status", array()) == 1)) {
                echo "checked";
            }
            echo "> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.poster_open_radio"), "html", null, true);
            echo "
      </label>
      <label class=\"checkbox-inline\">
        <input class=\"status-input\" type=\"radio\" value=\"0\" ";
            // line 10
            if (($this->getAttribute($context["data"], "status", array()) == 0)) {
                echo "checked";
            }
            echo "> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.poster_closed_radio"), "html", null, true);
            echo "
      </label>
      <input class=\"status-value\" type=\"hidden\" name=\"data[";
            // line 12
            echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
            echo "][";
            echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
            echo "][status]\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "status", array()), "html", null, true);
            echo "\">
    </div>
    <div class=\"form-group\">
      <label class=\"col-sm-2 control-label\">";
            // line 15
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.poster_layout_mode"), "html", null, true);
            echo "</label>
      <label class=\"checkbox-inline\">
        <input class=\"layout-input\" type=\"radio\" value=\"limitWide\" ";
            // line 17
            if (($this->getAttribute($context["data"], "layout", array()) == "limitWide")) {
                echo "checked";
            }
            echo "> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.poster_width_center_radio"), "html", null, true);
            echo "
      </label>
      <label class=\"checkbox-inline\">
        <input class=\"layout-input\" type=\"radio\" value=\"tile\" ";
            // line 20
            if (($this->getAttribute($context["data"], "layout", array()) == "tile")) {
                echo "checked";
            }
            echo "> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.poster_tile_center_radio"), "html", null, true);
            echo "
      </label>
      <input class=\"layout-value\" type=\"hidden\" name=\"data[";
            // line 22
            echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
            echo "][";
            echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
            echo "][layout]\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "layout", array()), "html", null, true);
            echo "\">
    </div>
    <div class=\"form-group\">
      <label class=\"col-sm-2 control-label\">";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.poster_edit_mode"), "html", null, true);
            echo "</label>
      <label class=\"checkbox-inline\">
        <input class=\"imgMode\" type=\"radio\" value=\"option1\" ";
            // line 27
            if (($this->getAttribute($context["data"], "mode", array()) == "img")) {
                echo "checked=\"checked\"";
            }
            echo "> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.poster_img_mode_radio"), "html", null, true);
            echo "
      </label>
      <label class=\"checkbox-inline\">
        <input class=\"htmlMode\" type=\"radio\" value=\"option2\" ";
            // line 30
            if (($this->getAttribute($context["data"], "mode", array()) == "html")) {
                echo "checked=\"checked\"";
            }
            echo "> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.poster_html_mode_radio"), "html", null, true);
            echo "
      </label>
      <input type=\"hidden\" name=\"data[";
            // line 32
            echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
            echo "][";
            echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
            echo "][mode]\" class=\"mode-value\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "mode", array()), "html", null, true);
            echo "\">
    </div>
    <div class=\"edit-mode-img\" ";
            // line 34
            if (($this->getAttribute($context["data"], "mode", array()) == "html")) {
                echo "style=\"display: none;\"";
            }
            echo ">
      <div class=\"form-group\">
        <label class=\"col-sm-2 control-label\">
          ";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.poster_select_img"), "html", null, true);
            echo "
        </label>

        <div class=\"col-sm-10\">
          <label class=\"control-label img-mrl\">";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "src", array()), "html", null, true);
            echo "</label>
          <a class=\"btn btn-default btn-sm img-mode-upload\" id=\"img-";
            // line 42
            echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
            echo "-uploadId-";
            echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
            echo "\" data-upload-token=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('AppBundle\Twig\WebExtension')->makeUpoadToken("system", "image"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.poster_img_upload"), "html", null, true);
            echo "</a>
          <p class=\"text-success\">
            ";
            // line 44
            if (($this->getAttribute((isset($context["block"]) ? $context["block"] : null), "code", array()) == "live_top_banner")) {
                // line 45
                echo "              ";
                echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.poster_live_top_banner_tips");
                echo "
              <a href=\"http://www.qiqiuyu.com/course/373/tasks\" target=\"_blank\">";
                // line 46
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit_img_compress_url"), "html", null, true);
                echo "</a>
            ";
            } else {
                // line 48
                echo "              ";
                echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.poster_other_tips");
                echo "
              <a href=\"http://www.qiqiuyu.com/course/373/tasks\" target=\"_blank\">";
                // line 49
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit_img_compress_url"), "html", null, true);
                echo "</a>
            ";
            }
            // line 51
            echo "          </p>
          <input class=\"form-control img-value\" type=\"hidden\" name=\"data[";
            // line 52
            echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
            echo "][";
            echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
            echo "][src]\" id=\"data[";
            echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
            echo "][";
            echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
            echo "][src]\" data-role=\"img-url\" value=\"";
            echo twig_escape_filter($this->env, (($this->getAttribute($context["data"], "src", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["data"], "src", array()), "")) : ("")), "html", null, true);
            echo "\">
          <img class=\"img-responsive mtm img-mtl\" src=\"";
            // line 53
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($context["data"], "src", array())), "html", null, true);
            echo "\" alt=\"\">
        </div>
      </div>
      <div class=\"form-group\">
        <label class=\"col-sm-2 control-label\">
          ";
            // line 58
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.tag_img_background_color"), "html", null, true);
            echo "
        </label>

        <div class=\"col-sm-10\">
          <input type=\"text\" class=\"form-control colorpicker-input width-input-small\" name=\"data[";
            // line 62
            echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
            echo "][";
            echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
            echo "][background]\" id=\"data[";
            echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
            echo "][";
            echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
            echo "][background]\" placeholder=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "background", array()), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "background", array()), "html", null, true);
            echo "\">
        </div>
      </div>
      <div class=\"form-group\">
        <label class=\"col-sm-2 control-label\">
          ";
            // line 67
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.poster_link_url"), "html", null, true);
            echo "
        </label>

        <div class=\"col-sm-10\">
          <input type=\"text\" class=\"form-control width-input-large\" name=\"data[";
            // line 71
            echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
            echo "][";
            echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
            echo "][href]\" id=\"data[";
            echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
            echo "][";
            echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
            echo "][href]\" placeholder=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "getScheme", array(), "method"), "html", null, true);
            echo "://\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "href", array()), "html", null, true);
            echo "\">
        </div>
      </div>
    </div>
    <div class=\"edit-mode-html\" ";
            // line 75
            if (($this->getAttribute($context["data"], "mode", array()) == "img")) {
                echo "style=\"display: none;\"";
            }
            echo ">
      <div class=\"form-group\">
        <div class=\"col-sm-10 col-sm-offset-2\">
          <textarea class=\"form-control mbl\" id=\"data[";
            // line 78
            echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
            echo "][";
            echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
            echo "][html]\" rows=\"15\"
          name=\"data[";
            // line 79
            echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
            echo "][";
            echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
            echo "][html]\" data-role=\"editor-field\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "html", array()), "html", null, true);
            echo "</textarea>
          <label class=\"control-label html-mrl\"></label><br>
          <a class=\"btn btn-default btn-sm html-mode-upload\" id=\"html-";
            // line 81
            echo twig_escape_filter($this->env, (isset($context["code"]) ? $context["code"] : null), "html", null, true);
            echo "-uploadId-";
            echo twig_escape_filter($this->env, $context["dataId"], "html", null, true);
            echo "\" data-upload-token=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('AppBundle\Twig\WebExtension')->makeUpoadToken("system", "image"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.block_manage.visual_edit.poster_img_upload"), "html", null, true);
            echo "</a>
        </div>
      </div>
    </div>
  </div>
  ";
            // line 86
            $context["first"] = 0;
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['dataId'], $context['data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "admin/block/tag/poster.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  297 => 86,  283 => 81,  274 => 79,  268 => 78,  260 => 75,  243 => 71,  236 => 67,  218 => 62,  211 => 58,  203 => 53,  191 => 52,  188 => 51,  183 => 49,  178 => 48,  173 => 46,  168 => 45,  166 => 44,  155 => 42,  151 => 41,  144 => 37,  136 => 34,  127 => 32,  118 => 30,  108 => 27,  103 => 25,  93 => 22,  84 => 20,  74 => 17,  69 => 15,  59 => 12,  50 => 10,  40 => 7,  35 => 5,  25 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/block/tag/poster.html.twig", "/var/www/edusoho/app/Resources/views/admin/block/tag/poster.html.twig");
    }
}
