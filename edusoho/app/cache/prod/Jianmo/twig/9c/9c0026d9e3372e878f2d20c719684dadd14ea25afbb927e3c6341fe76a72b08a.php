<?php

/* course/header/header-for-guest.html.twig */
class __TwigTemplate_b3405c46f74d365a7c516902b28188b41015486fb0c6360ff9487730b14f6100 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("course/header/header-layout.html.twig", "course/header/header-for-guest.html.twig", 1);
        $this->blocks = array(
            'course_header_subtitle' => array($this, 'block_course_header_subtitle'),
            'course_header_people_num' => array($this, 'block_course_header_people_num'),
            'course_member_count' => array($this, 'block_course_member_count'),
            'course_heard_content' => array($this, 'block_course_heard_content'),
            'course_member_exit' => array($this, 'block_course_member_exit'),
            'course_header_top' => array($this, 'block_course_header_top'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "course/header/header-layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        if (((isset($context["previewAs"]) ? $context["previewAs"] : null) == "guest")) {
            // line 4
            $context["member"] = null;
            // line 5
            $context["courses"] = $this->env->getExtension('AppBundle\Twig\DataExtension')->getData("CoursesByCourseSetId", array("courseSetId" => $this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "id", array())));
        }
        // line 8
        $context["page_type"] = "guest";
        // line 11
        $context["showCourses"] = ((array_key_exists("showCourses", $context)) ? (_twig_default_filter((isset($context["showCourses"]) ? $context["showCourses"] : null), 1)) : (1));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 12
    public function block_course_header_subtitle($context, array $blocks = array())
    {
    }

    // line 14
    public function block_course_header_people_num($context, array $blocks = array())
    {
    }

    // line 16
    public function block_course_member_count($context, array $blocks = array())
    {
        // line 17
        echo "  ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.course_member_count", array("%studentsNum%" => $this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "studentNum", array()))), "html", null, true);
        echo "
";
    }

    // line 20
    public function block_course_heard_content($context, array $blocks = array())
    {
        // line 21
        echo "  <div class=\"course-detail-bottom\">
    <div class=\"course-detail-img\">
      <span class=\"tags mrm\">
        ";
        // line 24
        if (($this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "serializeMode", array()) == "serialized")) {
            // line 25
            echo "          <span class=\"tag-serialing\"></span>
        ";
        } elseif (($this->getAttribute(        // line 26
(isset($context["courseSet"]) ? $context["courseSet"] : null), "serializeMode", array()) == "finished")) {
            // line 27
            echo "          <span class=\"tag-finished\"></span>
        ";
        }
        // line 29
        echo "      </span>
      <img class=\"img-responsive\" src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('AppBundle\Twig\WebExtension')->getFpath($this->env->getExtension('AppBundle\Twig\AppExtension')->courseSetCover((isset($context["courseSet"]) ? $context["courseSet"] : null), "large"), "courseSet.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "title", array()), "html", null, true);
        echo " \">
      <div class=\"image-overlay\"></div>
      ";
        // line 32
        if (((array_key_exists("previewTask", $context)) ? (_twig_default_filter((isset($context["previewTask"]) ? $context["previewTask"] : null), null)) : (null))) {
            // line 33
            echo "        <a href=\"#modal\" data-toggle=\"modal\" data-url=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_task_preview", array("courseId" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "id", array()), "id" => $this->getAttribute((isset($context["previewTask"]) ? $context["previewTask"] : null), "id", array()))), "html", null, true);
            echo "\" class=\"course-img-cover\">
          <i class=\"es-icon es-icon-playcircleoutline\"></i>
        </a>
      ";
        }
        // line 37
        echo "      
      <div class=\"course-detail-remask\">
        ";
        // line 39
        if (($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("course.show_student_num_enabled", "1") == 1)) {
            // line 40
            echo "          ";
            if (($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("course.show_cover_num_mode") == "studentNum")) {
                // line 41
                echo "            <i class=\"es-icon es-icon-people\"></i>";
                echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.students_count", array("%studentsNum%" => (($this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "studentNum", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "studentNum", array()), 0)) : (0))));
                echo "
          ";
            } elseif (($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("course.show_cover_num_mode") == "hitNum")) {
                // line 43
                echo "            <i class=\"es-icon es-icon-view\"></i>";
                echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.students_count", array("%studentsNum%" => (($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "hitNum", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "hitNum", array()), 0)) : (0))));
                echo "
          ";
            }
            // line 45
            echo "        ";
        }
        // line 46
        echo "        ";
        if (($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("course.show_review", "1") == 1)) {
            // line 47
            echo "          <div class=\"score pull-right mb0\">
            ";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["web_macro"]) ? $context["web_macro"] : null), "star", array(0 => (($this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "rating", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "rating", array()), 0)) : (0))), "method"), "html", null, true);
            echo "
            <span class=\"vertical-middle\">";
            // line 49
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.mask.rating_num", array("%ratingNum%" => (($this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "ratingNum", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "ratingNum", array()), 0)) : (0)))), "html", null, true);
            echo "</span>
          </div>
        ";
        }
        // line 52
        echo "      </div>
    </div>
    <div class=\"course-detail-info\">
      <div class=\"course-detail-info__text js-course-detail-info\">
        ";
        // line 56
        $context["onDiscount"] = $this->env->getExtension('AppBundle\Twig\CourseExtension')->isDiscount((isset($context["course"]) ? $context["course"] : null));
        // line 57
        echo "        <div class=\"course-detail-info__title\">
          ";
        // line 58
        if ((isset($context["marketingPage"]) ? $context["marketingPage"] : null)) {
            // line 59
            echo "            ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "title", array()), "html", null, true);
            echo "
          ";
        } else {
            // line 61
            echo "            ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "title", array()), "html", null, true);
            echo "
          ";
        }
        // line 63
        echo "        </div>
        ";
        // line 64
        $context["canManage"] = (((($this->getAttribute((isset($context["member"]) ? $context["member"] : null), "role", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["member"]) ? $context["member"] : null), "role", array()), "")) : ("")) == "teacher") || $this->env->getExtension('AppBundle\Twig\PermissionExtension')->hasPermission("admin_course_content_manage"));
        // line 65
        echo "        ";
        if ((isset($context["canManage"]) ? $context["canManage"] : null)) {
            // line 66
            echo "          <a class=\"btn btn-default btn-xs course-detail-info__manage\" href=\"";
            if ((((array_key_exists("page_type", $context)) ? (_twig_default_filter((isset($context["page_type"]) ? $context["page_type"] : null), "guest")) : ("guest")) == "member")) {
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_set_manage_course_tasks", array("courseSetId" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "courseSetId", array()), "courseId" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "id", array()))), "html", null, true);
            } else {
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_set_manage_base", array("id" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "courseSetId", array()))), "html", null, true);
            }
            echo "\">
            <i class=\"es-icon es-icon-setting\"></i>&nbsp;";
            // line 67
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.default.manage"), "html", null, true);
            echo "
          </a>
        ";
        }
        // line 70
        echo "        ";
        if ($this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "subtitle", array())) {
            // line 71
            echo "          <p class=\"course-detail-info__subtitle text-overflow subtitle cd-text-sm cd-mb16\" data-toggle=\"tooltip\" data-container=\"body\" data-placement=\"top\"
            data-trigger=\"hover\" title=\"";
            // line 72
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "subtitle", array()), "html", null, true);
            echo "\">
            <span >";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "subtitle", array()), "html", null, true);
            echo "</span>
          </p>
        ";
        }
        // line 76
        echo "        <div class=\"";
        if ((isset($context["onDiscount"]) ? $context["onDiscount"] : null)) {
            echo " mb10  ";
        } else {
            echo " cd-mb16 ";
        }
        echo "\">
          ";
        // line 77
        $this->loadTemplate("course/header/header-for-price.html.twig", "course/header/header-for-guest.html.twig", 77)->display($context);
        // line 78
        echo "        </div>
        
        ";
        // line 80
        if ((isset($context["onDiscount"]) ? $context["onDiscount"] : null)) {
            // line 81
            echo "          <div class=\"cd-mb8\">
            <span class=\"course-detail__label\">";
            // line 82
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("plugin.acitivity"), "html", null, true);
            echo "</span>
            <span class=\"text-12 color-warning\">
              <b class=\"hidden-xs course-detail-activity-title\">";
            // line 84
            echo $this->env->getExtension('AppBundle\Twig\WebExtension')->subTextFilter($this->getAttribute($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "discount", array()), "name", array()), 10);
            echo "</b><span class=\"cd-text-medium cd-ml16\" id=\"discount-endtime-countdown\" data-remaintime=";
            echo twig_escape_filter($this->env, ($this->getAttribute($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "discount", array()), "endTime", array()) - $this->env->getExtension('AppBundle\Twig\WebExtension')->timestamp()), "html", null, true);
            echo "></span><i class=\"es-icon es-icon-accesstime text-14 mls cd-text-medium\"></i>
            <span>
          </div>
        ";
        }
        // line 88
        echo "        <div class=\"course-detail-gray course-detail-gray--market ";
        if ( !(isset($context["onDiscount"]) ? $context["onDiscount"] : null)) {
            echo " not-activity ";
        }
        echo "\">
          ";
        // line 89
        if ((isset($context["previewAs"]) ? $context["previewAs"] : null)) {
            // line 90
            echo "            ";
            $context["hasMulCoursePlans"] = $this->env->getExtension('AppBundle\Twig\CourseExtension')->hasMulCourses($this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "id", array()));
            // line 91
            echo "          ";
        } else {
            // line 92
            echo "            ";
            $context["hasMulCoursePlans"] = $this->env->getExtension('AppBundle\Twig\CourseExtension')->hasMulCourses($this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "id", array()), 1);
            // line 93
            echo "          ";
        }
        // line 94
        echo "          ";
        if (((isset($context["showCourses"]) ? $context["showCourses"] : null) && (isset($context["hasMulCoursePlans"]) ? $context["hasMulCoursePlans"] : null))) {
            // line 95
            echo "            <div class=\"course-detail-content\">
              <span class=\"course-detail-title vertical-middle\">";
            // line 96
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.teaching_plan"), "html", null, true);
            echo "</span>
              <ul class=\"nav nav-pills nav-pills-sm nav-pills-transparent course-detail-nav\">
                ";
            // line 98
            $context["current_course"] = (isset($context["course"]) ? $context["course"] : null);
            // line 99
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["courses"]) ? $context["courses"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["course"]) {
                if ((($this->getAttribute($context["course"], "status", array()) == "published") || (isset($context["previewAs"]) ? $context["previewAs"] : null))) {
                    // line 100
                    echo "                  <li class=\"mb5 mrs ";
                    if (($this->getAttribute((isset($context["current_course"]) ? $context["current_course"] : null), "id", array()) == $this->getAttribute($context["course"], "id", array()))) {
                        echo " active ";
                    }
                    echo "\">
                    <a data-toggle=\"tooltip\" data-placement=\"top\" data-container=\"body\" title=\"";
                    // line 101
                    echo twig_escape_filter($this->env, $this->getAttribute($context["course"], "subtitle", array()), "html", null, true);
                    echo "\"
                  data-trigger=\"hover\" href=\"";
                    // line 102
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_show", array("id" => $this->getAttribute($context["course"], "id", array()), "previewAs" => (isset($context["previewAs"]) ? $context["previewAs"] : null))), "html", null, true);
                    echo "\" title=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["course"], "title", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, _twig_default_filter($this->env->getExtension('AppBundle\Twig\WebExtension')->subTextFilter($this->getAttribute($context["course"], "title", array()), 10), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.unname_title")), "html", null, true);
                    echo "
                    </a>
                    <i class=\"es-icon es-icon-done done-icon\"></i>
                  </li>
                ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['course'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 107
            echo "              </ul>
            </div>
          ";
        }
        // line 110
        echo "
          <div class=\"hidden-xs common-line-height\">
            <span class=\"course-detail-title\">";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.expiry_date"), "html", null, true);
        echo "</span>
            ";
        // line 113
        if (($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "expiryMode", array()) == "days")) {
            // line 114
            echo "              <span class=\"gray-dark course-detail-expiry\">
                ";
            // line 115
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.expiry_date.days_mode", array("%expiryDays%" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "expiryDays", array())));
            echo "
              </span>
            ";
        } elseif (($this->getAttribute(        // line 117
(isset($context["course"]) ? $context["course"] : null), "expiryMode", array()) == "end_date")) {
            // line 118
            echo "              <span class=\"gray-dark course-detail-expiry\">
                ";
            // line 119
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.expiry_date.end_date_mode", array("%expiryEndDate%" => twig_date_format_filter($this->env, $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "expiryEndDate", array()), "Y-m-d ")));
            echo "
              </span>
            ";
        } elseif (($this->getAttribute(        // line 121
(isset($context["course"]) ? $context["course"] : null), "expiryMode", array()) == "date")) {
            // line 122
            echo "              <span class=\"gray-dark course-detail-expiry\">
                ";
            // line 123
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.expiry_date.date_mode", array("%expiryStartDate%" => twig_date_format_filter($this->env, $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "expiryStartDate", array()), "Y-m-d "), "%expiryEndDate%" => twig_date_format_filter($this->env, $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "expiryEndDate", array()), "Y-m-d ")));
            echo "
              </span>
            ";
        } elseif (($this->getAttribute(        // line 125
(isset($context["course"]) ? $context["course"] : null), "expiryMode", array()) == "forever")) {
            // line 126
            echo "              <span class=\"gray-dark course-detail-expiry mrl\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.expiry_date.forever_mode"), "html", null, true);
            echo "</span>
            ";
        }
        // line 128
        echo "          </div>
          
          ";
        // line 130
        if ((($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "buyable", array()) != 0) && ($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "buyExpiryTime", array()) != 0))) {
            // line 131
            echo "            <div class=\"mtl hidden-xs common-line-height\">
              ";
            // line 132
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.buy_expiry_date.buy_expiry_time", array("%buyExpiryTime%" => twig_date_format_filter($this->env, $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "buyExpiryTime", array()), "Y-m-d ")));
            echo "
            </div>
          ";
        }
        // line 135
        echo "          
          ";
        // line 136
        if ((($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "showServices", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "showServices", array()), 1)) : (1))) {
            // line 137
            echo "            <div class=\"course-detail-service cd-mt16\">
              ";
            // line 138
            $context["serviceTags"] = $this->env->getExtension('AppBundle\Twig\AppExtension')->buildServiceTags((($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "services", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "services", array()), array())) : (array())));
            // line 139
            echo "              <span class=\"course-detail-title\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.showServices"), "html", null, true);
            echo "</span>
              ";
            // line 140
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["serviceTags"]) ? $context["serviceTags"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                // line 141
                echo "                ";
                if ($this->getAttribute($context["tag"], "active", array())) {
                    // line 142
                    echo "                  <span
                      class=\"lump-sm color-lump bg-primary mr5 hidden-xs\"
                      data-container=\"body\"
                      data-trigger=\"hover\"
                      data-placement=\"top\"
                      data-toggle=\"tooltip\"
                      title=\"";
                    // line 148
                    echo twig_escape_filter($this->env, $this->getAttribute($context["tag"], "fullName", array()), "html", null, true);
                    echo "\"
                      data-content=\"";
                    // line 149
                    echo twig_escape_filter($this->env, $this->getAttribute($context["tag"], "summary", array()), "html", null, true);
                    echo "\">
                    ";
                    // line 150
                    echo twig_escape_filter($this->env, $this->getAttribute($context["tag"], "shortName", array()), "html", null, true);
                    echo "
                  </span>
                ";
                }
                // line 153
                echo "              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 154
            echo "            </div>
          ";
        }
        // line 156
        echo "          
          ";
        // line 157
        if (((_twig_default_filter($this->env->getExtension('AppBundle\Twig\WebExtension')->getSetting("mobile.enabled"), false) && $this->env->getExtension('AppBundle\Twig\CourseExtension')->isSupportEnableAudio((($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "enableAudio", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "enableAudio", array()), 0)) : (0)))) && ($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "type", array()) == "normal"))) {
            // line 158
            echo "            <div class=\"course-detail-listening\">
              <span class=\"course-detail-title\">";
            // line 159
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.info.video.convert.audio.enable_label"), "html", null, true);
            echo "</span>
              ";
            // line 160
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.video.convert.client.click.listen"), "html", null, true);
            echo "
              <a href=\"";
            // line 161
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("mobile");
            echo "\" target=\"__blank\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.video.convert.client.try"), "html", null, true);
            echo "</a>
            </div>
          ";
        }
        // line 164
        echo "        </div>

        <div class=\"course-detail-section-responsive visible-xs visible-sm clearfix\">
          <ul class=\"clearfix\">
            <!--营销页显示-->
            ";
        // line 169
        if ((isset($context["marketingPage"]) ? $context["marketingPage"] : null)) {
            // line 170
            echo "              <li class=\"js-unfavorite-btn ";
            if ( !(isset($context["isUserFavorite"]) ? $context["isUserFavorite"] : null)) {
                echo "hidden";
            }
            echo "\" id=\"unfavorite-btn\" data-url=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_set_unfavorite", array("id" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "courseSetId", array()))), "html", null, true);
            echo "\">
                <a class=\"color-primary\" href=\"javascript:;\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
            // line 171
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.status.followed"), "html", null, true);
            echo "\"
                  data-trigger=\"hover\">
                  <i class=\"es-icon es-icon-favorite color-primary\"></i>
                  <br/>
                  ";
            // line 175
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.status.followed"), "html", null, true);
            echo "
                </a>
              </li>
              <li class=\"js-favorite-btn ";
            // line 178
            if ((isset($context["isUserFavorite"]) ? $context["isUserFavorite"] : null)) {
                echo "hidden";
            }
            echo "\" id=\"favorite-btn\" data-url=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_set_favorite", array("id" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "courseSetId", array()))), "html", null, true);
            echo "\">
                <a class=\"\" href=\"javascript:;\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
            // line 179
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.status.follow"), "html", null, true);
            echo "\"
                  data-trigger=\"hover\">
                  <i class=\"es-icon es-icon-favoriteoutline\"></i>
                  <br/>
                  ";
            // line 183
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.status.follow"), "html", null, true);
            echo "
                </a>
              </li>
            ";
        }
        // line 187
        echo "            
            <li class=\"es-share top\">
              <a class=\" dropdown-toggle\" href=\"\" data-toggle=\"dropdown\" title=\"";
        // line 189
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.share"), "html", null, true);
        echo "\">
                <i class=\"es-icon es-icon-share\"></i>
                <br/>
                ";
        // line 192
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.share"), "html", null, true);
        echo "
              </a>
              ";
        // line 194
        $this->loadTemplate("common/share-dropdown.html.twig", "course/header/header-for-guest.html.twig", 194)->display(array_merge($context, array("type" => "courseSet")));
        // line 195
        echo "            </li>

            ";
        // line 197
        echo $this->env->getExtension('Codeages\PluginBundle\Twig\SlotExtension')->slot("course.header-marketing-btn.extension", array("course" => (isset($context["course"]) ? $context["course"] : null), "mobile" => true));
        echo "

          </ul>
        </div>
      </div>

      <div class=\"course-detail-info__btn js-course-header-operation\">
        <ul class=\"course-operation course-operation--market hidden-xs hidden-sm clearfix mb0\">
          <li class=\"es-qrcode top hidden-xs mrl\" data-url=\"";
        // line 205
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_qrcode", array("id" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "id", array()))), "html", null, true);
        echo "\">
            <a class=\"cd-link-assist\" href=\"javascript:;\"><i class=\"es-icon es-icon-saoyisao course-operation__icon\"></i><span class=\"cd-text-sm cd-text-medium\">";
        // line 206
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.qrcode_tips"), "html", null, true);
        echo "</span></a>
            <span class=\"qrcode-popover\">
              <img class=\"mb0\" src=\"\" alt=\"\">";
        // line 208
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.tips.scan_qrcode_tips"), "html", null, true);
        echo "
            </span>
          </li>

          ";
        // line 213
        echo "          <li class=\"es-share es-share-item top mrl\">
            <a class=\"dropdown-toggle cd-link-assist\" href=\"javascript:;\" data-toggle=\"dropdown\">
              <i class=\"es-icon es-icon-share course-operation__icon\"></i><span class=\"cd-text-sm cd-text-medium\">";
        // line 215
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.share"), "html", null, true);
        echo "</span>
            </a>
            ";
        // line 217
        $this->loadTemplate("common/share-dropdown.html.twig", "course/header/header-for-guest.html.twig", 217)->display(array_merge($context, array("type" => "courseSet")));
        // line 218
        echo "          </li>
          <!--营销页显示-->
          ";
        // line 221
        echo "          ";
        if ((isset($context["marketingPage"]) ? $context["marketingPage"] : null)) {
            // line 222
            echo "            <li class=\"js-unfavorite-btn mrl ";
            if ( !(isset($context["isUserFavorite"]) ? $context["isUserFavorite"] : null)) {
                echo "hidden ";
            }
            echo "\" id=\"unfavorite-btn\" data-url=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_set_unfavorite", array("id" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "courseSetId", array()))), "html", null, true);
            echo "\">
              <a class=\"cd-link-assist\" href=\"javascript:;\"><i class=\"es-icon es-icon-favorite color-danger course-operation__icon\"></i><span class=\"cd-text-sm cd-text-medium\">";
            // line 223
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.status.followed"), "html", null, true);
            echo "</span>
              </a>
            </li>
            <li class=\"js-favorite-btn mrl ";
            // line 226
            if ((isset($context["isUserFavorite"]) ? $context["isUserFavorite"] : null)) {
                echo "hidden";
            }
            echo "\" id=\"favorite-btn\" data-url=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("course_set_favorite", array("id" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "courseSetId", array()))), "html", null, true);
            echo "\">
              <a class=\"cd-link-assist\" href=\"javascript:;\"><i class=\"es-icon es-icon-favoriteoutline course-operation__icon\"></i><span class=\"cd-text-sm cd-text-medium\">";
            // line 227
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.status.follow"), "html", null, true);
            echo "</span>
              </a>
            </li>
          ";
        }
        // line 231
        echo "

          ";
        // line 233
        echo $this->env->getExtension('Codeages\PluginBundle\Twig\SlotExtension')->slot("course.header-marketing-btn.extension", array("course" => (isset($context["course"]) ? $context["course"] : null)));
        echo "
          ";
        // line 235
        echo "         
          ";
        // line 236
        $this->displayBlock('course_member_exit', $context, $blocks);
        // line 237
        echo "        </ul>
        <div class=\"text-right buy-btn-group course-operation-btn ";
        // line 238
        if (((array_key_exists("classroom", $context)) ? (_twig_default_filter((isset($context["classroom"]) ? $context["classroom"] : null), null)) : (null))) {
            echo " course-classroom-tip";
        }
        echo "\">
          ";
        // line 239
        if (((array_key_exists("member", $context)) ? (_twig_default_filter((isset($context["member"]) ? $context["member"] : null), false)) : (false))) {
            // line 240
            echo "            ";
            if ((($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "expiryMode", array()) == "date") && ($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "expiryStartDate", array()) >= $this->env->getExtension('AppBundle\Twig\WebExtension')->timestamp()))) {
                // line 241
                echo "              ";
                echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.expiry_date.expiryStartDate", array("%expiryStartDate%" => twig_date_format_filter($this->env, $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "expiryStartDate", array()), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("date.format"))));
                echo "
            ";
            } else {
                // line 243
                echo "              <a class=\"cd-btn cd-btn-primary\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("my_course_show", array("id" => $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "id", array()))), "html", null, true);
                echo "\">
                ";
                // line 244
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("course.btn.learn"), "html", null, true);
                echo "
              </a>
            ";
            }
            // line 247
            echo "          ";
        } else {
            // line 248
            echo "            ";
            if (((array_key_exists("classroom", $context)) ? (_twig_default_filter((isset($context["classroom"]) ? $context["classroom"] : null), null)) : (null))) {
                // line 249
                echo "              ";
                $this->loadTemplate("course/header/header-for-classroom-info.html.twig", "course/header/header-for-guest.html.twig", 249)->display($context);
                // line 250
                echo "            ";
            } else {
                // line 251
                echo "              ";
                $this->loadTemplate("course/header/header-for-guest-buy-btn.html.twig", "course/header/header-for-guest.html.twig", 251)->display($context);
                // line 252
                echo "            ";
            }
            // line 253
            echo "          ";
        }
        // line 254
        echo "        </div>
      </div>
    </div>
  </div>
";
    }

    // line 236
    public function block_course_member_exit($context, array $blocks = array())
    {
    }

    // line 260
    public function block_course_header_top($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "course/header/header-for-guest.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  680 => 260,  675 => 236,  667 => 254,  664 => 253,  661 => 252,  658 => 251,  655 => 250,  652 => 249,  649 => 248,  646 => 247,  640 => 244,  635 => 243,  629 => 241,  626 => 240,  624 => 239,  618 => 238,  615 => 237,  613 => 236,  610 => 235,  606 => 233,  602 => 231,  595 => 227,  587 => 226,  581 => 223,  572 => 222,  569 => 221,  565 => 218,  563 => 217,  558 => 215,  554 => 213,  547 => 208,  542 => 206,  538 => 205,  527 => 197,  523 => 195,  521 => 194,  516 => 192,  510 => 189,  506 => 187,  499 => 183,  492 => 179,  484 => 178,  478 => 175,  471 => 171,  462 => 170,  460 => 169,  453 => 164,  445 => 161,  441 => 160,  437 => 159,  434 => 158,  432 => 157,  429 => 156,  425 => 154,  419 => 153,  413 => 150,  409 => 149,  405 => 148,  397 => 142,  394 => 141,  390 => 140,  385 => 139,  383 => 138,  380 => 137,  378 => 136,  375 => 135,  369 => 132,  366 => 131,  364 => 130,  360 => 128,  354 => 126,  352 => 125,  347 => 123,  344 => 122,  342 => 121,  337 => 119,  334 => 118,  332 => 117,  327 => 115,  324 => 114,  322 => 113,  318 => 112,  314 => 110,  309 => 107,  293 => 102,  289 => 101,  282 => 100,  276 => 99,  274 => 98,  269 => 96,  266 => 95,  263 => 94,  260 => 93,  257 => 92,  254 => 91,  251 => 90,  249 => 89,  242 => 88,  233 => 84,  228 => 82,  225 => 81,  223 => 80,  219 => 78,  217 => 77,  208 => 76,  202 => 73,  198 => 72,  195 => 71,  192 => 70,  186 => 67,  177 => 66,  174 => 65,  172 => 64,  169 => 63,  163 => 61,  157 => 59,  155 => 58,  152 => 57,  150 => 56,  144 => 52,  138 => 49,  134 => 48,  131 => 47,  128 => 46,  125 => 45,  119 => 43,  113 => 41,  110 => 40,  108 => 39,  104 => 37,  96 => 33,  94 => 32,  87 => 30,  84 => 29,  80 => 27,  78 => 26,  75 => 25,  73 => 24,  68 => 21,  65 => 20,  58 => 17,  55 => 16,  50 => 14,  45 => 12,  41 => 1,  39 => 11,  37 => 8,  34 => 5,  32 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "course/header/header-for-guest.html.twig", "/var/www/edusoho/app/Resources/views/course/header/header-for-guest.html.twig");
    }
}
