<?php

/* admin/block/label/textarea.html.twig */
class __TwigTemplate_2f2bce59fff8f99bb0c153bb81940ba56cdfda5e23f3bcebd27471e1206f78b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("admin/block/label/text.html.twig", "admin/block/label/textarea.html.twig", 1)->display($context);
    }

    public function getTemplateName()
    {
        return "admin/block/label/textarea.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/block/label/textarea.html.twig", "/var/www/edusoho/app/Resources/views/admin/block/label/textarea.html.twig");
    }
}
