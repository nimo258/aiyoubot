<?php

/* admin/theme/edit-modal-layout.html.twig */
class __TwigTemplate_c5cca55590588fce4c0f1aa2de490334e8c4098a7a8500720b00e27824e9d1b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("old-bootstrap-modal-layout.html.twig", "admin/theme/edit-modal-layout.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
            'custom_name' => array($this, 'block_custom_name'),
            'custom_sub_title' => array($this, 'block_custom_sub_title'),
            'custom_count' => array($this, 'block_custom_count'),
            'custom_category_count' => array($this, 'block_custom_category_count'),
            'custom_orderby' => array($this, 'block_custom_orderby'),
            'custom_background_color' => array($this, 'block_custom_background_color'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "old-bootstrap-modal-layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["modalSize"] = "large";
        // line 4
        $context["categoriesFirst"] = $this->env->getExtension('AppBundle\Twig\DataExtension')->getData("Categories", array("group" => "course"));
        // line 5
        $context["title"] = (($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "title", array())) ? ($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "title", array())) : ($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "defaultTitle", array())));
        // line 6
        $context["subTitle"] = (((($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "subTitle", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "subTitle", array()))) : (""))) ? ((($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "subTitle", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "subTitle", array()))) : (""))) : ((($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "defaultSubTitle", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "defaultSubTitle", array()))) : (""))));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.modal.edit"), "html", null, true);
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "  <form id=\"edit-left-course-form\" class=\"form-horizontal item-config-form\" role=\"form\">
    ";
        // line 10
        $this->displayBlock('content', $context, $blocks);
        // line 80
        echo "    <input class=\"hide\" value=\"";
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "id", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "id", array()))) : ("")), "html", null, true);
        echo "\" name=\"id\" >
    <input class=\"hide\" value=\"";
        // line 81
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "blockKey", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "blockKey", array()), "left")) : ("left")), "html", null, true);
        echo "\" name=\"blockKey\" >
  </form>

";
    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        // line 11
        echo "      ";
        $this->displayBlock('custom_name', $context, $blocks);
        // line 21
        echo "
      ";
        // line 22
        $this->displayBlock('custom_sub_title', $context, $blocks);
        // line 32
        echo "
      ";
        // line 33
        $this->displayBlock('custom_count', $context, $blocks);
        // line 44
        echo "
      ";
        // line 45
        $this->displayBlock('custom_category_count', $context, $blocks);
        // line 56
        echo "
      ";
        // line 57
        $this->displayBlock('custom_orderby', $context, $blocks);
        // line 69
        echo "
      ";
        // line 70
        $this->displayBlock('custom_background_color', $context, $blocks);
        // line 79
        echo "    ";
    }

    // line 11
    public function block_custom_name($context, array $blocks = array())
    {
        // line 12
        echo "        <div class=\"row form-group\">
          <div class=\"col-md-3 control-label\">
            <label for=\"name\">";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.custom_name"), "html", null, true);
        echo "</label>
          </div>
          <div class=\"col-md-7 controls\">
            <input type=\"text\"  class=\"form-control\" name=\"title\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "\">
          </div>
        </div>
      ";
    }

    // line 22
    public function block_custom_sub_title($context, array $blocks = array())
    {
        // line 23
        echo "        <div class=\"row form-group\">
          <div class=\"col-md-3 control-label\">
            <label for=\"subname\">";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.modal.subname"), "html", null, true);
        echo "</label>
          </div>
          <div class=\"col-md-7 controls\">
            <input type=\"text\"  class=\"form-control\" name=\"subTitle\" value=\"";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["subTitle"]) ? $context["subTitle"] : null), "html", null, true);
        echo "\">
          </div>
        </div>
      ";
    }

    // line 33
    public function block_custom_count($context, array $blocks = array())
    {
        // line 34
        echo "        <div class=\"form-group\">
          <label class=\"col-sm-3 control-label\">";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.modal.setting_count"), "html", null, true);
        echo "</label>
          <div class=\"col-sm-4\">
            ";
        // line 37
        $context["selectOptions"] = array(4 => 4, 8 => 8, 12 => 12);
        // line 38
        echo "            <select class=\"form-control width-input-large\" name=\"count\">
              ";
        // line 39
        echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->selectOptions((isset($context["selectOptions"]) ? $context["selectOptions"] : null), (($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "count", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "count", array()), 4)) : (4)));
        echo "
            </select>
          </div>
        </div>
      ";
    }

    // line 45
    public function block_custom_category_count($context, array $blocks = array())
    {
        // line 46
        echo "        <div class=\"form-group\">
          <label class=\"col-sm-3 control-label\">";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.modal.setting_custom_category_count"), "html", null, true);
        echo "</label>
          <div class=\"col-sm-4\">
            ";
        // line 49
        $context["selectOptions"] = array(2 => 2, 3 => 3, 4 => 4);
        // line 50
        echo "            <select class=\"form-control width-input-large\" name=\"categoryCount\">
              ";
        // line 51
        echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->selectOptions((isset($context["selectOptions"]) ? $context["selectOptions"] : null), (($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "categoryCount", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "categoryCount", array()), 2)) : (2)));
        echo "
            </select>
          </div>
        </div>
      ";
    }

    // line 57
    public function block_custom_orderby($context, array $blocks = array())
    {
        // line 58
        echo "        <div class=\"form-group\">
          <label class=\"col-md-3 control-label\">";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.modal.setting_custom_orderby"), "html", null, true);
        echo "</label>
          <div class=\"col-md-4 controls\">
          ";
        // line 61
        $context["selectOptions"] = array("latest" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.datagrid.filter.latest"), "hotSeq" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.datagrid.filter.hotest"), "recommendedSeq" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("site.datagrid.filter.recommended"));
        // line 62
        echo "            <select id=\"orderBy\" name=\"orderBy\" required=\"required\" class=\"form-control width-input width-input-large\">
              ";
        // line 63
        echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->selectOptions((isset($context["selectOptions"]) ? $context["selectOptions"] : null), (($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "orderBy", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "orderBy", array()), "latest")) : ("latest")));
        echo "
            </select>
            <div class=\"help-block\" style=\"display:none;\"></div>
          </div>
        </div>
      ";
    }

    // line 70
    public function block_custom_background_color($context, array $blocks = array())
    {
        // line 71
        echo "        <div class=\"form-group\">
          <label class=\"col-sm-3 control-label\">";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.modal.setting_ustom_background_color"), "html", null, true);
        echo "</label>
          <div class=\"col-sm-9 check-box radios pts\">
            ";
        // line 74
        $context["radiosOptions"] = array("section-gray" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.modal.color_gray"), "section-wihte" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.modal.color_wihte"), "" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.modal.color_default"));
        // line 75
        echo "            ";
        echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->radios("background", (isset($context["radiosOptions"]) ? $context["radiosOptions"] : null), (($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "background", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "background", array()), "")) : ("")));
        echo "
          </div>
        </div>
      ";
    }

    // line 86
    public function block_footer($context, array $blocks = array())
    {
        // line 87
        echo "  <button data-submiting-text=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.save.submiting"), "html", null, true);
        echo "\" class=\"btn btn-primary pull-right\" data-form=\"#edit-left-course-form\" id=\"save-btn\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.confirm"), "html", null, true);
        echo "</button>
  <button type=\"button\" class=\"btn btn-link pull-right\" data-dismiss=\"modal\">";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.cancel"), "html", null, true);
        echo "</button>
  <script>app.load('topxiaadminbundle/controller/theme/left-edit-modal')</script>
";
    }

    public function getTemplateName()
    {
        return "admin/theme/edit-modal-layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  259 => 88,  252 => 87,  249 => 86,  240 => 75,  238 => 74,  233 => 72,  230 => 71,  227 => 70,  217 => 63,  214 => 62,  212 => 61,  207 => 59,  204 => 58,  201 => 57,  192 => 51,  189 => 50,  187 => 49,  182 => 47,  179 => 46,  176 => 45,  167 => 39,  164 => 38,  162 => 37,  157 => 35,  154 => 34,  151 => 33,  143 => 28,  137 => 25,  133 => 23,  130 => 22,  122 => 17,  116 => 14,  112 => 12,  109 => 11,  105 => 79,  103 => 70,  100 => 69,  98 => 57,  95 => 56,  93 => 45,  90 => 44,  88 => 33,  85 => 32,  83 => 22,  80 => 21,  77 => 11,  74 => 10,  66 => 81,  61 => 80,  59 => 10,  56 => 9,  53 => 8,  46 => 7,  42 => 1,  40 => 6,  38 => 5,  36 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/theme/edit-modal-layout.html.twig", "/var/www/edusoho/app/Resources/views/admin/theme/edit-modal-layout.html.twig");
    }
}
