<?php

/* admin/theme/edit-modal/edit-course-grid-with-condition-index-modal.html.twig */
class __TwigTemplate_3ace83395826ffd0fc7bfa298f84cf2496ff521798cf700d7b8c388c93adc06c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/theme/edit-modal-layout.html.twig", "admin/theme/edit-modal/edit-course-grid-with-condition-index-modal.html.twig", 1);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/theme/edit-modal-layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "admin/theme/edit-modal/edit-course-grid-with-condition-index-modal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/theme/edit-modal/edit-course-grid-with-condition-index-modal.html.twig", "/var/www/edusoho/app/Resources/views/admin/theme/edit-modal/edit-course-grid-with-condition-index-modal.html.twig");
    }
}
