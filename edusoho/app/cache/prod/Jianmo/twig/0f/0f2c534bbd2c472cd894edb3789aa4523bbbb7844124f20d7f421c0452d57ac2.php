<?php

/* admin/operation-analysis/analysis-base-layout.html.twig */
class __TwigTemplate_8de88dc8734ad59252b835756bede18800e4886ae23b68fd3d72263dc2b223e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/operation-analysis/analysis-base-layout.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
            'analysisHead' => array($this, 'block_analysisHead'),
            'analysisBody' => array($this, 'block_analysisBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["panel"] = "dashboard";
        // line 3
        $context["nav"] = "system";
        // line 4
        $context["menu"] = "admin_operation_analysis";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_main($context, array $blocks = array())
    {
        // line 7
        echo "  <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/libs/gallery/morris/0.5.0/morris.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
  <div class=\"row\">

    <div class=\"col-md-12\">
      ";
        // line 11
        $this->displayBlock('analysisHead', $context, $blocks);
        // line 71
        echo "      ";
        $this->displayBlock('analysisBody', $context, $blocks);
        // line 73
        echo "    </div>
  </div>

";
    }

    // line 11
    public function block_analysisHead($context, array $blocks = array())
    {
        // line 12
        echo "        <div class=\"col-md-12\">
          ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["web_macro"]) ? $context["web_macro"] : null), "flash_messages", array(), "method"), "html", null, true);
        echo "

          <form class=\"well well-sm form-inline\" action=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("admin_operation_analysis_rount", array("tab" => (isset($context["tab"]) ? $context["tab"] : null))), "html", null, true);
        echo "\" method=\"get\" id=\"operation-form\"
            role=\"form\">
            <div class=\"form-group\">
              <select class=\"form-control\" name=\"analysisDateType\">
                ";
        // line 19
        echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->selectOptions($this->env->getExtension('Codeages\PluginBundle\Twig\DictExtension')->getDict("analysisDateType"), $this->getAttribute((isset($context["dataInfo"]) ? $context["dataInfo"] : null), "analysisDateType", array()), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_analysis.data_type_placeholder"));
        echo "
              </select>
            </div>
            <div class=\"form-group\">
              <a type=\"button\" class=\"btn btn-default\" id=\"btn-month\" currentMonthStart=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dataInfo"]) ? $context["dataInfo"] : null), "currentMonthStart", array()), "html", null, true);
        echo "\"
                currentMonthEnd=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dataInfo"]) ? $context["dataInfo"] : null), "currentMonthEnd", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_analysis.this_month"), "html", null, true);
        echo "</a>
            </div>
            <div class=\"form-group\">
              <a type=\"button\" class=\"btn btn-default \" id=\"btn-lastMonth\" lastMonthStart=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dataInfo"]) ? $context["dataInfo"] : null), "lastMonthStart", array()), "html", null, true);
        echo "\"
                lastMonthEnd=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dataInfo"]) ? $context["dataInfo"] : null), "lastMonthEnd", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_analysis.last_month"), "html", null, true);
        echo "</a>
            </div>

            <div class=\"form-group\">
              <a type=\"button\" class=\"btn btn-default \" id=\"btn-lastThreeMonths\" lastThreeMonthsStart=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dataInfo"]) ? $context["dataInfo"] : null), "lastThreeMonthsStart", array()), "html", null, true);
        echo "\"
                lastThreeMonthsEnd=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dataInfo"]) ? $context["dataInfo"] : null), "lastThreeMonthsEnd", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_analysis.recent_three_month"), "html", null, true);
        echo "</a>
            </div>

            <div class=\"form-group mll\">
              <label class=\"ptm\">";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_analysis.start_time"), "html", null, true);
        echo "</label>
              <input type=\"text\" class=\"form-control analysis-input mls\" name=\"startTime\" value=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dataInfo"]) ? $context["dataInfo"] : null), "startTime", array()), "html", null, true);
        echo "\">
            </div>

            <div class=\"form-group mls\">
              <label class=\"ptm\">";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_analysis.end_time"), "html", null, true);
        echo "</label>
              <input type=\"text\" class=\"form-control analysis-input mls\" name=\"endTime\" value=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dataInfo"]) ? $context["dataInfo"] : null), "endTime", array()), "html", null, true);
        echo "\">
            </div>

            <button class=\"btn btn-primary pull-right\" id=\"btn-search\">";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_analysis.search"), "html", null, true);
        echo "</button>
          </form>

          ";
        // line 49
        if ((((array_key_exists("showHelpMessage", $context)) ? (_twig_default_filter((isset($context["showHelpMessage"]) ? $context["showHelpMessage"] : null), null)) : (null)) == 1)) {
            // line 50
            echo "            <div class=\"help-block\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_analysis.help_tips"), "html", null, true);
            echo "</div>
          ";
        }
        // line 52
        echo "        </div>
        <div class=\"col-md-12\">
          ";
        // line 54
        if ((((isset($context["tab"]) ? $context["tab"] : null) == "trend") && !twig_in_filter($this->getAttribute((isset($context["dataInfo"]) ? $context["dataInfo"] : null), "analysisDateType", array()), array(0 => "courseSetSum", 1 => "userSum", 2 => "courseSum")))) {
            // line 55
            echo "            <p class=\"text-muted\">
              <span class=\"mrl\">";
            // line 56
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_analysis.total_count"), "html", null, true);
            echo "：<strong class=\"inflow-num\">";
            echo twig_escape_filter($this->env, ((array_key_exists("count", $context)) ? (_twig_default_filter((isset($context["count"]) ? $context["count"] : null), 0)) : (0)), "html", null, true);
            echo "</strong></span>
            </p>
          ";
        }
        // line 59
        echo "        </div>
        <div class=\"col-md-12\">
          <ul class=\"nav nav-tabs\" role=\"tablist\">
            <li role=\"presentation\"";
        // line 62
        if (((isset($context["tab"]) ? $context["tab"] : null) == "trend")) {
            echo " class=\"active\"";
        }
        echo " ><a
                href=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath((isset($context["href"]) ? $context["href"] : null), array("tab" => "trend", "startTime" => $this->getAttribute((isset($context["dataInfo"]) ? $context["dataInfo"] : null), "startTime", array()), "endTime" => $this->getAttribute((isset($context["dataInfo"]) ? $context["dataInfo"] : null), "endTime", array()), "analysisDateType" => $this->getAttribute((isset($context["dataInfo"]) ? $context["dataInfo"] : null), "analysisDateType", array()))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_analysis.thrend"), "html", null, true);
        echo "</a>
            </li>
            <li role=\"presentation\" ";
        // line 65
        if (((isset($context["tab"]) ? $context["tab"] : null) == "detail")) {
            echo " class=\"active\"";
        }
        echo "><a
                href=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath((isset($context["href"]) ? $context["href"] : null), array("tab" => "detail", "startTime" => $this->getAttribute((isset($context["dataInfo"]) ? $context["dataInfo"] : null), "startTime", array()), "endTime" => $this->getAttribute((isset($context["dataInfo"]) ? $context["dataInfo"] : null), "endTime", array()), "analysisDateType" => $this->getAttribute((isset($context["dataInfo"]) ? $context["dataInfo"] : null), "analysisDateType", array()))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.operation_analysis.detail"), "html", null, true);
        echo "</a>
            </li>
          </ul>
        </div>
      ";
    }

    // line 71
    public function block_analysisBody($context, array $blocks = array())
    {
        // line 72
        echo "      ";
    }

    public function getTemplateName()
    {
        return "admin/operation-analysis/analysis-base-layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 72,  210 => 71,  199 => 66,  193 => 65,  186 => 63,  180 => 62,  175 => 59,  167 => 56,  164 => 55,  162 => 54,  158 => 52,  152 => 50,  150 => 49,  144 => 46,  138 => 43,  134 => 42,  127 => 38,  123 => 37,  114 => 33,  110 => 32,  101 => 28,  97 => 27,  89 => 24,  85 => 23,  78 => 19,  71 => 15,  66 => 13,  63 => 12,  60 => 11,  53 => 73,  50 => 71,  48 => 11,  40 => 7,  37 => 6,  33 => 1,  31 => 4,  29 => 3,  27 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/operation-analysis/analysis-base-layout.html.twig", "/var/www/edusoho/app/Resources/views/admin/operation-analysis/analysis-base-layout.html.twig");
    }
}
