<?php

/* admin/theme/tab-panel/maincolor.html.twig */
class __TwigTemplate_265cbcf24374a3f0e07fca6a7a9de5fe04a41ce6acc0feba878cfa50c15c789a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["themeMainColors"] = array("default" => "#46c37b", "green-light" => "#81d867", "purple" => "#773cec", "purple-light" => "#9e9abd", "orange" => "#ff7200", "orange-light" => "#f9b469", "blue" => "#0081e6", "blue-light" => "#4bbbfa", "red" => "#cf010e", "red-light" => "#fd5f56");
        // line 14
        echo "
";
        // line 15
        $context["navigationColors"] = array("default" => "#212121", "white" => "#fff", "green" => "#46c37b", "green-light" => "#81d867", "purple" => "#773cec", "purple-light" => "#9e9abd", "orange" => "#ff7200", "orange-light" => "#f9b469", "blue" => "#0081e6", "blue-light" => "#4bbbfa", "red" => "#cf010e", "red-light" => "#fd5f56");
        // line 30
        echo "<div class=\"tab-pane\" id=\"maincolor\">
  <div class=\"panel panel-default\">
    <div class=\"panel-heading\">
    ";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.main_color"), "html", null, true);
        echo "
    </div>
    <form class=\"panel-body form-inline radios theme-custom-color-block\">
      <div class=\"form-group\">
        ";
        // line 37
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["themeMainColors"]) ? $context["themeMainColors"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["color"]) {
            // line 38
            echo "          <div class=\"col-lg-3\">
            <label class=\"input-group check-box\">
              <span class=\"input-group-addon\">
                <input type=\"radio\" ";
            // line 41
            if (($context["key"] == (isset($context["maincolor"]) ? $context["maincolor"] : null))) {
                echo "checked=\"checked\" ";
            }
            echo "name=\"maincolor\" value=\"";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">
              </span>
              <div class=\"mod-cpanel-basic-color\" style=\"background:";
            // line 43
            echo twig_escape_filter($this->env, $context["color"], "html", null, true);
            echo "; display:table-cell;height:35px\"></div>
            </label>
          </div>        
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['color'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "      </div>
    </form>
  </div>

  <div class=\"panel panel-default\">
    <div class=\"panel-heading\">";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.setting.theme.manage.navigation_color_scheme"), "html", null, true);
        echo "</div>
    <form class=\"panel-body form-inline radios theme-custom-navigationcolor-block\">
      <div class=\"form-group\">
        ";
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["navigationColors"]) ? $context["navigationColors"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["color"]) {
            // line 56
            echo "          <div class=\"col-lg-3\">
            <label class=\"input-group check-box\">
              <span class=\"input-group-addon\">
                <input type=\"radio\" ";
            // line 59
            if (($context["key"] == (isset($context["navigationcolor"]) ? $context["navigationcolor"] : null))) {
                echo "checked=\"checked\" ";
            }
            echo " name=\"navigationcolor\" value=\"";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">
              </span>
              <div class=\"mod-cpanel-basic-color\" style=\"background:";
            // line 61
            echo twig_escape_filter($this->env, $context["color"], "html", null, true);
            echo "; display:table-cell;height:35px\"></div>
            </label>
          </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['color'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "      </div>
    </form>
  </div>
</div>

";
        // line 70
        $this->env->getExtension('AppBundle\Twig\WebExtension')->loadScript("topxiaadminbundle/controller/theme/theme-setting/color.js");
    }

    public function getTemplateName()
    {
        return "admin/theme/tab-panel/maincolor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 70,  107 => 65,  97 => 61,  88 => 59,  83 => 56,  79 => 55,  73 => 52,  66 => 47,  56 => 43,  47 => 41,  42 => 38,  38 => 37,  31 => 33,  26 => 30,  24 => 15,  21 => 14,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/theme/tab-panel/maincolor.html.twig", "/var/www/edusoho/app/Resources/views/admin/theme/tab-panel/maincolor.html.twig");
    }
}
