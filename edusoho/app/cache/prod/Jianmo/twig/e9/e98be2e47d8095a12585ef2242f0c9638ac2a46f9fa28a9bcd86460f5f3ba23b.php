<?php

/* admin/course/course-item.html.twig */
class __TwigTemplate_558fef255b48214072a016f4137af72ad9fcac995a94e1d62746303672644eb6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<li data-course-id=\"";
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "id", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "id", array()), null)) : (null)), "html", null, true);
        echo "\" class=\"course-grid related-course-grid\" role=\"course-item\" 
";
        // line 2
        if ( !((array_key_exists("showDelBtn", $context)) ? (_twig_default_filter((isset($context["showDelBtn"]) ? $context["showDelBtn"] : null), false)) : (false))) {
            // line 3
            echo "style=\"cursor:pointer\"
";
        }
        // line 4
        echo ">
\t<div class=\"grid-body\" style=\"position:relative\">
    ";
        // line 6
        if ((isset($context["course"]) ? $context["course"] : null)) {
            // line 7
            echo "      ";
            $context["courseSet"] = $this->env->getExtension('AppBundle\Twig\DataExtension')->getData("CourseSet", array("id" => (($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "courseSetId", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "courseSetId", array()),  -1)) : ( -1))));
            // line 8
            echo "    ";
        } else {
            // line 9
            echo "      ";
            $context["courseSet"] = array();
            // line 10
            echo "    ";
        }
        // line 11
        echo "    <img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('AppBundle\Twig\WebExtension')->getFpath($this->env->getExtension('AppBundle\Twig\AppExtension')->courseSetCover((isset($context["courseSet"]) ? $context["courseSet"] : null), "large"), "courseSet.png"), "html", null, true);
        echo "\" class=\"img-responsive thumb\"/>
\t\t<span class=\"title\" style=\"max-height: 15px;overflow: hidden;line-height: 20px;\">
\t\t";
        // line 13
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "title", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["courseSet"]) ? $context["courseSet"] : null), "title", array()), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_manage.manage.not_select_course"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.course_manage.manage.not_select_course"))), "html", null, true);
        if (((isset($context["course"]) ? $context["course"] : null) && $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "title", array()))) {
            echo "-";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["course"]) ? $context["course"] : null), "title", array()), "html", null, true);
        }
        // line 14
        echo "\t\t</span>

\t\t<button type=\"button\" role=\"course-item-delete\" data-course-id=\"";
        // line 16
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "id", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["course"]) ? $context["course"] : null), "id", array()), null)) : (null)), "html", null, true);
        echo "\" ";
        if ( !((array_key_exists("showDelBtn", $context)) ? (_twig_default_filter((isset($context["showDelBtn"]) ? $context["showDelBtn"] : null), false)) : (false))) {
            echo "style=\"display:none\"";
        }
        echo " class=\"series-mode-label close\"><i class=\"glyphicon glyphicon-remove\"></i></button>

\t</div>
</li>";
    }

    public function getTemplateName()
    {
        return "admin/course/course-item.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 16,  60 => 14,  54 => 13,  48 => 11,  45 => 10,  42 => 9,  39 => 8,  36 => 7,  34 => 6,  30 => 4,  26 => 3,  24 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/course/course-item.html.twig", "/var/www/edusoho/app/Resources/views/admin/course/course-item.html.twig");
    }
}
