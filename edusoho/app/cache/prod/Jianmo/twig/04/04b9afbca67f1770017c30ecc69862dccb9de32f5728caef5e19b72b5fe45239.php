<?php

/* admin/wechat-app/index.html.twig */
class __TwigTemplate_7f639e2368f3d6e1b9ec1ddce119eb253b679fbba7e4ae64628adb3e474afc06 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/wechat-app/index.html.twig", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["menu"] = "admin_wechat_app";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        // line 6
        echo "  <div class=\"wechat-app-banner\">
    <div class=\"banner-content\">
      <h3 class=\"banner-title\">
        ";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_app.banner_title"), "html", null, true);
        echo "
      </h3>
      <p class=\"banner-introduce\">
        ";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_app.banner_introduce"), "html", null, true);
        echo "
      </p>

      ";
        // line 15
        if ( !(isset($context["purchased"]) ? $context["purchased"] : null)) {
            // line 16
            echo "        <a class=\"btn btn-primary\" href=\"javascript: window.open('";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_wechat_app_access");
            echo "')\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_app.access_btn"), "html", null, true);
            echo "</a>
      ";
        } elseif ((        // line 17
(isset($context["purchased"]) ? $context["purchased"] : null) &&  !(isset($context["installed"]) ? $context["installed"] : null))) {
            // line 18
            echo "        <a href=\"#\" data-toggle=\"modal\" data-target=\"#modal\" data-url=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_app_package_update_modal", array("id" => (isset($context["latestPackageId"]) ? $context["latestPackageId"] : null), "type" => "install")), "html", null, true);
            echo "\" class=\"btn btn-primary app-btn\" data-keyboard=\"false\" data-backdrop=\"static\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_app.install_btn"), "html", null, true);
            echo "</a>
      ";
        }
        // line 20
        echo "    </div>
  </div>
  <div class=\"wechat-app-introduction row\">
    <div class=\"col-md-3 col-sm-4 col-xs-6\">
      <div class=\"introduction-box\" ";
        // line 24
        if ((((array_key_exists("lang", $context)) ? (_twig_default_filter((isset($context["lang"]) ? $context["lang"] : null), null)) : (null)) == "en")) {
            echo "style=\"height: 336px;\"";
        }
        echo ">
        <img class=\"introduction-icon\" src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/admin/wechat-app/fast-access.png"), "html", null, true);
        echo "\"
          srcset=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/admin/wechat-app/fast-access.png"), "html", null, true);
        echo " 1x, ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/admin/wechat-app/fast-access@2x.png"), "html", null, true);
        echo " 2x\"/>
        <h4 class=\"introduction-title\">";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_app.fast_access_introduction_title"), "html", null, true);
        echo "</h4>
        <p class=\"introduction-content\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_app.fast_access_introduction_content"), "html", null, true);
        echo "</p>
      </div>
    </div>
    <div class=\"col-md-3 col-sm-4 col-xs-6\">
      <div class=\"introduction-box\" ";
        // line 32
        if ((((array_key_exists("lang", $context)) ? (_twig_default_filter((isset($context["lang"]) ? $context["lang"] : null), null)) : (null)) == "en")) {
            echo "style=\"height: 336px;\"";
        }
        echo ">
        <img class=\"introduction-icon\" src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/admin/wechat-app/data-synchronization.png"), "html", null, true);
        echo "\"
          srcset=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/admin/wechat-app/data-synchronization.png"), "html", null, true);
        echo " 1x, ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/admin/wechat-app/data-synchronization@2x.png"), "html", null, true);
        echo " 2x\"/>
        <h4 class=\"introduction-title\">";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_app.data_sync_introduction_title"), "html", null, true);
        echo "</h4>
        <p class=\"introduction-content\">";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_app.data_sync_introduction_content"), "html", null, true);
        echo "</p>
      </div>
    </div>
    <div class=\"col-md-3 col-sm-4 col-xs-6\">
      <div class=\"introduction-box\" ";
        // line 40
        if ((((array_key_exists("lang", $context)) ? (_twig_default_filter((isset($context["lang"]) ? $context["lang"] : null), null)) : (null)) == "en")) {
            echo "style=\"height: 336px;\"";
        }
        echo ">
        <img class=\"introduction-icon\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/admin/wechat-app/flow-entrance.png"), "html", null, true);
        echo "\"
          srcset=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/admin/wechat-app/flow-entrance.png"), "html", null, true);
        echo " 1x, ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/admin/wechat-app/flow-entrance@2x.png"), "html", null, true);
        echo " 2x\"/>
        <h4 class=\"introduction-title\">";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_app.flow_entrance_introduction_title"), "html", null, true);
        echo "</h4>
        <p class=\"introduction-content\">";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_app.flow_entrance_introduction_content"), "html", null, true);
        echo "</p>
      </div>
    </div>
    <div class=\"col-md-3 col-sm-4 col-xs-6\">
      <div class=\"introduction-box\" ";
        // line 48
        if ((((array_key_exists("lang", $context)) ? (_twig_default_filter((isset($context["lang"]) ? $context["lang"] : null), null)) : (null)) == "en")) {
            echo "style=\"height: 336px;\"";
        }
        echo ">
        <img class=\"introduction-icon\" src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/admin/wechat-app/easy-promotion.png"), "html", null, true);
        echo "\"
          srcset=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/admin/wechat-app/easy-promotion.png"), "html", null, true);
        echo " 1x, ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("static-dist/app/img/admin/wechat-app/easy-promotion@2x.png"), "html", null, true);
        echo " 2x\"/>
        <h4 class=\"introduction-title\">";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_app.easy_promotion_introduction_title"), "html", null, true);
        echo "</h4>
        <p class=\"introduction-content\">";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.wechat_app.easy_promotion_introduction_content"), "html", null, true);
        echo "</p>
      </div>
    </div>
  </div>
";
    }

    public function getTemplateName()
    {
        return "admin/wechat-app/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  177 => 52,  173 => 51,  167 => 50,  163 => 49,  157 => 48,  150 => 44,  146 => 43,  140 => 42,  136 => 41,  130 => 40,  123 => 36,  119 => 35,  113 => 34,  109 => 33,  103 => 32,  96 => 28,  92 => 27,  86 => 26,  82 => 25,  76 => 24,  70 => 20,  62 => 18,  60 => 17,  53 => 16,  51 => 15,  45 => 12,  39 => 9,  34 => 6,  31 => 5,  27 => 1,  25 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/wechat-app/index.html.twig", "/var/www/edusoho/app/Resources/views/admin/wechat-app/index.html.twig");
    }
}
