<?php

/* admin/article/article-modal.html.twig */
class __TwigTemplate_7138f7466cf5ff5a053874d3a2f80dbd9942fdecda974897a87a31ba1b48f8ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/article/article-modal.html.twig", 1);
        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["menu"] = "admin_operation_article_manage";
        // line 5
        $context["script_controller"] = "article/article-modal";
        // line 7
        $context["article"] = ((array_key_exists("article", $context)) ? (_twig_default_filter((isset($context["article"]) ? $context["article"] : null), null)) : (null));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_page_title($context, array $blocks = array())
    {
        if ((isset($context["article"]) ? $context["article"] : null)) {
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.edit"), "html", null, true);
        } else {
            $this->displayParentBlock("page_title", $context, $blocks);
        }
    }

    // line 11
    public function block_main($context, array $blocks = array())
    {
        // line 12
        echo "
<style>
  #article-form .popover {
    max-width: 400px;
    width: 400px;
  }
</style>


<form class=\"two-col-form\" id=\"article-form\" method=\"post\" enctype=\"multipart/form-data\"
  ";
        // line 22
        if ((isset($context["article"]) ? $context["article"] : null)) {
            // line 23
            echo "    action=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_article_edit", array("id" => $this->getAttribute((isset($context["article"]) ? $context["article"] : null), "id", array()))), "html", null, true);
            echo "\"
  ";
        } else {
            // line 25
            echo "    action=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_article_create");
            echo "\"
  ";
        }
        // line 27
        echo "  >
  <div class=\"row\">
    <div class=\"col-md-8\">
      <div class=\"form-group\">
        <label for=\"article-title-field\" class=\"control-label\">";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.title"), "html", null, true);
        echo "</label>
        <div class=\"controls\">
          <input class=\"form-control\" id=\"article-title-field\" type=\"text\" name=\"title\" value=\"";
        // line 33
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "title", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "title", array()), "")) : ("")), "html", null, true);
        echo "\">
        </div>
      </div>

      <div class=\"form-group\">
        <label for=\"categoryId\" class=\"control-label\">";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.category"), "html", null, true);
        echo "</label>
        <div class=\"controls\">
          <select class=\"form-control\" id=\"categoryId\" type=\"text\" name=\"categoryId\" required=\"required\" tabindex=\"2\">
            ";
        // line 41
        if ( !((array_key_exists("article", $context)) ? (_twig_default_filter((isset($context["article"]) ? $context["article"] : null), false)) : (false))) {
            // line 42
            echo "              <option></option>
            ";
        }
        // line 44
        echo "          ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categoryTree"]) ? $context["categoryTree"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["tree"]) {
            // line 45
            echo "            <option value=";
            echo twig_escape_filter($this->env, $this->getAttribute($context["tree"], "id", array()), "html", null, true);
            echo " ";
            if (($this->getAttribute($context["tree"], "id", array()) == (($this->getAttribute((isset($context["category"]) ? $context["category"] : null), "id", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["category"]) ? $context["category"] : null), "id", array()))) : ("")))) {
                echo "selected";
            }
            echo ">";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, ($this->getAttribute($context["tree"], "depth", array()) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                if (($this->getAttribute($context["tree"], "depth", array()) > 1)) {
                    echo "　";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo twig_escape_filter($this->env, $this->getAttribute($context["tree"], "name", array()), "html", null, true);
            echo "</option>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tree'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "            </select>
        </div>
      </div>

      <div class=\"form-group\">
        <label for=\"article-tagIds\"　class=\"control-label\">";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.tag"), "html", null, true);
        echo "</label>
        <div class=\"controls\">
          <input type=\"form-control\" id=\"article-tags\" name=\"tags\" required=\"required\" class=\"width-full select2-offscreen\" tabindex=\"-1\" value=\"";
        // line 54
        echo twig_escape_filter($this->env, twig_join_filter(((array_key_exists("tagNames", $context)) ? (_twig_default_filter((isset($context["tagNames"]) ? $context["tagNames"] : null), array())) : (array())), ","), "html", null, true);
        echo "\" data-match-url=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("tag_match");
        echo "\">
          <div class=\"help-block\" style=\"display:none;\"></div>
        </div>
      </div>

      ";
        // line 59
        $this->loadTemplate("org/org-tree-select.html.twig", "admin/article/article-modal.html.twig", 59)->display(array_merge($context, array("orgCode" => (($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "orgCode", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "orgCode", array()), null)) : (null)), "nocolmd" => true)));
        // line 60
        echo "
      <div class=\"form-group\">
        <label for=\"richeditor-body-field\" class=\"control-label\">";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.content"), "html", null, true);
        echo "</label>
        <div class=\"controls\">
          <textarea class=\"form-control\" id=\"richeditor-body-field\" rows=\"16\" name=\"body\"
            data-image-upload-url=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("editor_upload", array("token" => $this->env->getExtension('AppBundle\Twig\WebExtension')->makeUpoadToken("default"))), "html", null, true);
        echo "\"
            data-flash-upload-url=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("editor_upload", array("token" => $this->env->getExtension('AppBundle\Twig\WebExtension')->makeUpoadToken("default", "flash"))), "html", null, true);
        echo "\"
           >";
        // line 67
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "body", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "body", array()), "")) : ("")), "html", null, true);
        echo "</textarea>
        </div>
      </div>

      ";
        // line 71
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->controller("AppBundle:File/Attachment:formFields", array("targetType" => "article", "targetId" => (($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "id", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "id", array()), 0)) : (0))), array("showLabel" => false, "useSeajs" => true)));
        echo "

      <div class=\"form-group\">
        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderCsrfToken("site"), "html", null, true);
        echo "\">
        <button id=\"article-operate-save\" class=\"btn btn-primary\" data-toggle=\"form-submit\" data-loading-text=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.save.submiting"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.confirm"), "html", null, true);
        echo "</button>
        <a class=\"btn btn-link\" href=\"";
        // line 76
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_article");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.return"), "html", null, true);
        echo "</a>
      </div>

    </div>
    <div class=\"col-md-4\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.property"), "html", null, true);
        echo "</div>
        <div class=\"panel-body\">
            <label class=\"checkbox-inline\">
            <input type=\"checkbox\" name=\"sticky\" value=\"1\" ";
        // line 85
        if ($this->env->getExtension('AppBundle\Twig\HtmlExtension')->fieldValue((isset($context["article"]) ? $context["article"] : null), "sticky")) {
            echo " checked=\"checked\" ";
        }
        echo "> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("thread.status.stick"), "html", null, true);
        echo "
          </label>
          <label class=\"checkbox-inline\">
            <input type=\"checkbox\" name=\"featured\" value=\"1\" ";
        // line 88
        if ($this->env->getExtension('AppBundle\Twig\HtmlExtension')->fieldValue((isset($context["article"]) ? $context["article"] : null), "featured")) {
            echo " checked=\"checked\" ";
        }
        echo "> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("thread.status.top"), "html", null, true);
        echo "
          </label>
          <label class=\"checkbox-inline\">
            <input type=\"checkbox\" name=\"promoted\" value=\"1\" ";
        // line 91
        if ($this->env->getExtension('AppBundle\Twig\HtmlExtension')->fieldValue((isset($context["article"]) ? $context["article"] : null), "promoted")) {
            echo " checked=\"checked\" ";
        }
        echo "> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("thread.status.recommend"), "html", null, true);
        echo "
          </label>
          <a class=\"glyphicon glyphicon-question-sign text-muted pull-right\" id=\"article-property-tips\" data-toggle=\"tooltip\" data-placement=\"bottom\" href=\"javascript:\" title=\"\">
          </a>
          <div id=\"article-property-tips-html\" style=\"display:none;\">
            ";
        // line 96
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.property_tooltip");
        echo "
          </div>
        </div>
      </div>

      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">";
        // line 102
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.source_setting"), "html", null, true);
        echo "</div>
        <div class=\"panel-body\">
            <div class=\"form-group\">
              <label for=\"article-source-field\">";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.source_name"), "html", null, true);
        echo "</label>
              <div class=\"controls\">
                <input class=\"form-control\" id=\"article-source-field\" type=\"text\" name=\"source\" value=\"";
        // line 107
        echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->fieldValue((isset($context["article"]) ? $context["article"] : null), "source");
        echo "\">
              </div>
            </div>

            <div class=\"form-group\">
                <label for=\"article-sourceUrl-field\">";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.source_address"), "html", null, true);
        echo "</label>
              <div class=\"controls\">
                  <input class=\"form-control\" id=\"article-sourceUrl-field\" type=\"text\" name=\"sourceUrl\" value=\"";
        // line 114
        echo $this->env->getExtension('AppBundle\Twig\HtmlExtension')->fieldValue((isset($context["article"]) ? $context["article"] : null), "sourceUrl");
        echo "\">
              </div>
            </div>
        </div>
      </div>

      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.thumb_setting"), "html", null, true);
        echo "</div>
        <div class=\"panel-body\">
          <div id=\"article-thumb-container\">
              ";
        // line 124
        if ((($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "thumb", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "thumb", array()), null)) : (null))) {
            // line 125
            echo "                <img class=\"img-responsive\" src='";
            echo twig_escape_filter($this->env, $this->env->getExtension('AppBundle\Twig\WebExtension')->getFpath($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "thumb", array())), "html", null, true);
            echo "'>
              ";
        }
        // line 127
        echo "            </div>
            <br>
            <a href=\"#modal\" data-toggle=\"modal\" data-url=\"";
        // line 129
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_article_show_upload");
        echo "\" class=\"btn btn-default\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.thumb_setting.upload_btn"), "html", null, true);
        echo "</a>
          <a id=\"article_thumb_remove\"  class=\"btn btn-default\" data-url=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_article_thumb_remove", array("id" => (($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "id", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "id", array()), 0)) : (0)))), "html", null, true);
        echo "\" ";
        if ( !(($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "thumb", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "thumb", array()), null)) : (null))) {
            echo "style=\"display:none;\" ";
        }
        echo ">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.btn.delete"), "html", null, true);
        echo "</a>
          <input type=\"hidden\" name=\"thumb\" value=\"";
        // line 131
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "thumb", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "thumb", array()))) : ("")), "html", null, true);
        echo "\" id=\"article-thumb\">
          <input type=\"hidden\" name=\"originalThumb\" value=\"";
        // line 132
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "originalThumb", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["article"]) ? $context["article"] : null), "originalThumb", array()))) : ("")), "html", null, true);
        echo "\" id=\"article-originalThumb\">
          ";
        // line 133
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.thumb_setting.upload_tips");
        echo "
        </div>
      </div>

      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.publish_time"), "html", null, true);
        echo "</div>
        <div class=\"panel-body\">
          <div class=\"form-group\">
            <div class=\"controls\">
              <input class=\"form-control\" type=\"text\" name=\"publishedTime\" value=\" ";
        // line 142
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->env->getExtension('AppBundle\Twig\HtmlExtension')->fieldValue((isset($context["article"]) ? $context["article"] : null), "publishedTime", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "server", array()), "get", array(0 => "REQUEST_TIME"), "method")), "Y-m-d H:i:s"), "html", null, true);
        echo "\">
              <div class=\"help-block\">";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("admin.article_setting.publish_time_tips"), "html", null, true);
        echo "</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>

";
    }

    public function getTemplateName()
    {
        return "admin/article/article-modal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  354 => 143,  350 => 142,  343 => 138,  335 => 133,  331 => 132,  327 => 131,  317 => 130,  311 => 129,  307 => 127,  301 => 125,  299 => 124,  293 => 121,  283 => 114,  278 => 112,  270 => 107,  265 => 105,  259 => 102,  250 => 96,  238 => 91,  228 => 88,  218 => 85,  212 => 82,  201 => 76,  195 => 75,  191 => 74,  185 => 71,  178 => 67,  174 => 66,  170 => 65,  164 => 62,  160 => 60,  158 => 59,  148 => 54,  143 => 52,  136 => 47,  111 => 45,  106 => 44,  102 => 42,  100 => 41,  94 => 38,  86 => 33,  81 => 31,  75 => 27,  69 => 25,  63 => 23,  61 => 22,  49 => 12,  46 => 11,  36 => 9,  32 => 1,  30 => 7,  28 => 5,  26 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "admin/article/article-modal.html.twig", "/var/www/edusoho/app/Resources/views/admin/article/article-modal.html.twig");
    }
}
